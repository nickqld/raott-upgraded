/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import Inputs from './Inputs';
import Setup from './src/setup/setup';




AppRegistry.registerComponent(appName, () => Setup);
console.disableYellowBox = true;


