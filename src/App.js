import React, {Component} from 'react';
import {View, Platform} from 'react-native';
import {Root} from 'native-base';
//import {createDrawerNavigator} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';

import Drawer from './components/Drawer';
import SideBar from './components/sidebar/';

const AppNavigator = createDrawerNavigator(
  {
    Drawer: {screen: Drawer},
  },
  {
    initialRouteName: 'Drawer',
    headerMode: 'none',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
    contentComponent: props => <SideBar {...props} />,
  },
);

export default () => (
  <Root>
    <AppNavigator />
  </Root>
);
