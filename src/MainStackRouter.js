import React, {Component} from 'react';
import {View, Platform} from 'react-native';
//import {createDrawerNavigator, createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';

import {createAppContainer} from 'react-navigation';


// import App from './App';
import LoginStackRouter from './LoginStackRouter';
import Initial from './components/Initial';
import Drawer from './components/Drawer';
import EditProfile from './components/EditProfile';

import SideBar from './components/sidebar/';
const MainStackRouter = createDrawerNavigator(
  {
    Initial: {screen: Initial},
    LoginStackRouter: {screen: LoginStackRouter},
    // AppStackRouter: { screen: App }
    AppStackRouter: {screen: Drawer},
    EditProfile: {screen: EditProfile},
  },
  {
    // initialRouteName: "EditProfile",
    initialRouteName: 'Initial',
    headerMode: 'none',
    mode: 'modal',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
    contentComponent: props => {
      return <SideBar {...props} />;
    },
    drawerLockMode: 'locked-closed',
  },
);

MainStackRouter.navigationOptions = {
  gesturesEnabled: false,
};

const App = createAppContainer(MainStackRouter);
export default App;
