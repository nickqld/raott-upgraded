//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import devTools from 'remote-redux-devtools';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistReducer, persistStore } from 'redux-persist';
import reducer from '../reducers';
import promise from './promise';

export default function configureStore(onCompletion) {
  const enhancer = compose(
    applyMiddleware(thunk, promise),
    devTools({
      name: 'raotts',
      realtime: true,
    }),
  );

  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
  };
  
  const persistedReducer = persistReducer(persistConfig, reducer);
  
  // const store = createStore(reducer, enhancer);
  const store = createStore(persistedReducer, enhancer);
  persistStore(store, null, onCompletion);

  return store;
}
