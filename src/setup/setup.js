import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import App from './App';
import configureStore from './configureStore';
import Geolocation from 'react-native-geolocation-service';
import { PermissionsAndroid, Platform } from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NativeBaseProvider } from 'native-base';
import axios from 'axios';
import {
  Modal,
  View,
  Image,
  TouchableOpacity,
  Text,
  Vibration,
} from 'react-native';
import styles from './style';
import numeral from 'numeral';
import {
  getTrackingStatus,
  requestTrackingPermission,
} from 'react-native-tracking-transparency';

import { LogBox } from 'react-native';
export default class setup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      treasurePopup: false,
      treasure_details: [],
      isLoading: false,
      store: configureStore(() => this.setState({ isLoading: false })),
    };
  }

  async componentDidMount() {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
    let trackingStatus = await getTrackingStatus();

    if (Platform.OS === 'ios') {
      if (trackingStatus === 'not-determined') {
        trackingStatus = await requestTrackingPermission();

        // trackingStatus = await setTimeout(()=>{requestTrackingPermission();}, 3000); 

      }
    }

    if (Platform.OS !== 'android') {
      if (trackingStatus === 'authorized' || trackingStatus === 'unavailable' || trackingStatus === '' || !trackingStatus) {
        Geolocation.requestAuthorization('whenInUse');
        Geolocation.setRNConfiguration({
          skipPermissionRequests: false,
          authorizationLevel: 'whenInUse',
        });
      }
    } else {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
    }

    console.log('trackingStatus' + trackingStatus);
    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable' || trackingStatus === '' || !trackingStatus) {
      this.myInterval = setInterval(() => {
        this.setState({ loading: true });

        //  console.log('Geolocation.getCurrentPosition:');
        try {
          Geolocation.getCurrentPosition(
            position => {
              //    console.log(position.coords);

              AsyncStorage.getItem('@deviceid:key').then(value => {
                // value.replace(/[^a-zA-Z ]/g, "")
                value?.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '')

                let data = {
                  lat: position.coords.latitude,
                  lon: position.coords.longitude,
                  userid: '37b892d23e6336ee4820723882fe30156d0a9bec634e1abec11591473b3f3946',
                };

                axios({
                  url: 'https://raott.com/api/v2/LocationHunt/',
                  type: 'POST',
                  method: 'POST',
                  data,
                  headers: {
                    RaottAuth: 'unauthenticated-user-no-api-token',
                  },
                })
                  .then(response => {
                    console.log(response.data);
                    this.setState({
                      loading: false,
                    });
                    if (response.data.status === 'success') {
                      this.showAlert(response.data.treasure_details);
                    }
                  })
                  .catch(error => {
                    //   console.log('location hunt api err:', error.message);
                  });
              });
            },
            error => {
              // See error code charts below.
              console.log('Geolocation' + error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
          );

        } catch (error) {
          console.log('Geolocation catch', error.message);
        }

      }, 30000);
    }
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }

  showAlert(details) {
    Vibration.vibrate(1 * 1000);
    this.setState({
      treasurePopup: true,
      treasure_details: details,
    });
  }

  renderTreasurePopup() {
    return (

      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.treasurePopup}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.headerTextStyle}>
              Treasure GeoMinted in this location!
            </Text>
            <View
              style={{
                alignItems: 'center',
              }}>
              <Image
                style={styles.imagestyle}
                source={{
                  uri:
                    'https://raott.com/raottdev/assets/treasure_images/' +
                    this.state.treasure_details.treasure_image,
                }}
              />
            </View>
            <Text style={[styles.center, styles.centerRed]}>
              {this.state.treasure_details.treasure_title || 'Treasure Name'}
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: '#000000',
                textAlign: 'center',
              }}>
              Treasure Value: R{' '}
              {numeral(this.state.treasure_details.treasure_amount).format(
                '0,0',
              )}{' '}
              ({this.state.treasure_details.treasure_home_cur})
            </Text>
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => this.setState({ treasurePopup: false })}>
              <Text style={styles.textStyle}>Collect</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    return (
      <NativeBaseProvider>
        <Provider store={this.state.store}>
          <SafeAreaProvider>
            <Text style={styles.headerTextStyle2}>
            </Text>

            {this.state.loading && (
              <View
                style={[
                  styles.searchContainer,
                  { marginTop: Platform.OS !== 'android' ? 25 : 0 },
                ]}>
                <Image
                  style={styles.compass}
                  source={require('../../img/compass.gif')}
                />
                <Text>Searching for hidden treasures nearby...</Text>
              </View>
            )}
            <App />
            {this.state.treasurePopup && this.renderTreasurePopup()}
          </SafeAreaProvider>
        </Provider>
      </NativeBaseProvider>
    );
  }
}
