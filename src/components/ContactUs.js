import React, { Component } from 'react';
import { ButtonPress } from './common';
import {
  Text,
  Platform,
  View,
  Dimensions,
  Image,
  ScrollView,
  TouchableOpacity,
  Linking,
  Alert,
  WebView,
  BackHandler,
} from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  ListItem,
  List,
  Card,
  CardItem,
  Item,
  Input,
} from 'native-base';
import { InternetCheck } from './common/';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
let emailAddress = 'admin@raott.com';

import SafeAreaView from 'react-native-safe-area-view';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
class ContactUs extends Component {
  constructor(props) {
    super(props);
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.props.navigation.navigate('LandingPage');
    return true;
  }

  openFbInstaLink(URL_FOR_APP, URL_FOR_BROWSER) {
    debugger;
    Linking.canOpenURL(URL_FOR_APP)
      .then(supported => {
        console.log(supported, 'supported');
        if (!supported) {
          console.log('linkbrowser');
          Linking.openURL(URL_FOR_BROWSER);
        } else {
          console.log('App');
          Linking.openURL(URL_FOR_APP);
        }
      })
      .catch(err => console.error('An error occurred', err));
  }

  openUrl(url, type) {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          let error = '';
          if (type === 'phone') {
            error = 'Call facility is not available in this device.';
          } else if (type === 'mail') {
            error = 'Email facility is not available in this device.';
          }
          Alert.alert('Error', error);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => Alert.alert('An error occurred', err));
  }

  render() {
    const email = (
      <Text
        style={styles.linkStyle}
        onPress={() => this.openUrl('mailto:' + emailAddress, 'mail')}>
        {emailAddress}
      </Text>
    );
    const Facebook = (
      <Text
        style={styles.linkStyle}
        onPress={() =>
          this.openFbInstaLink(
            Platform.OS === 'ios'
              ? 'fb://profile/437796252957226'
              : 'fb://page/437796252957226',
            'https://www.facebook.com/WorldRaott',
          )
        }>
        Facebook
      </Text>
    );
    const Instagram = (
      <Text
        style={styles.linkStyle}
        onPress={() =>
          this.openFbInstaLink(
            'instagram://user?username=theworldraott',
            'https://www.instagram.com/theworldraott',
          )
        }>
        Instagram
      </Text>
    );
    return (
      <SafeAreaProvider style={{ flex: 1 }}>
        {/* <Container style={styles.mainContainer}>
          <View>
            <Header style={styles.headerStyle}>
              <Left style={{flex: 1}}>
                <Button
                  transparent
                  onPress={() => this.props.navigation.openDrawer()}>
                  <Icon name="menu" style={{fontSize: 25, color: '#e02d2e'}} />
                </Button>
              </Left>
              <Body style={{flex: 10, alignItems: 'center'}}>
                <Title style={{color: '#e02d2e'}}>Contact Us</Title>
              </Body>
             
            </Header>
          </View> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >



          <View
            style={{
              width: deviceWidth,
              backgroundColor: '#e7e4e5',
              paddingHorizontal: 10,
              flexDirection: 'row'
            }}>
            <Button
              transparent
              size="10"
              colorScheme="transparent"
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon
                as={<MaterialIcons name="menu" />}
                style={{ fontSize: 25, color: '#e02d2e' }}
              />
            </Button>
            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', alignSelf: 'stretch' }} >
              <Text style={{ color: '#e02d2e' }}>Contact Us</Text>
            </View>
          </View>

          <View style={styles.textViewStyle}>
            <Text style={styles.textStyle}>
              If you have any questions or just want to tell us how much you
              love our treasure hunt game, please get in touch with us at{' '}
              {email}
            </Text>
            {/* <Text style={styles.textStyle}>
            You can also follow us on {Facebook} or {Instagram}
          </Text> */}
          </View>
          {/* </Container> */}
        </Box>
      </SafeAreaProvider>
    );
  }
}
const styles = {
  mainContainer: {
    flex: 1,
    width: deviceWidth,
    backgroundColor: 'white',
  },
  headerStyle: {
    backgroundColor: '#e7e4e5',
  },
  textViewStyle: {
    flex: 1,
    margin: 16,
  },
  textHeaderStyle: {
    color: '#e02d2e',
    fontSize: 18,
    lineHeight: 24,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 20,
    textDecorationLine: 'underline',
  },
  textStyle: {
    fontSize: 14,
    lineHeight: 18,
    marginBottom: 10,
  },
  linkStyle: {
    color: '#e02d2e',
    fontSize: 14,
    fontWeight: '600',
  },
};

ContactUs.navigationOptions = {
  header: null,
  gesturesEnabled: false,
  drawerLockMode: 'locked-closed',
};
export default ContactUs;
