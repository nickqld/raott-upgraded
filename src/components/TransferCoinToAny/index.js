import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
  Item,
  Input,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  NetInfo,
  BackHandler,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import dialogBox from '../dialogBox/';
import styles from './styles';
import CoinsDetail from './CoinsDetail.js';
import axios from 'axios';
import {SlideButton, SlideDirection} from './slide';
import DialogNotMember from './../TransferCoinToFriends/DialogNotMember.js';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import {connect} from 'react-redux';
import {InternetCheck, TimeOutError} from '../common/';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SafeAreaView from 'react-native-safe-area-view';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
let itemClicked = 'false';

class TransferCoinToAny extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      searchName: '',
      users: [],
      count: '1',
      navigate: this.props.navigation.state.params.navigationFrom,
      user: null,
      isRequesting: false,
      RemoveLoadMore: false,
      friendRequestSent: false,
      showDialog: false,
      clickedFriend: '',
      showDialogGuestUserStatus: false,
    };

    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
  }

  goBack() {
    const {navigation} = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }

    if (navigation.state.params.onRefresh) {
      navigation.state.params.onRefresh();
    }
  }

  componentDidMount() {
    console.log('Category page', this.props);
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  render() {
   
    return (
      <SafeAreaProvider style={{flex: 1}}>
        {/* <Container style={styles.container}> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View style={styles.headerStyle}>
            <TouchableOpacity
              onPress={() => this.goBack()}
              style={{
                height: 44,
                width: 44,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {/* <Icon style={{color: 'white'}} name="arrow-back" /> */}
              <MaterialCommunityIcons name="arrow-left" size={24} color="white" />
            </TouchableOpacity>
            <View transparent style={styles.headerText}>
              <Text style={{color: 'white'}}>
                Add Friends / Transfer Money{' '}
              </Text>
            </View>
          </View>
          <View style={styles.mainView}>
            <View style={styles.searchStyle}>
              <View
                style={{
                  flexDirection: 'row',
                
                  paddingHorizontal: 7,
                }}>
                <TouchableOpacity
                  onPress={() => this.onChangeText(this.state.searchName)}
                  style={{alignSelf: 'center'}}>
                  {/* <Icon
                    style={{
                      fontSize: 25,
                      marginLeft: 10,
                      alignSelf: 'center',
                      backgroundColor: 'transparent',
                      alignSelf: 'center',
                    }}
                    active
                    name="search"
                  /> */}

        <MaterialIcons name="search" size={24} color="grey"   alignSelf= 'start' />

                </TouchableOpacity>
                
                <Input
                flex='1'
                  style={{
                    color: 'rgba(106,106,106,1)',
                    marginLeft: 10,
                    height: 40,
                    fontSize: 14,
                    alignSelf: 'center',
                  }}
                  placeholder="Search User"
                  onChangeText={searchName => this.onChangeText(searchName)}
                  value={this.state.searchName}
                  autoCorrect={false}
                  variant="Unstyled"
                />

                
                {/* <Icon
                  active
                  name="people"
                  style={{
                    fontSize: 25,
                    position: 'absolute',
                    right: 10,
                    marginTop: 2,
                    backgroundColor: 'transparent',
                    alignSelf: 'center',
                  }}
                /> */}

    <Ionicons name="md-people" size={24} color="grey" alignSelf="center" />

              </View>
            </View>
          </View>

          {SELF.state.searchName !== '' &&
          SELF.state.users &&
          SELF.state.users.length != 0 ? (
            <ScrollView>
              {this.renderUsers()}
              {SELF.state.searchName !== '' &&
                SELF.state.users.length != 0 &&
                SELF.state.RemoveLoadMore == false && (
                  <TouchableOpacity onPress={() => this.loadMore()}>
                    <View
                      transparent
                      style={{height: 30, marginTop: 1, alignSelf: 'center'}}>
                      <Text style={{color: 'black', marginTop: 5}}>
                        Load More
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
            </ScrollView>
          ) : (
            <View>
              {SELF.state.user == null && (
                <Text style={{textAlign: 'center', margin: 10}}>
                  No friends found
                </Text>
              )}
            </View>
          )}

          {itemClicked == 'true' && this.renderSearchItemCard()}

          {this.state.showDialog && this.renderDialog()}
          {this.state.showDialogGuestUserStatus &&
            this.renderDialogGuestUserStatus()}
        {/* </Container> */}
        </Box>
      </SafeAreaProvider>
    );
  }

  renderUsers() {
    return this.state.users.map((user, i) => {
      if (user.userid !== this.props.user.userid) {
        return (
          <TouchableOpacity
            key={i}
            onPress={() =>
              this.onSearchedItemClick(user, 'Transfer Coin To Any')
            }>
            <View
              style={{
                backgroundColor: 'rgba(200,200,200,1)',
                height: 30,
                marginTop: 1,
                width: deviceWidth * 0.95,
                alignSelf: 'center',
                justifyContent: 'center',
                borderRadius: 5,
              }}>
              <Text style={{borderWidth: 0, marginLeft: 10}}>
                {user.username}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }
    });
  }

  onSearchedItemClick(user) {
    itemClicked = 'true';
    SELF.setState({users: [], searchName: '', user: user});
  }

  getButtonUI(text, color) {
    let onPress = () => this.addFriend();
    if (text != 'Add Friend') {
      onPress = null;
    }

    return (
      <View style={styles.btnView}>
        <View
          style={{
            backgroundColor: color,
            alignItems: 'center',
            width: deviceWidth * 0.5,
            height: 35,
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={onPress}
            style={{alignSelf: 'center', alignItems: 'center'}}>
            <Text style={{color: 'white'}}>{text}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderSearchItemCard() {
    // itemClicked = 'false'
    if (this.state.user) {
      let buttonUI = null;
      if (
        this.state.friendRequestSent == false ||
        this.state.friendRequestSent == undefined
      ) {
        buttonUI = this.getButtonUI('Add Friend', 'rgba(224,37,38,1)');
      } else {
        buttonUI = this.getButtonUI('Request Pending', 'green');
      }

      var coin_icon = this.state.user.selected_symbol;
      if (coin_icon == 'Crown') {
        coin_icon = require('../../../img/rsz_gold_crown.png');
      } else if (coin_icon == 'Ring') {
        coin_icon = require('../../../img/rsz_gold_ring.png');
      } else if (coin_icon == 'Cup') {
        coin_icon = require('../../../img/rsz_gold_cup.png');
      } else {
        coin_icon = null;
      }

      var icon = this.state.user.profile_image
        ? {
            uri:
              'https://raott.com/raottdev/assets/profileImages/' +
              this.state.user.profile_image,
          }
        : require('../../../img/dummyProfile.png');

      return (
      
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          {/* <Content> */}
            <View style={styles.midMainView}>
              {/* <Card style={styles.profileCard}> */}
              <Box
        width='74%'
        overflow="hidden"
        borderColor="gray.300"
        borderWidth="1"
        borderRadius="3"
        alignSelf= "center"
       marginTop='10'
       paddingY='10'
   
         shadow= '2'
        
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: "gray.50",
        }}
        >
                <Image style={styles.profileImage} source={icon} />

                <View style={styles.nameView}>
                  <Text style={{fontSize: 17}}>{this.state.user.username}</Text>
                  <Text style={{fontSize: 12, color: 'rgba(124,124,124,1)'}}>
                    {this.state.user.fullname}
                  </Text>
                </View>

                {buttonUI}

                {SELF.state.navigate == 'LandingPage' && (
                  <View style={styles.btnView}>
                    <View
                      style={{
                        backgroundColor: 'rgba(224,37,38,1)',
                        alignItems: 'center',
                        width: deviceWidth * 0.5,
                        height: 35,
                        marginTop: Platform.OS === 'ios' ? 1 : 0,
                        borderRadius: 20,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'center',
                          justifyContent: 'center',
                          marginLeft: 40,
                          fontSize: 12,
                          position: 'absolute',
                        }}>
                        Slide to Transfer >>
                      </Text>
                      <SlideButton
                        slideDirection={SlideDirection.RIGHT}
                        onSlideSuccess={this.transferClick.bind(this)}
                        width={deviceWidth * 0.4994}
                        height={35}
                        borderRadius={20}
                        successfulSlideWidth={deviceWidth * 0.2}>
                        {/* <View style={{ backgroundColor: 'rgba(224,37,38,0)',
   
    justifyContent: 'center',
    width: deviceWidth * 0.5,
    height: 25,
    borderRadius: 25}}> */}

                        <View
                          style={{
                            width: 25,
                            height: 25,
                            borderRadius: 30,
                            backgroundColor: 'white',
                            marginLeft: 5,
                            marginTop: 5,
                          }}
                        />
                        {/* </View> */}
                      </SlideButton>
                    </View>
                  </View>
                )}

                <View style={styles.footerView}>
                  <Image style={styles.coinImage} source={coin_icon} />
                  {coin_icon == null ? (
                    <Text style={{color: 'rgba(192,192,192,1)', fontSize: 12}}>
                      No Selected Coin
                    </Text>
                  ) : (
                    <Text style={{color: 'rgba(192,192,192,1)', fontSize: 12}}>
                      {' '}
                      Selected Symbol
                    </Text>
                  )}
                </View>
              {/* </Card> */}
              </Box>
            </View>
          {/* </Content> */}
          {/* <Loading isLoading={this.state.isLoading} /> */}
         {/* </Container> */}
       </Box>
      );
    }
  }

  loadMore() {
    const SELF = this;
    // console.log(SELF.state.count++)
    SELF.setState({count: parseInt(SELF.state.count) + 1});
    //   console.log(SELF.state.count)
    SELF.fetchData(SELF.state.searchName);
  }

  onChangeText(searchName) {
    const SELF = this;
    itemClicked = 'false';
    SELF.setState({
      searchName: searchName,
      count: 1,
      friendRequestSent: false,
    });
    SELF.fetchData(searchName);
  }

  fetchData(searchName) {
    const SELF = this;
    if (searchName != '') {
      if (this.state.isRequesting) {
        return;
      }
      this.setState({
        isRequesting: true,
        RemoveLoadMore: false,
      });
      // console.log('************** userFriendsSearch ' + searchName)
      AsyncStorage.getItem('apiToken')
        .then(value => {
          // console.log(value);
          // console.log(SELF.state.count);
          // console.log(searchName);

          axios({
            url:
              'https://raott.com/api/v2/allUsersSearch/' +
              SELF.state.count +
              '/10/' +
              searchName,
            type: 'GET',
            method: 'GET',
            timeout: 30000,
            headers: {
              RaottAuth: value,
            },
          })
            .then(function(response) {
              // console.log('response of userFriendsSearch');
              // console.log(response);
              let userList = [];
              Object.keys(response.data.friends).map(key => {
                userList.push(response.data.friends[key]);
              });

              if (userList.length == '0') {
                SELF.setState({RemoveLoadMore: true});
              }

              if (SELF.state.count > 1) {
                let userArray = SELF.state.users;

                userList.map(userL => {
                  userArray.push(userL);
                });
                SELF.setState({users: userArray, isRequesting: false});
              } else {
                SELF.setState({users: userList, isRequesting: false});
              }
            })
            .catch(function(error) {
              // console.log(error);
              SELF.setState({isLoading: false});
              TimeOutError(error, () => SELF.fetchData(searchName));
            });
        })
        .done(); // ending statement of asyncstorage
    } else {
      SELF.setState({users: []});
    }
  }

  addFriend() {
    const SELF = this;

    SELF.setState({isLoading: true});
    itemClicked = 'true';

    // console.warn(this.state.user.username)

    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        var a = 'https://raott.com/api/v2/userAddFriend/';
        var b = this.state.user.userid;
        var add = `${a}${b}`;

        // console.log(add);
        axios({
          url: add,
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log(response);
            // console.log(response.data.status);
            if (response.data.status == 'success') {
              SELF.setState({friendRequestSent: true, isLoading: false});
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.addFriend());
          });
      })
      .done(); // ending statement of asyncstorage
  }

  renderDialog() {
    return (
      <DialogNotMember
        method={() => this.toggleModal()}
        titleText=" Not a Member"
        msg={
          'You cannot transfer World Roatt to ' +
          this.state.clickedFriend +
          '. ${this.state.clickedFriend} is still a guest user of Raotts. Only Full members can send and receive World Raott Coins.'
        }
      />
    );
  }
  toggleModal() {
    this.setState({
      showDialog: !this.state.showDialog,
    });
  }

  renderDialogGuestUserStatus() {
    return (
      <DialogNotMember
        method={() => this.toggleModalGuestUserStatus()}
        titleText="Error Transfer Coins"
        msg={
          'You cannot transfer World Roatt to ' +
          this.state.clickedFriend +
          '.You are still a guest user of Raotts. Only Full members can send and receive World Raott Coins. You have to invite at least one friend and he/she must join Raotts using your invite code for you to become a full member.'
        }
      />
    );
  }
  toggleModalGuestUserStatus() {
    this.setState({
      showDialogGuestUserStatus: !this.state.showDialogGuestUserStatus,
    });
  }
  transferClick() {
    if (this.props.user && this.props.user.membership_status == 0) {
      this.setState({
        showDialogGuestUserStatus: true,
        clickedFriend: this.state.user.username,
      });
    } else if (this.state.user.membership_status == '0') {
      this.setState({
        showDialog: true,
        clickedFriend: this.state.user.username,
      });
    } else {
      this.props.navigation.navigate('RoattCoins', {
        headerTitle:
          'Transfer World Raott Coins to ' + this.state.user.username,
        screen: 'FriendList',
        fid: this.state.user.userid,
      });
    }
  }
}

function bindAction(dispatch) {
  return {};
}

const mapStateToProps = state => ({
  user: state.user.data,
});

export default connect(mapStateToProps, bindAction)(TransferCoinToAny);
