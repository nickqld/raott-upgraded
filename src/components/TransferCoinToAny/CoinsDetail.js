import React, {Component} from 'react';

import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  Item,
  Icon,
  Input,
} from 'native-base';
import {
  Dimensions,
  Image,
  ScrollView,
  View,
  TouchableOpacity,
  Slider,
  StyleSheet,
  Alert,
  Platform,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './styles';
import axios from 'axios';
import Loading from '../Loading/';
import {SlideButton, SlideDirection} from './slide';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
let id = '';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
import SafeAreaView from 'react-native-safe-area-view';

class CoinsDetail extends Component {
  // eslint-disable-line
  constructor(props) {
    super(props);

    headerTitle = 'Transfer World Raott Coins to ';
    screen = 'FriendList';
    button = '';

    this.state = {
      friendRequestAccepted: false,
      isLoading: false,
    };
  }

  transferClick() {
    this.props.onClick();
  }
  getPendingButtonUI(button, color) {
    return (
      <View
        style={{
          backgroundColor: color,
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          width: deviceWidth * 0.5,
          height: 40,
          marginTop: 10,
          borderRadius: 20,
        }}>
        <Text style={{color: 'white', textAlign: 'center'}}>{button}</Text>
      </View>
    );
  }
  getAcceptButtonUI(button) {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginTop: 10,
          justifyContent: 'center',
        }}>
        <View
          style={{
            backgroundColor: 'rgba(224,37,38,1)',
            alignItems: 'center',
            width: deviceWidth * 0.25,
            height: 35,
            marginHorizontal: 5,
            borderRadius: 20,
            justifyContent: 'center',
          }}>
          <TouchableOpacity onPress={() => this.acceptRequest()}>
            <Text style={{color: 'white', textAlign: 'center'}}>{button}</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            backgroundColor: 'rgba(224,37,38,1)',
            alignItems: 'center',
            width: deviceWidth * 0.25,
            height: 35,
            marginHorizontal: 5,
            borderRadius: 20,
            justifyContent: 'center',
          }}>
          <TouchableOpacity onPress={() => this.denyRequest()}>
            <Text style={{color: 'white', textAlign: 'center'}}>Deny</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  getSlideButtonUI(button) {
    return (
      <View style={styles.btnView}>
        <View
          style={{
            backgroundColor: 'rgba(224,37,38,1)',
            alignItems: 'center',
            width: deviceWidth * 0.5,
            height: 35,
            marginTop: Platform.OS === 'ios' ? 1 : 0,
            borderRadius: 20,
            flexDirection: 'row',
          }}>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              justifyContent: 'center',
              marginLeft: 40,
              fontSize: 12,
              position: 'absolute',
            }}>
            Slide to Transfer >>
          </Text>
          <SlideButton
            slideDirection={SlideDirection.RIGHT}
            onSlideSuccess={this.transferClick.bind(this)}
            width={deviceWidth * 0.4994}
            height={35}
            borderRadius={20}
            successfulSlideWidth={deviceWidth * 0.2}>
            <View
              style={{
                width: 25,
                height: 25,
                borderRadius: 30,
                backgroundColor: 'white',
                marginLeft: 5,
                marginTop: 5,
              }}
            />
            {/* </View> */}
          </SlideButton>
        </View>
      </View>
    );
  }

  render() {
    // const { singleFriend } = this.props;
    let singleFriend = this.props.singleFriend;
    let buttonUI = null;
    id = singleFriend.friend.userid;

    if (this.state.friendRequestAccepted == true) {
      buttonUI = this.getPendingButtonUI('Accepted', 'green');
    } else if (this.state.friendRequestDenied == true) {
      buttonUI = this.getPendingButtonUI('Denied', '#e02d2e');
    } else {
      if (singleFriend.request_status_text == 'Friend') {
        button = 'Slide to Transfer >>';
        buttonUI = this.getSlideButtonUI(button);
      } else if (
        singleFriend.request_status_text == 'Friend Request Received'
      ) {
        button = 'Accept ';
        buttonUI = this.getAcceptButtonUI(button);
      } else {
        button = 'Request Pending';
        buttonUI = this.getPendingButtonUI(button, 'rgba(224,37,38,1)');
      }
    }

    var icon = singleFriend.friend.profile_image
      ? {uri: singleFriend.friend.profile_image}
      : require('../../../img/dummyProfile.png');
    var coin_icon = singleFriend.friend.selected_symbol;
    if (coin_icon == 'Crown') {
      coin_icon = require('../../../img/rsz_gold_crown.png');
    } else if (coin_icon == 'Ring') {
      coin_icon = require('../../../img/rsz_gold_ring.png');
    } else if (coin_icon == 'Cup') {
      coin_icon = require('../../../img/rsz_gold_cup.png');
    } else {
      coin_icon = null;
    }

    return (
      <SafeAreaProvider style={{flex: 1}}>
        {/* <Container>
          <Content> */}
          <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <Box  flexDirection='column' >
            <View style={styles.midMainView}>
              {/* <Card style={styles.profileCard}><  /Box> */}
              <Box
        width='74%'
        overflow="hidden"
        borderColor="gray.300"
        borderWidth="1"
        borderRadius="3"
        alignSelf= "center"
       marginTop='10'
   
         shadow= '2'
        
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: "gray.50",
        }}
        >


                <View style={{width: deviceWidth * 0.784}}>
                  <Image style={styles.profileImage} source={icon} />
                </View>

                <View style={styles.nameView}>
                  <Text style={{fontSize: 17}}>
                    {singleFriend.friend.username}
                  </Text>
                  <Text style={{fontSize: 12, color: 'rgba(124,124,124,1)'}}>
                    {singleFriend.friend.fullname}
                  </Text>
                  <Text style={{fontSize: 12, color: 'rgba(124,124,124,1)'}}>
                    {singleFriend.friend.email}
                  </Text>
                </View>

                {buttonUI}
                <View style={styles.footerView}>
                  <Image style={styles.coinImage} source={coin_icon} />
                  {coin_icon == null ? (
                    <Text style={{color: 'rgba(192,192,192,1)', fontSize: 12}}>
                      No Selected Coin
                    </Text>
                  ) : (
                    <Text style={{color: 'rgba(192,192,192,1)', fontSize: 12}}>
                      {' '}
                      Selected Symbol
                    </Text>
                  )}
                </View>
              {/* </Card> */}
              </Box>
            </View>
          {/* </Content> */}
          </Box>
          <Loading isLoading={this.state.isLoading} />
        {/* </Container> */}
        </Box>
      </SafeAreaProvider>
    );
  }

  addFriend() {
    const SELF = this;
    this.setState({
      isLoading: true,
    });

    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        var a = 'https://raott.com/api/v2/userAddFriend/';
        var b = singleFriend.friend.userid;
        var add = `${a}${b}`;

        // console.log(add);
        axios({
          url: add,
          type: 'GET',
          method: 'GET',
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log(response);

            SELF.setState({
              isLoading: false,
            });
          })
          .catch(function(error) {
            // console.log(error);

            SELF.setState({
              isLoading: false,
            });
          });
      })
      .done(); // ending statement of asyncstorage
  }

  acceptRequest() {
    SELF = this;

    if (!SELF.state.isLoading) {
      SELF.setState({isLoading: true});
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        // console.log(id)

        axios({
          url: 'https://raott.com/api/v2/userConfirmFriend/' + id,
          type: 'GET',
          method: 'GET',
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log(response);

            if (
              response.data.status == 'success' ||
              response.data.status == 'succes'
            ) {
              SELF.setState({friendRequestAccepted: true, isLoading: false});
            }
          })
          .catch(function(error) {
            // console.log(error);
          });
      })
      .done(); // ending statement of asyncstorage
  }

  denyRequest() {
    const SELF = this;
    if (!SELF.state.isLoading) {
      SELF.setState({isLoading: true});
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        // console.log(id)

        axios({
          url: 'https://raott.com/api/v2/userDenyFriend/' + id,
          type: 'GET',
          method: 'GET',
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log(response);

            if (response.data.status == 'success') {
              Alert.alert('Success', 'Friend Request Denied');
              SELF.setState({isLoading: false, friendRequestDenied: true});
            } else {
              Alert.alert('Error', 'Unable to deny friend request');
              SELF.setState({isLoading: false});
            }
          })
          .catch(function(error) {
            // console.log(error);
          });
      })
      .done(); // ending statement of asyncstorage
  }
}
export default CoinsDetail;
