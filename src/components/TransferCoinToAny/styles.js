import { Dimensions, Platform } from "react-native";

const React = require("react-native");

const { StyleSheet } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    backgroundColor: "white",
    height: deviceHeight - 120
  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: Platform.OS === "ios" ? 25 : 0,
    flexDirection: "row"
  },
  headerText: {
    height: 44,
    width: deviceWidth - 98,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    marginLeft: 5
  },
  // coins detail styling

  mainView: {
    backgroundColor: "rgba(227,227,227,1)"
  },
  searchStyle: {
    backgroundColor: "white",
    width: deviceWidth - 20,
    alignItems: "center",
    alignSelf: "center",
    borderRadius: 10,
    margin: 10
  },
  midMainView: {
    backgroundColor: "white",
    width: deviceWidth,
    height: deviceHeight * 0.78
  },
  profileCard: {
    backgroundColor: "white",
    width: deviceWidth * 0.745,
    alignSelf: "center",
    // height: 150,
    marginTop: 40,
    paddingVertical: 10
  },
  profileImage: {
    width: 100,
    height: 100,
    alignSelf: "center",
    marginTop: 20,
    borderRadius: 50
  },
  nameView: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: Platform.OS === "ios" ? 25 : 5,
    width: deviceWidth * 0.784
  },
  btnView: {
    width: deviceWidth / 2,
    alignItems: "center",
    alignSelf: "center",
    marginTop: 13,
    borderWidth: 0
  },
  btnStyle: {
    backgroundColor: "rgba(224,37,38,0)",

    justifyContent: "center",
    alignItems: "center",
    width: deviceWidth * 0.5,
    height: 25,
    borderRadius: 25
  },
  successTransfer: {
    backgroundColor: "rgba(30,255,38,1)"
  },
  footerView: {
    width: deviceWidth * 0.743,
    justifyContent: "center",
    alignItems: "center",
    height: 120,
    marginTop: 20,
    // position:'absolute',bottom:0,
    backgroundColor: "rgba(255,255,223,1)"
  },
  coinImage: {
    width: 100,
    height: 100
  },
  headerLeftBtn: {
    height: 44,
    width: 44,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute"
  },
  headerTitle: {
    height: 44,
    width: deviceWidth - 98,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    marginLeft: 5
  },
  goBackButton: {
    height: 44,
    width: 44,
    alignItems: "center",
    justifyContent: "center"
  }
};
