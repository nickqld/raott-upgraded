import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//import {DrawerNavigator} from 'react-navigation';
import FriendRequest from './FriendRequest';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
('');

let icon = '';

class drawerHeader extends Component {
  render() {
    const { onClickFriendRequest } = this.props;
    icon = this.props.pImage
      ? { uri: this.props.pImage }
      : require('../../../../img/dummyProfile.png');
    return (
      <View style={{

        flexDirection: 'column'
      }} >

        <View
          style={{
            width: deviceWidth,

            backgroundColor: '#e7e4e5',
            paddingHorizontal: 10,
            flexDirection: 'row'
          }}>

          <Button
            transparent
            size="10"
            colorScheme="transparent"
            onPress={() => this.props.navigation.openDrawer()}
          >

            <Icon
              as={<MaterialIcons name="menu" />}

              style={{ fontSize: 25, color: '#e02d2e' }}
            />
          </Button>
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', alignSelf: 'stretch' }} />

          {/* <Button
            onPress={() => this.props.navigation.navigate('FriendRequest')}
            transparent

            size="10"
            colorScheme="transparent"
          >
            <Icon
              as={<MaterialIcons name="person-add" />}

              style={{ fontSize: 25, color: '#e02d2e' }}
            />
          </Button> */}


        </View>



        {/* <Header style={{backgroundColor: '#e7e4e5'}}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="menu" style={{fontSize: 25, color: '#e02d2e'}} />
            </Button>
          </Left>
          <Body>
            <Title style={{color: '#e02d2e'}} />
          </Body>
          <Right style={{alignItems: 'center'}}>
            <Button
              onPress={() => this.props.navigation.navigate('FriendRequest')}
              transparent>
              <Icon name="md-person-add" style={{color: '#afadae'}} />
            </Button>
          </Right>
        </Header> */}




        <View
          style={{
            width: deviceWidth,
            height: 100,
            backgroundColor: '#e02d2e',
            alignItems: 'center',
          }}>
          <View>
            <Text
              style={{
                color: 'white',
                marginTop: 20,
                fontSize: 22,
                alignSelf: 'center',
              }}>
              {this.props.username}
            </Text>
            <Text style={{ color: 'white', fontSize: 10, alignSelf: 'center' }}>
              My Raott Account
            </Text>
          </View>
        </View>


      </View>
    );
  }
  // friendRequest()
  // {
  //  this.props.navigation.navigate("FriendRequest")

  // }
}
export default drawerHeader;
