import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import RequestDetail from './RequestDetail.js';
import styles from './styles';
import axios from 'axios';
import Loading from '../../Loading/';
// import InternetCheck from "./drawerHeader/"
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';

export default class FriendRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requests: [],
      isLoading: true,
      noDataText: false,
    };
  }

  componentDidMount() {
    // InternetCheck();
    this.getRequests();
  }

  renderRequests() {
    if (this.state.requests && this.state.requests.length > 0) {
      return this.state.requests.map((request, i) => {
        return (
          <RequestDetail
            key={i}
            request={request}
            onAccept={request => this.onAccept(request)}
            onDeny={request => this.onDeny(request)}
          />
        );
      });
    } else {
      return <View />;
    }
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <Container style={styles.container}>
          <View style={styles.headerStyle}>
            <TouchableOpacity
              style={styles.goBackButton}
              onPress={() => this.props.navigation.goBack()}>
              <Icon style={{color: 'white'}} name="arrow-back" />
            </TouchableOpacity>
            <View transparent style={styles.headerText}>
              <Text style={{color: 'white'}}>FRIEND REQUESTS</Text>
            </View>
          </View>
          <ScrollView>{this.renderRequests()}</ScrollView>
          <Loading isLoading={this.state.isLoading} />
          {this.state.noDataText && (
            <Text style={styles.noNewRequestText}>No new friend request</Text>
          )}
        </Container>
      </SafeAreaView>
    );
  }

  getRequests() {
    const SELF = this;
    if (!SELF.state.isLoading) {
      SELF.setState({isLoading: true});
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        axios({
          url: 'https://raott.com/api/v2/friendRequests/',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log('response friendRequests ');
            // console.log(response);
            if (response.data.status == 'success') {
              let requestList = [];
              Object.keys(response.data.requests).map(key => {
                requestList.push(response.data.requests[key]);
              });
              if (requestList.length > 0) {
                SELF.setState({requests: requestList, isLoading: false});
              } else {
                SELF.setState({noDataText: true, isLoading: false});
              }
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.getRequests());
          });
      })
      .done(); // ending statement of asyncstorage
  }

  onDeny(request) {
    const SELF = this;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        axios({
          url: 'https://raott.com/api/v2/userDenyFriend/' + request.userid,
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log(response);
            if (response.data.status == 'success') {
              Alert.alert('Success', ' Request Denied');
              SELF.getRequests();
            } else {
              Alert.alert('Error', 'Unable to deny request');
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.onDeny(request));
          });
      })
      .done(); // ending statement of asyncstorage
  }

  onAccept(request) {
    const SELF = this;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        axios({
          url: 'https://raott.com/api/v2/userConfirmFriend/' + request.userid,
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log(response);
            if (response.data.status == 'success') {
              Alert.alert('Success', ' Request accepted');
              SELF.getRequests();
            }
          })
          .catch(function(error) {
            // console.log(error);
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.onAccept(request));
          });
      })
      .done(); // ending statement of asyncstorage
  }
}
