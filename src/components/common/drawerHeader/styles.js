const React = require("react-native");
import { Dimensions, Platform } from 'react-native'
// const { StyleSheet } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: "white",
  },
  mb10: {
    marginBottom: 10
  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: (Platform.OS === 'ios') ? 20 : 0,
    flexDirection: 'row'
  },
  profileImage: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    left: 10,
    borderRadius: 30
  },
  goBackButton:
    {
      height: 44,
      width: 44,
      alignItems: 'center',
      justifyContent: 'center'
    },
  headerText:
    {
      height: 44,
      width: deviceWidth - 98,
      alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'center',
      marginLeft: 5
    },
  noNewRequestText: {
    textAlign: 'center',
    alignSelf: 'center',
    margin: 10,
    position: 'absolute',
    top: 65
  },
  buttonViewStyle: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'center'
  },
  buttonInnerViewStyle: {
    backgroundColor: 'rgba(224,37,38,1)',
    alignItems: 'center',
    width: deviceWidth * 0.25,
    height: 35,
    marginHorizontal: 5,
    borderRadius: 20,
    justifyContent: 'center'
  },
  buttonTextStyle: {
    color: 'white',
    textAlign: 'center'
  },
  mainViewStyle: {
    flexDirection: 'row',
    height: 80
  },
  innerViewStyle: {
    justifyContent: 'center',
    marginLeft: 20
  },
  userNameTextStyle: {
    marginLeft: 5,
    borderWidth: 0
  }
};
