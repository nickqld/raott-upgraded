import React, { Component } from 'react';
import DateTimePicker from 'react-native-modal-datetime-picker';

class PickDate extends Component {
  state = {
    isDateTimePickerVisible: false,
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    // console.log('A date has been picked: ', date);

    this._hideDateTimePicker();
  };

  render() {
    const date = this.props.initialDate || new Date()
    return (

      <DateTimePicker
        isVisible={this.props.isDateTimePickerVisible}
        onConfirm={this.props.handleDatePicked}
        onCancel={this.props.hideDateTimePicker}
        date={date}
      />

    );
  }

}
export { PickDate }

