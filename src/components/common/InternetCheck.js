import React, {Component} from 'react';
import {Alert} from 'react-native';
import NetInfo from '@react-native-community/netinfo';

export const InternetCheck = () => {
  NetInfo.fetch().then(state => {
    console.log('First, is ' + (state.isConnected ? 'online' : 'offline'));
  });

  NetInfo.addEventListener(state => {
    console.log('Connection type', state.type);
    console.log('Is connected?', state.isConnected);

    if (!state.isConnected) {
      // Alert.alert('Alert!!', 'No Internet Connection. Try Again');
    }
  });
};
