import React, { Component } from 'react';

import { TouchableOpacity, Text, Dimensions } from 'react-native'


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const ButtonPress = ({ onPress, children }) => {



  return (
    <TouchableOpacity onPress={onPress} style={{
      alignSelf: 'center',
      marginTop: 20,
      alignItems: 'center',
      width: deviceWidth - 40,
      height: 40,
      justifyContent: 'center',
      backgroundColor: '#e02d2e',
      borderRadius: 30,
    }}>


      <Text style={{
        color: 'white',
        fontSize: 13, fontWeight: '600'
      }} > {children} </Text>


    </TouchableOpacity>
  );
}

export { ButtonPress }