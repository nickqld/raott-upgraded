import React, {Component} from 'react';
import {NetInfo, Alert} from 'react-native';

export const TimeOutError = (error, onPress) => {
  // console.log({error}, 'ERROR*********************')

  if (error && error.message.toLowerCase().includes('timeout')) {
    Alert.alert(
      'Error',
      'Unable to fetch data',
      [{text: 'OK'}, {text: 'Retry', onPress: () => onPress()}],
      {cancelable: false},
    );
  } else if (
    error &&
    error.message &&
    error.message.includes('Network Error')
  ) {
    Alert.alert('Alert!!', 'No Internet Connection. Try Again');
  } else {
    console.log('moto' + error.message)
    Alert.alert('Error!!', "Couldn't connect . Try Again");
  }
};
