import React, {Component} from 'react';
import {ButtonPress} from './common';
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Linking,
  url,
  Platform,
  Alert,
  NetInfo,
  BackHandler,
  TextInput,
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import ModalDropdown from 'react-native-modal-dropdown';
import Hyperlink from 'react-native-hyperlink';
import {Item, Input, Icon, Container, Content} from 'native-base';
import LoginForm from './LoginForm';
import axios from 'axios';
import Loading from './Loading/';
import Otp from './Otp';
import DialogNotMember from './TransferCoinToFriends/DialogNotMember.js';
import {InternetCheck, TimeOutError} from './common/';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NativeBaseProvider, Box, Center} from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
class CreateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      // _phone_code: 'Select',
      // _phone_number: '',
      password: '',
      re_password: '',
      invite_code: '',
      isLoading: false,
      enablePasswordError: false,
      enableEmailError: '',
      emailNullError: false,
      emailValidityError: false,
      // enablePhoneError: '',
      // phoneNullError: false,
      enableUsernameError: '',
      usernameNullError: false,
      // phoneCodeDialog: false,
      phoneCodeOptions: [],
      countryCodeOptions: [],
      idsOptions: [],
      // phoneCodeNullError: false,
      passwordNullError: false,
      country: 'Select Country',
      countryNullError: false,
      countryCode: '',
      enableInviteCodeError: '',
      signUpPressed: false,
      passwordView: false,
      retypeView: false,
    };
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
    // this.getPhoneCode()
    this.getCountries();
  }

  renderButton() {
    if (!this.state.signUpPressed) {
      return (
        <ButtonPress onPress={() => this.buttonPress()}>Sign me up</ButtonPress>
      );
    } else {
      return <ButtonPress>Sign me up</ButtonPress>;
    }
  }
  buttonPress() {
    if (this.state.email == '') {
      this.setState({emailNullError: true});
    } else if (this.state.username == '') {
      this.setState({usernameNullError: true});
    }
    // else if (this.state._phone_code == 'Select') {
    //   this.setState({ phoneCodeNullError: true })
    // }
    // else if (this.state._phone_number == '') {
    //   this.setState({ phoneNullError: true, phoneCodeNullError: false })
    // }
    else if (this.state.password == '') {
      this.setState({passwordNullError: true});
    } else if (this.state.re_password == '') {
      this.submitEditingPassword();
    } else if (this.state.country == '') {
      this.setState({countryNullError: true});
    } else if (this.state.password != this.state.re_password) {
      this.submitEditingPassword();
    }
    // else if(this.state.enableInviteCodeError == 'true')
    // {
    //   this.checkInvite()
    // }
    else {
      this.submitData();
    }
  }

  backAndroidPress() {
    this.props.navigation.goBack();
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  // renderDialog() {
  //   return (
  //     <DialogNotMember method={() => this.toggleModal()} titleText=""
  //       msg={"Please select phone code first."}
  //     />)
  // }

  // toggleModal() {
  //   this.setState({
  //     phoneCodeDialog: !this.state.phoneCodeDialog, _phone_number: ''
  //   })

  // }

  openUrl(url) {
    if (Linking.canOpenURL(url)) {
      Linking.openURL(url);
    }
  }

  emailWarning(color, text) {
    return (
      <Text
        style={{
          alignSelf: 'center',
          fontSize: 11,
          width: deviceWidth - 50,
          color: color,
        }}>
        {' '}
        {text}
      </Text>
    );
  }

  usernameWarning(color, text) {
    return (
      <Text
        style={{
          alignSelf: 'center',
          fontSize: 11,
          width: deviceWidth - 50,
          color: color,
        }}>
        {' '}
        {text}
      </Text>
    );
  }

  // phoneNumberWarning(color, text) {
  //   return (
  //     <Text style={{ alignSelf: 'center', fontSize: 11, width: deviceWidth - 50, color: color }}> {text} </Text>
  //   )
  // }

  render() {
    // console.log(this.state.emailValidityError,' emailValidityError ', this.state.emailNullError,' emailNullError ')
    let emailUI = null;
    if (this.state.enableEmailError == 'true') {
      emailUI = this.emailWarning('red', ' Email already exists. Try again ');
    } else if (this.state.enableEmailError == 'false') {
      emailUI = this.emailWarning('green', ' Email Id is available. ');
    } else if (this.state.emailNullError == true) {
      emailUI = this.emailWarning('red', ' Please enter email Id ');
    } else if (this.state.emailValidityError == true) {
      emailUI = this.emailWarning('red', 'Email id you entered is not valid  ');
    } else {
      emailUI = null;
    }

    let usernameUI = null;
    if (this.state.enableUsernameError == 'true') {
      usernameUI = this.usernameWarning(
        'red',
        ' Username already taken. Please try another one. ',
      );
    } else if (this.state.enableUsernameError == 'false') {
      usernameUI = this.usernameWarning('green', ' Username is available. ');
    } else if (this.state.usernameNullError == true) {
      usernameUI = this.usernameWarning('red', ' Username is not set blank ');
    } else {
      usernameUI = null;
    }
    // let phoneNumberUI = null
    // if (this.state.enablePhoneError == 'true') {
    //   phoneNumberUI = this.phoneNumberWarning('red', 'Phone number already exists. Try another')
    // }
    // else if (this.state.enablePhoneError == 'false') {
    //   phoneNumberUI = this.phoneNumberWarning('green', 'Phone number available')
    // }
    // else if (this.state.phoneNullError == true) {
    //   phoneNumberUI = this.phoneNumberWarning('red', 'Please fill out this field')
    // }
    // else {
    //   phoneNumberUI = null
    // }
    return (
      <KeyboardAwareScrollView>
        {/* <NativeBaseProvider > */}
        <SafeAreaProvider style={{flex: 1}}>
          {/* <Container style={container}> */}
          <Box
            display="flex"
            flex="1"
            backgroundColor="white"
            flexDirection="column">
            {/* <SafeAreaView style={{flex: 1}}>
        
        <Container style={styles.container}> */}
            {/* *************** Header********** */}
            <View>
              <View style={styles.headerContainer}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}>
                  <MaterialIcons
                    name="keyboard-arrow-left"
                    size={30}
                    style={{color: 'white', marginLeft: 10}}
                  />
                </TouchableOpacity>
                <Text style={styles.headerText}>Sign up to Raotts</Text>
              </View>
            </View>

            {/* <Content> */}
            {/* ******************* Email ****************** */}
            {/* <KeyboardAwareScrollView
  enableOnAndroid={true}
  keyboardShouldPersistTaps={"handled"}
  enableResetScrollToCoords={false}
  > */}
            {/* <Item style={styles.itemStyle}>
              <Icon active name="ios-mail" style={styles.iconStyle} />
              <TextInput
                placeholder="Email"
                placeholderTextColor="#c2c2c2"
                style={styles.inputTextStyle}
                onChangeText={email => this.setState({email})}
                value={this.state.email}
                autoCorrect={false}
                onEndEditing={event => {
                  this.checkEmail();
                }}
              />
           2.In Treasure Collection page, view transactions page, account creation page and sometimes in DealCoins page,, “couldn’t connect” error is coming and app is crashing also.


            </Item> */}

            <Box
              display="flex"
              flex="1"
              backgroundColor="white"
              flexDirection="column"
              margin="5">
              <Input
                placeholder="Email"
                placeholderTextColor="#c2c2c2"
                backgroundColor="#f7f7f7"
                variant="Filled"
                fontSize="13"
                minHeight="10"
                fontWeight="600"
                onChangeText={(email) => this.setState({email})}
                value={this.state.email}
                autoCorrect={false}
                onEndEditing={(event) => {
                  this.checkEmail();
                }}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="mail" />}
                    style={styles.iconStyleNew}
                  />
                }
              />

              {emailUI}

              {/* ********************** Username ************************* */}
              {/* <Item style={styles.itemStyle}>
              <Icon active name="ios-person" style={styles.iconStyle} />
              <Input
                placeholder="Username"
                placeholderTextColor="#c2c2c2"
                style={styles.inputTextStyle}
                onChangeText={username => this.setState({ username })}
                value={this.state.username}
                autoCorrect={false}
                onEndEditing={event => {
                  this.checkUsername();
                }}
              />
            </Item> */}

              <Input
                placeholder="Username"
                placeholderTextColor="#c2c2c2"
                backgroundColor="#f7f7f7"
                variant="Filled"
                fontSize="13"
                minHeight="10"
                marginTop="5"
                fontWeight="600"
                onChangeText={(username) => this.setState({username})}
                value={this.state.username}
                autoCorrect={false}
                onEndEditing={(event) => {
                  this.checkUsername();
                }}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="person" />}
                    style={styles.iconStyleNew}
                  />
                }
              />

              {usernameUI}

              {/* *************************** Phone code + Phone Number ************************* */}
              {/* <View style={{
            flexDirection: 'row', borderWidth: 0, marginTop: 20, marginLeft: 20,
            width: deviceWidth - 40, justifyContent: 'space-between'

          }}>

            <Item style={{

              width: deviceWidth * 0.35,
              height: 40,
              backgroundColor: '#f7f7f7',
              borderRadius: 5
            }} >
              <Icon active name='ios-call' style={styles.iconStyle} />
              <ModalDropdown options={this.state.phoneCodeOptions}
                defaultValue='Select'
                underlayColor='#c2c2c2'
                placeholderTextColor='#c2c2c2'
                color='#c2c2c2'

                onSelect={(idx, value) => this._dropdown_onSelect(idx, value)}
                style={{
                  backgroundColor: '#f7f7f7', alignSelf: 'center', width: deviceWidth * 0.20,
                }} >
                <Text style={{ alignSelf: 'flex-start', color: '#c2c2c2', fontSize: 13, fontWeight: '600', marginTop: 1 }}>{this.state._phone_code}</Text>
                <MaterialIcons name="arrow-drop-down"
                  size={20} style={{ color: '#c2c2c2', position: 'absolute', right: 0 }} />
              </ModalDropdown> */}

              {/* <MaterialIcons name ="arrow drop down" size={12}/> */}

              {/* </Item>

            <Item
              style={{
                width: deviceWidth * 0.50,
                alignSelf: 'flex-end',
                height: 40,
                backgroundColor: '#f7f7f7',
                borderRadius: 5
              }} >

              <Input placeholder=' Phone number' placeholderTextColor='#c2c2c2'
                style={styles.inputTextStyle}
                onChangeText={(_phone_number) => this.setState({ _phone_number })}
                value={this.state._phone_number} autoCorrect={false}
                keyboardType={'numeric'}

                onEndEditing={(event) => {
                  this.checkPhone();
                }}
              />
            </Item>

          </View>
          {this.state.phoneCodeNullError == true &&
            <Text style={styles.errorText}> Please select an item in the list</Text>}
          {phoneNumberUI} */}

              {/* ************************* Password ******************************* */}

              {/* <Item style={styles.itemStyle}>
           
              <MaterialIcons
                name="lock"
                size={24}
                style={styles.iconStyleNew}
              />
              <Input
                placeholder="Password"
                secureTextEntry
                placeholderTextColor="#c2c2c2"
                style={styles.inputTextStyle}
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                autoCorrect={false}
              />
            </Item> */}

              <Input
                placeholder="Password"
                placeholderTextColor="#c2c2c2"
                secureTextEntry={this.state.passwordView ? false : true}
                backgroundColor="#f7f7f7"
                variant="Filled"
                fontSize="13"
                minHeight="10"
                marginTop="5"
                fontWeight="600"
                onChangeText={(password) => this.setState({password})}
                value={this.state.password}
                autoCorrect={false}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="lock" />}
                    style={styles.iconStyleNew}
                  />
                }
                InputRightElement={
                  <Icon
                    as={
                      <MaterialCommunityIcons
                        name={this.state.passwordView ? 'eye' : 'eye-off'}
                      />
                    }
                    style={styles.iconStyleNew}
                    onPress={() =>
                      this.setState({passwordView: !this.state.passwordView})
                    }
                  />
                }
              />

              {this.state.passwordNullError == true && (
                <Text style={styles.errorText}>
                  Please fill out this field.{' '}
                </Text>
              )}

              {/* <Item style={styles.itemStyle}>
              <MaterialIcons
                name="lock"
                size={24}
                style={styles.iconStyleNew}
              />
              <Input
                placeholder="Retype Password"
                secureTextEntry
                placeholderTextColor="#c2c2c2"
                style={styles.inputTextStyle}
                onEndEditing={event => {
                  this.submitEditingPassword();
                }}
                onChangeText={re_password => this.setState({ re_password })}
                value={this.state.re_password}
                autoCorrect={false}
              />
            </Item> */}
              <Input
                placeholder="Retype Password"
                placeholderTextColor="#c2c2c2"
                secureTextEntry={this.state.retypeView ? false : true}
                backgroundColor="#f7f7f7"
                variant="Filled"
                fontSize="13"
                minHeight="10"
                marginTop="5"
                fontWeight="600"
                onEndEditing={(event) => {
                  this.submitEditingPassword();
                }}
                onChangeText={(re_password) => this.setState({re_password})}
                value={this.state.re_password}
                autoCorrect={false}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="lock" />}
                    style={styles.iconStyleNew}
                  />
                }
                InputRightElement={
                  <Icon
                    as={
                      <MaterialCommunityIcons
                        name={this.state.retypeView ? 'eye' : 'eye-off'}
                      />
                    }
                    style={styles.iconStyleNew}
                    onPress={() =>
                      this.setState({retypeView: !this.state.retypeView})
                    }
                  />
                }
              />

              {this.state.enablePasswordError == true && (
                <Text style={styles.errorText}>
                  {' '}
                  Passwords does not match. Please try again{' '}
                </Text>
              )}

              {/* **************************** Country *********************/}
              {/* <Item style={styles.itemStyle}> */}

              <Box
                marginTop="5"
                backgroundColor="#f7f7f7"
                display="flex"
                flexDirection="row">
                <Icon
                  as={<MaterialIcons name="language" />}
                  style={styles.iconStyleNew}
                />
                <ModalDropdown
                  options={this.state.countryCodeOptions}
                  defaultValue="Select Country"
                  onSelect={(idx, value) =>
                    this._dropdown_onSelectCountry(idx, value)
                  }
                  dropdownStyle={{width: deviceWidth * 0.7}}
                  style={styles.modalContainerStyle}>
                  <Text style={styles.countryTextStyle}>
                    {this.state.country}
                  </Text>
                  <MaterialIcons
                    name="arrow-drop-down"
                    size={20}
                    style={styles.countryIconStyle}
                  />
                </ModalDropdown>
              </Box>
              {/* </Item> */}

              {this.state.countryNullError == true && (
                <Text style={styles.errorText}>
                  {' '}
                  Please select an item in the list{' '}
                </Text>
              )}

              {/* ******************** Invite Code ***********************/}
              {/* <Item style={styles.itemStyle}>
              <Icon active name="ios-mail" style={styles.iconStyle} />
              <Input
                placeholder="Invite Code"
                placeholderTextColor="#c2c2c2"
                style={styles.inputTextStyle}
                onChangeText={invite_code => this.setState({ invite_code })}
                value={this.state.invite_code}
                autoCorrect={false}
                onEndEditing={event => {
                  this.checkInvite();
                }}
              />
            </Item> */}

              <Input
                placeholder="Invite Code"
                placeholderTextColor="#c2c2c2"
                backgroundColor="#f7f7f7"
                variant="Filled"
                fontSize="13"
                minHeight="10"
                marginTop="5"
                fontWeight="600"
                onChangeText={(invite_code) => this.setState({invite_code})}
                value={this.state.invite_code}
                autoCorrect={false}
                onEndEditing={(event) => {
                  this.checkInvite();
                }}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="mail" />}
                    style={styles.iconStyleNew}
                  />
                }
              />

              {this.state.enableInviteCodeError == 'true' && (
                <Text style={styles.errorText}> Invalid Invite Code </Text>
              )}

              <View style={styles.existingMemberViewStyle}>
                <Text style={styles.existingMemberTextStyle}>
                  Already a member of the Raotts ?
                </Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('LoginForm')}>
                  <Text style={[styles.hyperLinkStyle, {fontWeight: 'bold'}]}>
                    Click here
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={styles.TandCstyle}>
                <Hyperlink
                  onPress={(url) => this.openUrl(url)}
                  linkStyle={styles.hyperLinkStyle}
                  linkText={(url) => {
                    if (url === 'https://raott.com/raottdev/privacy_policy') {
                      return 'Privacy Policy';
                    } else if (url === 'https://raott.com/raottdev/123') {
                      return '';
                    }
                    return 'Terms and Conditions';
                  }}>
                  <Text style={styles.existingMemberTextStyle}>
                    By clicking Sign me up, I agree to the
                    https://raott.com/raottdev/123
                    https://raott.com/raottdev/terms_and_conditions and
                    https://raott.com/raottdev/privacy_policy of the Raotts
                  </Text>
                </Hyperlink>
              </View>

              <View style={styles.buttonViewStyle}>{this.renderButton()}</View>
            </Box>
            {/* </KeyboardAwareScrollView> */}
            {/* </Content> */}
            {/* {this.state.phoneCodeDialog &&
          this.renderDialog()
        } */}
            <Loading isLoading={this.state.isLoading} />
            {/* </Container>
        
      </SafeAreaView> */}
          </Box>
        </SafeAreaProvider>
        {/* </NativeBaseProvider > */}
      </KeyboardAwareScrollView>
    );
  }

  // _dropdown_onSelect(idx, value) {
  //   console.log(value)
  //   this.setState({ _phone_code: value.split(' (')[0] })

  // }

  _dropdown_onSelectCountry(idx, value) {
    // this.setState({ country: value })
    let list = [];
    list = this.state.idsOptions;
    let index = list[idx];
    this.setState({country: value, countryCode: index});
    // this.getCountryCode(index)
  }

  submitEditingPassword() {
    if (this.state.password != this.state.re_password) {
      this.setState({enablePasswordError: true, passwordNullError: false});
    } else {
      this.setState({enablePasswordError: false, passwordNullError: false});
    }
  }

  submitData() {
    const SELF = this;
    SELF.setState({
      isLoading: true,
      signUpPressed: true,
    });
    // console.log(SELF.state.email, 'email')
    // console.log(SELF.state.username, 'username')
    // console.log(SELF.state.password, 'password')
    // console.log(SELF.state._phone_number, '_phone_number')
    // console.log(SELF.state._phone_code, '_phone_code')
    // console.log(SELF.state.invite_code, 'friends_invite_code')
    // console.log(SELF.state.countryCode, 'countryCode')

    axios.interceptors.request.use((request) => {
      console.log('Starting Request', JSON.stringify(request, null, 2));
      return request;
    });

    axios({
      url: 'https://raott.com/api/Auth/signup_step1/',
      type: 'POST',
      method: 'POST',
      timeout: 30000,
      data: {
        email: SELF.state.email,
        username: SELF.state.username,
        password: SELF.state.password,
        // _phone_number: SELF.state._phone_number,
        // _phone_code: SELF.state._phone_code,
        friends_invite_code: SELF.state.invite_code,
        country: SELF.state.countryCode,
      },
    })
      .then(function (response) {
        console.log('response create account');
        console.log(response);
        if (response.data.status == 'success') {
          SELF.setState({
            countryNullError: false,
          });
          Alert.alert(
            '',
            'We have sent a verification code to your registered email address. Please enter that code to complete registration.',
            [
              {
                text: 'OK',
                onPress: () =>
                  //            <TouchableOpacity onPress={() => navigation.navigate('Edit', { id: navigation.getParam('id') })}>

                  SELF.props.navigation.navigate('Otp', {
                    userid: response.data.userid,
                  }),
              },
            ],
            {cancelable: false},
          );
        } else {
          Alert.alert(response.data.status, response.data.error);
        }
        SELF.setState({
          isLoading: false,
        });
      })
      .catch(function (error) {
        // console.log(error);
        SELF.setState({isLoading: false});
        TimeOutError(error, () => SELF.submitData());
      });
  }

  checkEmail() {
    const SELF = this;
    // console.log('Email :', SELF.state.email)
    if (SELF.state.email) {
      if (!SELF.state.email.includes('@') || !SELF.state.email.includes('.')) {
        SELF.setState({
          emailValidityError: true,
          emailNullError: false,
          enableEmailError: false,
        });
      } else {
        axios({
          url: 'https://raott.com/api/Auth/existsEmail',
          type: 'POST',
          method: 'POST',
          data: {
            email: SELF.state.email,
          },
        })
          .then(function (response) {
            // console.log('response exist email');
            // console.log(response);
            if (response.data.status == 1) {
              SELF.setState({
                enableEmailError: 'true',
                emailNullError: false,
                emailValidityError: false,
              });
            } else {
              SELF.setState({
                enableEmailError: 'false',
                emailNullError: false,
                emailValidityError: false,
              });
            }
          })
          .catch(function (error) {
            // console.log(error);
            if (error instanceof NetworkError) {
              SELF.setState({isLoading: false});
              Alert.alert('Alert!!', 'No Internet Connection. Try Again');
            }
          });
      }
    }
  }

  checkInvite() {
    const SELF = this;
    // console.log('Invite Code:', SELF.state.invite_code)
    if (SELF.state.invite_code) {
      axios({
        url: 'https://raott.com/api/Auth/existsInvitecode',
        type: 'POST',
        method: 'POST',
        data: {
          invite_code: SELF.state.invite_code,
        },
      })
        .then(function (response) {
          // console.log('response exist invite_code');
          // console.log(response);
          if (response.data.status == 1) {
            SELF.setState({enableInviteCodeError: 'false'});
          } else {
            SELF.setState({enableInviteCodeError: 'true'});
          }
        })
        .catch(function (error) {
          if (error instanceof NetworkError) {
            SELF.setState({isLoading: false});
            Alert.alert('Alert!!', 'No Internet Connection. Try Again');
          }
          // console.log(error);
        });
    }
  }

  checkUsername() {
    const SELF = this;
    // console.log('username :', SELF.state.username)
    if (SELF.state.username) {
      axios({
        url: 'https://raott.com/api/auth/existsUsername',
        type: 'POST',
        method: 'POST',
        data: {
          username: SELF.state.username,
        },
      })
        .then(function (response) {
          // console.log('response existsUsername');
          // console.log(response);
          if (response.data.status == 1) {
            SELF.setState({
              enableUsernameError: 'true',
              usernameNullError: false,
            });
          } else {
            SELF.setState({
              enableUsernameError: 'false',
              usernameNullError: false,
            });
          }
        })
        .catch(function (error) {
          if (error instanceof NetworkError) {
            SELF.setState({isLoading: false});
            Alert.alert('Alert!!', 'No Internet Connection. Try Again');
          }
          // console.log(error);
        });
    }
  }

  // checkPhone() {
  //   const SELF = this;

  //   if (SELF.state._phone_code == 'Select') {
  //     SELF.setState({ phoneCodeDialog: true })
  //   }
  //   else {
  //     console.log('phoneNumber :', SELF.state._phone_number)
  //     console.log('phoneCode :', SELF.state._phone_code)
  //     if (SELF.state._phone_number) {
  //       axios({
  //         url: 'https://raott.com/api/auth/existsPhone',
  //         type: 'POST',
  //         method: 'POST',
  //         data: {
  //           _phone_code: SELF.state._phone_code.toString(),
  //           _phone_number: SELF.state._phone_number.toString()
  //         },
  //       })
  //         .then(function (response) {
  //           console.log('response existsPhone');
  //           console.log(response);
  //           if (response.data.status == 1) {
  //             SELF.setState({ enablePhoneError: 'true', phoneNullError: false, phoneCodeNullError: false })
  //           }
  //           else {
  //             SELF.setState({ enablePhoneError: 'false', phoneNullError: false, phoneCodeNullError: false })
  //           }
  //         })
  //         .catch(function (error) {
  //           if (error instanceof NetworkError) {
  //             SELF.setState({ isLoading: false });
  //             Alert.alert('Alert!!', 'No Internet Connection. Try Again')
  //           }
  //           // console.log(error);
  //         });
  //     }
  //   }

  // }

  // getPhoneCode() {
  //   const SELF = this;
  //   axios({
  //     url: 'https://raott.com/api/res/phonecode/',
  //     type: 'GET',
  //     method: 'GET',
  //     headers: {
  //       'RaottAuth': 'unauthenticated-user-no-api-token'
  //     },
  //   }).then(function (response) {
  //     // console.log('response phoneCode ****')
  //     // console.log(response);

  //     let phoneList = [];
  //     Object.keys(response.data.phonecode).map((key) => {
  //       phoneList.push(response.data.phonecode[key]);
  //     })
  //     SELF.setState({ phoneCodeOptions: phoneList });

  //   })
  //     .catch(function (error) {
  //       if (error instanceof NetworkError) {
  //         SELF.setState({ isLoading: false });
  //         Alert.alert('Alert!!', 'No Internet Connection. Try Again')
  //       }
  //       // console.log(error);
  //     });
  // }

  getCountries() {
    const SELF = this;
    axios({
      url: 'https://raott.com/api/res/countries/',
      type: 'GET',
      method: 'GET',
      headers: {
        RaottAuth: 'unauthenticated-user-no-api-token',
      },
    })
      .then(function (response) {
        // console.log('response countries ****')
        // console.log(response);
        console.log('cotrun list: ' + JSON.stringify(response));
        let countryList = [];
        let idList = [];
        Object.keys(response.data.countries).map((key) => {
          countryList.push(response.data.countries[key]);
          idList.push(key);
        });
        SELF.setState({countryCodeOptions: countryList, idsOptions: idList});
      })
      .catch(function (error) {
        if (error instanceof NetworkError) {
          SELF.setState({isLoading: false});
          Alert.alert('Alert!!', 'No Internet Connection. Try Again');
        }
        // console.log(error);
      });
  }

  getCountryCode(id) {
    const SELF = this;
    axios({
      url: 'https://raott.com/api/res/country/' + id,
      type: 'GET',
      method: 'GET',
      headers: {
        RaottAuth: 'unauthenticated-user-no-api-token',
      },
    })
      .then(function (response) {
        // console.log('response countrycode ****')
        // console.log(response);
        SELF.setState({countryCode: response.data.country.country_code});
      })
      .catch(function (error) {
        if (error instanceof NetworkError) {
          SELF.setState({isLoading: false});
          Alert.alert('Alert!!', 'No Internet Connection. Try Again');
        }
        // console.log(error);
      });
  }
}

const styles = {
  errorText: {
    alignSelf: 'center',
    fontSize: 11,
    width: deviceWidth - 50,
    color: 'red',
  },
  inputTextStyle: {
    alignSelf: 'center',
    fontSize: 13,
    fontWeight: '600',
  },
  container: {
    backgroundColor: 'white',
  },
  dropdown: {
    flex: 1,
    right: 8,
    alignSelf: 'stretch',
  },
  itemStyle: {
    marginTop: 20,
    width: deviceWidth - 40,
    alignSelf: 'center',
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10,
  },
  dropdown_text: {
    marginVertical: 10,
    marginHorizontal: 6,
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  dropdown_row: {
    flexDirection: 'row',
    height: 35,
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  dropdown_row_text: {
    marginHorizontal: 4,
    fontSize: 12,
    color: '#AAAAAA',
    textAlignVertical: 'center',
  },
  TandCstyle: {
    marginHorizontal: 20,
    marginTop: 10,
    alignItems: 'center',
  },
  existingMemberTextStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#c2c2c2',
  },
  headerContainer: {
    width: deviceWidth,
    flexDirection: 'row',
    ...Platform.select({
      ios: {
        height: 44,
        marginTop: 20,
      },
      android: {
        height: 44,
      },
    }),
    backgroundColor: '#e02d2e',
    alignItems: 'center',
  },
  headerText: {
    textAlign: 'center',
    borderWidth: 0,
    color: 'white',
    width: deviceWidth - 60,
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 15,
  },
  modalContainerStyle: {
    backgroundColor: '#f7f7f7',
    alignSelf: 'center',
    justifyContent: 'center',
    width: deviceWidth * 0.75,
  },
  countryTextStyle: {
    textAlign: 'left',
    color: '#c2c2c2',
    fontSize: 13,
    fontWeight: '600',
  },
  countryIconStyle: {
    color: '#c2c2c2',
    position: 'absolute',
    right: 0,
  },
  existingMemberViewStyle: {
    marginHorizontal: 20,
    alignSelf: 'flex-start',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    alignSelf: 'center',
  },
  hyperLinkStyle: {
    color: '#e02d2e',
    fontSize: 14,
  },
  buttonViewStyle: {
    marginTop: 5,
    marginBottom: 5,
  },
  iconStyleNew: {
    color: '#e02d2e',
    marginLeft: 10,
    marginRight: 10,
  },
};

export default CreateAccount;
