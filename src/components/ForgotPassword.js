import React, { Component } from 'react';
import { ButtonPress } from './common';
import {
  Text,
  View,
  Dimensions,
  Platform,
  Alert,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  Item,
  Container,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Input,
} from 'native-base';
import Loading from './Loading/';
// import DrawerHeader from './common/drawerHeader/drawerHeader'
import { InternetCheck, TimeOutError } from './common/';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      email: '',
      isLoading: false,
    };

    InternetCheck();
  }

  render() {
    const {
      iconStyle,
      container,
      contentStyle,
      mainView,
      headerView,
      headerStyle,
      goBackButton,
      headerText,
      textInputView,
      itemViewStyle,
      inputFieldStyle,
    } = styles;
    return (
      <NativeBaseProvider >
        <SafeAreaProvider style={{ flex: 1 }}>
          {/* <Container style={container}> */}
          <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
            <View style={headerStyle}>
              <TouchableOpacity
                style={goBackButton}
                onPress={() => this.props.navigation.goBack()}>

                <Icon
                  as={<MaterialIcons name="arrow-back" />}

                  style={{ color: 'white' }}
                /> 

              </TouchableOpacity>
              <View style={headerText}>
                <Text style={{ color: 'white' }}>FORGOT PASSWORD</Text>
              </View>
            </View>
            {/* <Content style={contentStyle}> */}
            <View style={mainView}>
              <View style={textInputView}>
                <Text style={{ color: '#c2c2c2' }}> Email</Text>

                <Input
                  placeholder="Enter Email"
                  placeholderTextColor="#c2c2c2"
                  backgroundColor='#f7f7f7'
                  variant="Filled"
                  fontSize='13'
                  minHeight='10'
                  marginTop='3'
                  fontWeight='600'
                  onChangeText={email => this.setState({ email })}
                  value={SELF.state.email}
                  InputLeftElement={
                    <Icon
                      as={<MaterialIcons name="person" />}
                      style={styles.iconStyleNew}
                    />
                  }
                />

                {/*                
                <Item style={itemViewStyle}>
                  <Icon active name="md-person" style={iconStyle} />
                  <Input
                    placeholder="Enter Email"
                    placeholderTextColor="#c2c2c2"
                    style={inputFieldStyle}
                    onChangeText={email => this.setState({email})}
                    value={SELF.state.email}
                  />
                </Item> */}

              </View>
              {this.renderButton()}
            </View>
            {/* </Content> */}
            <Loading isLoading={this.state.isLoading} />
            {/* </Container> */}
          </Box>
        </SafeAreaProvider>
      </NativeBaseProvider >
    );
  }
  renderButton() {
    return (
      <ButtonPress onPress={() => this.submitData()}>
        Reset Password
      </ButtonPress>
    );
  }
  submitData() {
    SELF.setState({
      isLoading: true,
    });
    if (SELF.state.email == '') {
      Alert.alert('Invalid Credential', 'Please enter Email');
      SELF.setState({
        isLoading: false,
      });
    } else {
      axios({
        url: 'https://raott.com/api/Auth/forgotPassword/',
        type: 'POST',
        method: 'POST',
        timeout: 30000,
        data: {
          email: SELF.state.email,
        },
      })
        .then(function (response) {
          // console.log('response forgotPassword');
          // console.log(response);
          Alert.alert(response.data.status, response.data.email);
          SELF.setState({
            isLoading: false,
          });
        })
        .catch(function (error) {
          // console.log(error);
          SELF.setState({ isLoading: false });
          TimeOutError(error, () => SELF.submitData());
        });
    }
  }
}

const styles = {
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10,
  },

  container: {
    width: deviceWidth,
    backgroundColor: 'white',
  },
  contentStyle: {
    marginTop: 60,
    marginBottom: 10,
  },
  mainView: {
    width: deviceWidth,
    backgroundColor: 'white',
    marginTop: 40
  },
  headerView: {
    width: deviceWidth - 40,
    alignSelf: 'center',
  },
  headerStyle: {
    backgroundColor: '#e02d2e',
    height: 44,
    marginTop: Platform.OS === 'ios' ? 25 : 0,
    flexDirection: 'row',
  },
  headerTextView: {
    width: deviceWidth - 80,
    marginTop: Platform === 'ios' ? 20 : 10,
    position: 'absolute',
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  goBackButton: {
    height: 44,
    width: 44,
    alignItems: 'center',
    justifyContent: 'center',
  },

  headerText: {
    height: 44,
    width: deviceWidth - 98,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 5,
  },
  textInputView: {
    marginTop: 10,
    width: deviceWidth - 40,
    alignSelf: 'center',
  },
  itemViewStyle: {
    marginTop: 10,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  inputFieldStyle: {
    alignSelf: 'center',
    fontSize: 11,
  },
  iconStyleNew: {
    color: '#e02d2e',
    marginLeft: 10,
    marginRight: 10,
    size: 15
  },
};
