import * as React from 'react';
import { Animated, View, Text, TouchableOpacity, StyleSheet, ScrollView, Pressable } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

import { NativeBaseProvider, Box, Center } from 'native-base';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Loading from '../components/Loading/';
import Books from './TreasureChest/Books.js';
//import Loading from '../Loading/';
import axios from 'axios';
import CoinCombinationDialog from './TreasureChest/CoinCombinationDialog.js';
import DynamicTabView from "react-native-dynamic-tab-view";
export default class DynamicTabViewDeals extends React.Component {
  constructor(props) {
    super(props)
 
    this.state = {
      index: 0,
      defaultIndex: 0,
      data: [],
      coin_combination: '',
      renderCoinCombination: false,
      noDataText: false,
      isLoading: true,

    }

    this.state.data.push({
      key: "all_treasures",
      title: "ALL TREASURES",

    })
    this.state.data.push({
      key: "ready_to_buy",
      title: "READY TO BUY TREASURES",

    })


    if (props.deals_data != 'undefined' || this.props.deals_data.length < 1) {
      this.setState({
        noDataText: true
      });
    }

    props.categories.map(tab => {
      console.log('pushing---------------------', tab.category_name);
      this.state.data.push({
        key: tab.id,
        title: tab.category_name,

      })
    })




  }
  onCardCoinCombinationClick(combination) {
    this.setState({
      coin_combination: combination,
      renderCoinCombination: true,
    });
  }
  startLoader() {
    this.setState({
      isLoading: true,
    });
  }
  stopLoader() {
    this.setState({
      isLoading: false,
    });
  }
  onRefresh = data => {
    // this.getdealCategories();
    // this.getDealsData();
  };
  componentDidMount() {
    // this.setState({

    // });
  }
  _renderItem = (item, index) => {
    console.log("in renderItem index", index + " ");
    // this.startLoader()
    if (this.state.isLoading === false) {
      console.log("in renderItem startLoader");
      //  this.startLoader()
    }
    setTimeout(() => { this.setState({ isLoading: false }) }, 5000)




    if (index == 0) {

      return <Books
        navigation={this.props.navigation}
        list={this.props.deals_data}
        noDataText={this.state.noDataText}
        startLoader={() => this.startLoader()}
        stopLoader={() => this.stopLoader()}
        onCardCoinCombinationClick={combination =>
          this.onCardCoinCombinationClick(combination)
        }
        screen={this.props.navigation.state.params.screen}
        onRefresh={this.onRefresh}
        navigateToHistory={() =>
          this.props.navigation.navigate('DealHistory', {
            onSelect: null,
          })
        }
      />
    }
    else if (index == 1) {

      let noDataText;
      let ReadyToBuyList = [];
      if (this.props.deals_data && this.props.deals_data.length) {
        this.props.deals_data.map(deal => {
          let quantity = 0,
            userHas = 0;
          deal._coinCombination.map((coin, i) => {
            if (coin.quantity < coin.userHas) {
              quantity = quantity + parseInt(coin.quantity);
              userHas = userHas + parseInt(coin.quantity);
            } else {
              quantity = quantity + parseInt(coin.quantity);
              userHas = userHas + parseInt(coin.userHas);
            }
          });
          if (userHas >= quantity) {
            ReadyToBuyList.push(deal);
          }
        });
      }
      if (ReadyToBuyList.length > 0) {
        noDataText = false;
      } else {
        noDataText = true;
      }

      return <Books
        navigation={this.props.navigation}
        list={ReadyToBuyList}
        noDataText={noDataText}
        startLoader={() => this.startLoader()}
        onRefresh={this.onRefresh}
        stopLoader={() => this.stopLoader()}
        screen={this.props.navigation.state.params.screen}
        onCardCoinCombinationClick={combination =>
          this.onCardCoinCombinationClick(combination)
        }
        navigateToHistory={() =>
          this.props.navigation.navigate('DealHistory', {
            onSelect: null,
          })
        }
      />



    }

    else {

      let noDataText;
      let layout = {}

      this.state.data &&
        this.state.data != 'undefined' &&
        this.state.data.map((cat, i) => {

          let dealList = [];
          this.props.deals_data.map((singleDeal, j) => {
            if (cat.key == singleDeal.dealCategory) {
              dealList.push({ ...singleDeal, itemIndex: j });
            }
            if (dealList.length > 0) {
              noDataText = false;
            } else {
              noDataText = true;
            }
          });

          if (item["key"] == cat.key) {
            console.log("in renderItem creating layout", item["key"] + " " + cat.key);
            layout = <Books
              navigation={this.props.navigation}
              list={dealList}
              noDataText={noDataText}
              startLoader={() => this.startLoader()}
              stopLoader={() => this.stopLoader()}
              onCardCoinCombinationClick={combination =>
                this.onCardCoinCombinationClick(combination)
              }
              screen={this.props.navigation.state.params.screen}
              onRefresh={this.onRefresh}
              navigateToHistory={() =>
                this.props.navigation.navigate('DealHistory', {
                  onSelect: null,
                })
              }
            />

          }
        })


      return (
        layout
      );
    }
  };
  onChangeTab = index => { };


  renderCoinCombinationDialog() {
    return (
      <CoinCombinationDialog
        details={this.state.coin_combination}
        method={() => this.toggleCoinCombinationDialog()}
      />
    ); 
  }
  toggleCoinCombinationDialog() {
    this.setState({ renderCoinCombination: false });
  }


  render() {
    return (
      <SafeAreaProvider style={{ flex: 1 }}>
        <Box display='flex' flex='1' flexDirection='column' >

          <DynamicTabView
            data={this.state.data}
            renderTab={this._renderItem}
            defaultIndex={this.state.defaultIndex}
            containerStyle={styles.container}
            headerBackgroundColor={'white'}
            headerTextStyle={styles.headerText}
            onChangeTab={this.onChangeTab}
            headerUnderlayColor={'red'}

          />
          {this.state.renderCoinCombination &&
            this.renderCoinCombinationDialog()}
          <Loading isLoading={this.state.isLoading} />
        </Box>

      </SafeAreaProvider>


    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  // `headerContainer: {
  //   marginTop: 16
  // },`
  headerText: {
    color: 'red'
  },
  tabBar: {
    flexDirection: 'row',
    paddingTop: 50,
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 10
  },
  tabBarText: {

    color: 'black',

  },
  bg: {
    flex: 1,
    backgroundColor: 'yellow',

  },
  tabStyle: {

    height: 40,
    //alignSelf: 'center',
    backgroundColor: '#e02d2e',
    //borderRadius: 5,
    flexDirection: 'row',
  },
  tabBar: {
    flexDirection: 'row',
    paddingTop: 10,
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },

  headerText: {
    color: 'red'
  },
  headerTextInactive: {
    color: 'black'
  },
  tabBarText: {

    color: 'black',

  },
  tabBarScrollView: {

    alignItems: 'center',
    padding: 0,

  },
});