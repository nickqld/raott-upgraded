import React, {Component} from 'react';
import {ButtonPress} from './common';
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
  BackHandler,
  StyleSheet,
  Platform,
  // AsyncStorage
} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Container, Content, Item, Input, Icon} from 'native-base';
import {NativeBaseProvider, Box, Center} from 'native-base';
// import CreateAccount from "./CreateAccount/";
// import ForgotPassword from "./ForgotPassword/";
import Loading from './Loading/';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import Otp from "./Otp";
import {InternetCheck, TimeOutError} from './common/';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
// import PushNotificationIOS from '@react-native-community/push-notification-ios';
// import PushNotification from "react-native-push-notification";
/*import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType,
} from 'react-native-fcm';
*/
//import {registerAppListener} from '../Listeners';
// import firebase from 'react-native-firebase';
// import firebase from '@react-native-firebase/app';
// import firebase from '@react-native-firebase/app'
import {requestPermission} from 'react-native-android-permissions';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import SafeAreaView from 'react-native-safe-area-view';
class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: '',
      pwd: '',
      isLoading: false,
      token: '',
      isRemeberMe: false,
    };

    this._backAndroidPress = this.backAndroidPress.bind(this);
    // we are getting initial username and password to set isRememberMe state
    AsyncStorage.getItem('username')
      .then((username) => {
        if (username !== undefined && username !== null && username !== '') {
          this.setState({uname: username});
        }
        AsyncStorage.getItem('password')
          .then((password) => {
            if (
              password !== undefined &&
              password !== null &&
              password !== ''
            ) {
              isRemeberMe = this.state.isRemeberMe;
              if (this.state.uname !== '') {
                isRemeberMe = true;
              }
              this.setState({pwd: password, isRemeberMe});
            }
          })
          .done();
      })
      .done();
    this.handleRememberMe = this.handleRememberMe.bind(this);
    this.loginButtonPress = this.loginButtonPress.bind(this);

    // this.loginButtonPress = .debounce(this.loginButtonPress.bind(this), 1000, {
    //   leading: true,
    //   trailing: false
    // });

    InternetCheck();
  }

  backAndroidPress() {
    this.props.navigation.goBack();
    return true;
  }

  componentDidMount() {
    console.log('akbhai helsodafsadfsfna efoaweb foawefn');

    // Must be outside of any component LifeCycle (such as `componentDidMount`).
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log('akbhai TOKssEN:', token);
        // SELF.storingdeviceId(token);
        AsyncStorage.setItem(
          '@deviceid:key',
          JSON.stringify(token['token'] || {}),
        );

        // try {
        //   AsyncStorage.setItem('@deviceid:key', JSON.stringify(token || {}));
        // } catch (error) {

        //   console.log('Error saving data token');
        // }
        // this.setState({ token: token });

        // this.setState({
        //   isRemeberMe: !this.state.isRemeberMe,
        // });
      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log('akbhai NOTIFICATION:', notification);

        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log('akbhai ACTION:', notification.action);
        console.log('akbhai NOTIFICATION:', notification);

        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });

    /*FCM.requestPermissions(); // for iOS
    FCM.getFCMToken().then(token => {
      this.storingdeviceId(token);
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async notif => {
      // do some component related stuff
    }); */
    //started
    // Build a channel

    // const channel = new firebase.notifications.Android.Channel(
    //   'test-channel',
    //   'Test Channel',
    //   firebase.notifications.Android.Importance.Max).setDescription('My apps test channel');

    // // Create the channel
    // firebase.notifications().android.createChannel(channel);

    // //registerAppListener();

    // this.notificationListener = firebase
    //   .notifications()
    //   .onNotification(notification => {
    //     firebase.notifications().displayNotification(notification);
    //   });

    // this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(token => {
    //   console.log('TOKEN (refreshUnsubscribe)', token);
    // });

    // this.messageListener = firebase.messaging().onMessage(message => {
    //   this.displayNotificationFromCustomData(message);
    // });

    // firebase.notifications().getInitialNotification();
    // this.checkPermission;

    // firebase
    //   .messaging()
    //   .getToken()
    //   .then(token => {
    //     console.log('TOKEN (getFCMToken)', token);
    //     this.storingdeviceId(token);
    //   });
    //end

    // this.checkPermission();
    // this.createNotificationListeners();

    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }
  //  displayNotificationFromCustomData(message) {
  //   if (message.data && message.data.title) {
  //     let notification = new firebase.notifications.Notification();
  //     notification = notification
  //       .setTitle(message.data.title)
  //       .setBody(message.data.body)
  //       .setData(message.data);

  //     notification.android.setPriority(
  //       firebase.notifications.Android.Priority.High,
  //     );
  //     notification.android.setChannelId('test-channel');
  //     firebase.notifications().displayNotification(notification);
  //   }
  // }
  // checkPermission = async () => {
  //   // user has completed product tour_end
  //   if (!(await firebase.messaging().hasPermission())) {
  //     try {
  //       await firebase.messaging().requestPermission();
  //     } catch (e) {
  //       alert("Failed to grant permission")
  //     }
  //   }
  // };

  componentWillUnmount() {
    // stop listening for events
    // this.notificationListener.remove();

    //    this.notificationListener();
    //  this.onTokenRefreshListener();
    // this.messageListener();

    // this.notificationListener;
    // this.notificationOpenedListener;

    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  // async checkPermission() {
  //   console.log('ak checkPermission');

  //   firebase
  //     .messaging()
  //     .hasPermission()
  //     .then(enabled => {
  //       if (enabled) {
  //         console.log('ak aa gya');
  //         // User has permissions

  //         this.getFcmToken(); // const fcmToken = await firebase.messaging().getToken();

  //       } else {
  //         console.log('ak yarr');
  //         // User doesn't have permission
  //         firebase
  //           .messaging()
  //           .requestPermission()
  //           .then(() => {
  //             // User has authorized
  //             this.getFcmToken(); // const fcmToken = await firebase.messaging().getToken();
  //           })
  //           .catch(error => {
  //             // User has rejected permissions
  //             console.log(
  //               'PERMISSION REQUEST :: notification permission rejected',
  //             );
  //           });
  //       }
  //     });

  //     console.log('ak abey');

  //   // const enabled = await firebase.messaging().hasPermission();

  //   // console.log('ak aa gya');

  //   // if (enabled) {
  //   //   console.log('ak gusa');

  //   //   this.getToken();
  //   // } else {
  //   //   console.log('ak ni gusa');

  //   //   this.requestPermission();
  //   // }

  //   // console.log('ak yarr');

  // }

  // async getToken() {
  //   let fcmToken = await AsyncStorage.getItem('fcmToken');
  //   if (!fcmToken) {
  //     fcmToken = await firebase.messaging().getToken();
  //     if (fcmToken) {
  //       console.log('fcmToken:', fcmToken);
  //       await AsyncStorage.setItem('fcmToken', fcmToken);
  //     }
  //   }
  // }

  // async requestPermission() {
  //   console.log('jai ho');

  //   try {
  //     await firebase.messaging().requestPermission();
  //     // If user allow Push Notification
  //     this.getToken();
  //   } catch (error) {
  //     // If user do not allow Push Notification
  //     console.log('Rejected');
  //   }
  // }

  // async createNotificationListeners() {

  //   // If your app is in Foreground

  //   this.notificationListener = firebase.notifications().onNotification((notification) => {
  //     const localNotification = new firebase.notifications.Notification({
  //       show_in_foreground: true,
  //     })
  //       .setNotificationId(notification.notificationId)
  //       .setTitle(notification.title)
  //       .setBody(notification.body)

  //     firebase.notifications()
  //       .displayNotification(localNotification)
  //       .catch(err => console.error(err));
  //   });

  //   //If your app is in background

  //   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
  //     const { title, body } = notificationOpen.notification;
  //     console.log('onNotificationOpened:');
  //     Alert.alert(title, body)
  //   });

  //   // If your app is closed

  //   const notificationOpen = await firebase.notifications().getInitialNotification();
  //   if (notificationOpen) {
  //     console.log('getInitialNotification:');
  //   }

  //   // For data only payload in foreground

  //   this.messageListener = firebase.messaging().onMessage
  //     ((message) => {
  //       //process data message
  //       console.log("Message", JSON.stringify(message));
  //     });
  // }

  handleRememberMe() {
    this.setState({
      isRemeberMe: !this.state.isRemeberMe,
    });
  }

  render() {
    const checkboxcolor = this.state.isRemeberMe ? '#e02d2e' : '#b9bbc1';
    const {
      rememberText,
      mainContainerStyle,
      scrollContainer,
      logoStyle,
      usernameItem,
      iconStyle,
      textInputStyle,
      passwordItem,
      checkboxView,
      checkboxButton,
      forgetPasswordView,
      forgotText,
      bottomMain,
      signUpText,
      textRaottStyle,
    } = styles;
    return (
      <NativeBaseProvider>
        <SafeAreaProvider width={deviceWidth}>
          <Box
            display="flex"
            flex="1"
            backgroundColor="white"
            flexDirection="column"
            justifyContent="space-around">
            {/* <Center flex={1}> */}
            {/* <Content contentContainerStyle={scrollContainer}> */}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('HomeScreen')}>
              <Image
                style={logoStyle}
                source={require('../img/raott_logo.png')}
              />
            </TouchableOpacity>

            <Box
              margin="5"
              display="flex"
              flexDirection="column"
              justifyContent="space-between">
              {/* <View> */}

              <Input
                placeholder="User Name"
                placeholderTextColor="#c2c2c2"
                backgroundColor="#f7f7f7"
                variant="Filled"
                fontSize="13"
                fontWeight="600"
                minHeight="10"
                onChangeText={(uname) => this.setState({uname})}
                value={this.state.uname}
                autoCorrect={false}
                blurOnSubmit={true}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="person" />}
                    style={styles.iconStyleNew}
                  />
                }
              />
              <Input
                placeholder="Password"
                secureTextEntry={this.state.passwordView ? false : true}
                placeholderTextColor="#c2c2c2"
                backgroundColor="#f7f7f7"
                variant="Filled"
                fontSize="13"
                minHeight="10"
                marginTop="3"
                fontWeight="600"
                onChangeText={(pwd) => this.setState({pwd})}
                value={this.state.pwd}
                blurOnSubmit={true}
                InputLeftElement={
                  <Icon
                    as={<MaterialIcons name="lock" />}
                    style={styles.iconStyleNew}
                  />
                }
                InputRightElement={
                  <Icon
                    as={
                      <MaterialCommunityIcons
                        name={this.state.passwordView ? 'eye' : 'eye-off'}
                      />
                    }
                    style={styles.iconStyleNew}
                    onPress={() =>
                      this.setState({passwordView: !this.state.passwordView})
                    }
                  />
                }
              />

              {/* </Item> */}
              <View style={checkboxView}>
                <TouchableOpacity
                  activeOpacity={1.0}
                  onPress={this.handleRememberMe}
                  style={checkboxButton}>
                  <MaterialIcons
                    name={
                      this.state.isRemeberMe
                        ? 'check-box'
                        : 'check-box-outline-blank'
                    }
                    size={24}
                    style={{color: checkboxcolor}}
                  />
                  <Text style={rememberText}>Remember Me</Text>
                </TouchableOpacity>
              </View>
              <ButtonPress onPress={() => this.loginButtonPress()}>
                {' '}
                Sign In{' '}
              </ButtonPress>

              <View style={forgetPasswordView}>
                <TouchableOpacity
                  onPress={
                    () => this.props.navigation.navigate('ForgotPassword')
                    // this.props.navigation.navigate('ForgotPassword')
                    // this.props.navigation.navigate('Otp', { userid: '426' })
                  }>
                  <Text style={forgotText}>Forgot Password ?</Text>
                </TouchableOpacity>
              </View>
              {/* </View> https://i.diawi.com/dYFUBg https://testflight.apple.com/join/dXMU5UBB  */}
            </Box>
            {/* <TouchableOpacity style={{ width: 120, height: 120, alignSelf: 'center' }} onPress={() => this.props.navigation.navigate("Category", { screen: "LoginForm" })} >
                    <Image style={{ width: 120, height: 120, alignSelf: 'center' }} source={require('../../img/treasureHunt.png')}></Image>
                </TouchableOpacity> */}
            <Box>
              <View style={bottomMain}>
                <Text style={signUpText}>Sign Up to</Text>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('CreateAccount')
                  }>
                  <Text style={textRaottStyle}>Raotts</Text>
                </TouchableOpacity>
              </View>
              {/* {this.state.isLoading && (
                <Loading isLoading={this.state.isLoading} />
              )} */}

              {/* </Content> */}
            </Box>
            <Loading isLoading={this.state.isLoading} />
          </Box>
        </SafeAreaProvider>
      </NativeBaseProvider>
    );
  }

  async loginButtonPress() {
    const SELF = this;
    //Keyboard.dismiss();

    SELF.setState({
      isLoading: true,
    });
    SELF.setState({
      isLoading: false,
    });

    // if username or password are empty

    if (this.state.uname == '' || this.state.pwd == '') {
      Alert.alert('Invalid Credential', 'Please enter Email & Password');
      SELF.setState({isLoading: false, uname: '', pwd: ''});
    }
    // checking the validity of username and password
    else {
      // if credentials were already filled bcoz of remember state
      if (this.state.isRemeberMe) {
        try {
          AsyncStorage.setItem('username', SELF.state.uname);
          AsyncStorage.setItem('password', SELF.state.pwd);
          // await AsyncStorage.setItem(
          //   '@deviceid:key',
          //   JSON.stringify(SELF.state.token || {}),
          // );
        } catch (error) {
          // Error saving data
        }
      }

      axios.interceptors.request.use((request) => {
        console.log('Starting Request', JSON.stringify(request, null, 2));
        return request;
      });
      let deviceId = '';
      deviceId = await AsyncStorage.getItem('@deviceid:key').then((data) =>
        data ? JSON.parse(data) : null,
      );
      deviceId?.replace(/[^a-zA-Z ]/g, '');
      // alert(SELF.state.uname + SELF.state.pwd)
      axios({
        url: 'https://raott.com/api/Auth/authenticate',
        type: 'POST',
        method: 'POST',
        timeout: 30000,
        data: {
          username: SELF.state.uname,
          password: SELF.state.pwd,
          deviceid: deviceId,
        },
      })
        .then(function (response) {
          // alert(response.data);
          console.log(response.data);
          if (response.data.status == 'success') {
            SELF.setState({isLoading: false});
            try {
              AsyncStorage.setItem('apiToken', response.data.apitoken);
            } catch (error) {
              // Error saving data
            }
            // SELF.props.navigation.navigate('LandingPage');
            SELF.props.navigation.push('LandingPage');
          } else {
            if (response.data.error == 'Account not confirmed') {
              SELF.requestOtp(response.data.userid);
              SELF.setState({isLoading: false});
            } else if (response.data.error == 'Authentication failure!') {
              Alert.alert('Invalid Credential', response.data.error);
              SELF.setState({uname: '', isLoading: false, pwd: ''});
            } else {
              Alert.alert('Error', 'Username or Password is Invalid');
              SELF.setState({uname: '', isLoading: false, pwd: ''});
            }
          }
        })
        .catch(function (error) {
          SELF.setState({isLoading: false});
          TimeOutError(error, () => SELF.loginButtonPress());
        });
    }
  }
  requestOtp(userid) {
    let fetchUserId = userid;
    const SELF = this;
    axios
      .get('https://raott.com/api/Auth/request_otp/' + userid, {
        timeout: 30000,
      })
      .then(function (response) {
        // console.log('response request_otp');
        // console.log(response);
        if (response.data.status == 'success') {
          Alert.alert(
            '',
            'We have sent a verification code to your registered email address. Please enter that code to complete registration.',
            [
              {
                text: 'OK',
                onPress: () =>
                  SELF.props.navigation.navigate('Otp', {userid: userid}),
              },
            ],
            {cancelable: false},
          );
        } else {
          Alert.alert(response.data.status, response.data.error);
        }
      })
      .catch(function (error) {
        // console.log(error);
        SELF.setState({isLoading: false});
        TimeOutError(error, () => SELF.requestOtp(fetchUserId));
      });
  }
  async storingdeviceId(token) {
    try {
      await AsyncStorage.setItem('@deviceid:key', JSON.stringify(token || {}));
    } catch (error) {
      console.log('Error saving data token');
    }
    this.setState({token: token});
  }
}

const styles = StyleSheet.create({
  mainContainerStyle: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'space-around',
  },
  scrollContainer: {
    justifyContent: 'space-around',
    height: deviceHeight,
  },
  textInputStyle: {
    alignSelf: 'center',
    fontSize: 13,
    fontWeight: '600',
  },
  usernameItem: {
    alignSelf: 'center',
    marginTop: 20,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  passwordItem: {
    alignSelf: 'center',
    marginTop: 20,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  errorTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: 'red',
  },
  bottomMain: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  forgotText: {
    width: deviceWidth - 40,
    height: 30,
    marginTop: 10,
    textAlign: 'center',
    fontSize: 13,
    fontWeight: '600',
    backgroundColor: 'white',
    color: '#c2c2c2',
  },
  signUpText: {
    backgroundColor: 'white',
    color: '#c2c2c2',
    fontSize: 13,
    fontWeight: '600',
  },
  rememberText: {
    width: 120,
    marginLeft: 10,
    color: '#c2c2c2',
    backgroundColor: 'white',
    fontSize: 13,
    fontWeight: '600',
  },
  logoStyle: {
    width: 120,
    height: 120,
    alignSelf: 'center',
    marginTop: 15,
  },
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10,
  },
  iconStyleNew: {
    color: '#e02d2e',
    marginLeft: 10,
    marginRight: 10,
    size: 15,
  },
  checkboxView: {
    width: deviceWidth - 40,
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 10,
  },
  checkboxButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgetPasswordView: {
    alignSelf: 'center',
    marginTop: 10,
  },
  textRaottStyle: {
    color: '#e02d2e',
    marginLeft: 5,
    backgroundColor: 'white',
    fontSize: 13,
    fontWeight: '600',
  },

  container1: {
    flex: 1, // You should only need this
    height: '100%', // But these wouldn't hurt.
    width: '100%',
  },
});

LoginForm.navigationOptions = {
  header: null,
  gesturesEnabled: false,
};

export default LoginForm;
