import React from 'react';
import { View, ActivityIndicator, Dimensions, Text, Button, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Dialog = (props, children) => {
  return (
    <View style={styles.containerDialog}>
      <View style={styles.innerContainerDialog}>

        <View style={styles.messageHeaderView}>
          <Text style={styles.headerTextStyle}> {props.titleText}</Text>
          <TouchableOpacity onPress={() => props.method()}>
            <Image style={styles.cancelImageStyle} source={require('../../../img/closeicon.png')}>
            </Image>
          </TouchableOpacity>
        </View>

        <Text style={styles.msgTextStyle}>{props.msg}</Text>

        <TouchableOpacity onPress={() => props.method()}>
          <Text style={styles.okButtonTextStyle2}> OK </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

Dialog.propTypes = {
  size: PropTypes.string,
};

Dialog.defaultProps = {
  size: 'large',
};

export default Dialog;
