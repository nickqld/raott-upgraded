import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  NetInfo,
  BackHandler,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import FriendsDetail from './FriendsDetail.js';
import styles from './styles';
import axios from 'axios';
import Loading from '../Loading/';
import DialogNotMember from './DialogNotMember.js';
import { connect } from 'react-redux';
import { InternetCheck, TimeOutError } from '../common/';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SafeAreaView from 'react-native-safe-area-view';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
class FriendsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      friends: [],
      isLoading: true,
      count: '1',
      loadButton: true,
      showDialog: false,
      clickedFriend: '',
      showDialogGuestUserStatus: false,
      timeoutDone: false,
      nodataText: false,
    };
    headerTitle = 'Transfer World Raott Coins to ';
    screen = 'FriendList';

    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
    this.getCoinsToFriends();
  }

  onCardClick(singleFriend) {
    this.props.navigation.navigate('TransferCoins', {
      singleFriend,
      onRefresh: this.onRefresh,
    });
  }

  onTransferClick(name, fid, membership_status, button) {
    // console.log(button,membership_status, '*****BUTTON ON FRIEND LIST ****')

    if (button == 'Transfer') {
      if (this.props.user && this.props.user.membership_status == 0) {
        this.setState({ showDialogGuestUserStatus: true, clickedFriend: name });
      } else if (membership_status == '0') {
        this.setState({ showDialog: true, clickedFriend: name });
      } else {
        this.props.navigation.navigate('RoattCoins', {
          headerTitle: headerTitle + name,
          screen: 'FriendList',
          fid: fid,
        });
        // console.log(name,'name')
      }
    } else if (button == 'Accept') {
      this.acceptRequest(fid, name);
    } else {
    }
  }

  addfriend() {
    this.props.navigation.navigate('TransferCoinToAny', {
      navigationFrom: 'FriendList',
      onRefresh: this.onRefresh,
    });
  }

  onRefresh = data => {
    // console.warn('onRefresh')
    this.getCoinsToFriends();
  };
  acceptRequest(id, name) {
    const SELF = this;
    if (SELF.state.isLoading) {
      SELF.setState({ isLoading: true });
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        // console.log(id)

        axios({
          url: 'https://raott.com/api/v2/userConfirmFriend/' + id,
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {
            // console.log(response);

            if (
              response.data.status == 'success' ||
              response.data.status == 'succes'
            ) {
              // SELF.setState({ clickedFriend: name })
              Alert.alert('Success', ' Request accepted');
              SELF.setState({ isLoading: true });
              SELF.getCoinsToFriends();
            } else {
              Alert.alert('Error', 'Unable to Accept request');
            }
          })
          .catch(function (error) {
            // console.log(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.acceptRequest(id, name));
          });
      })
      .done(); // ending statement of asyncstorage
  }

  renderDialog() {
    return (
      <DialogNotMember
        method={() => this.toggleModal()}
        titleText=" Not a Member"
        msg={`You cannot transfer World Roatt to ${this.state.clickedFriend}. ${this.state.clickedFriend} is still a guest user of Raotts. Only Full members can send and receive World Raott Coins.  `}
      />
    );
  }

  toggleModal() {
    this.setState({
      showDialog: !this.state.showDialog,
    });
  }

  renderDialogGuestUserStatus() {
    return (
      <DialogNotMember
        method={() => this.toggleModalGuestUserStatus()}
        titleText="Error Transfer Coins"
        msg={`You cannot transfer World Roatt to ${this.state.clickedFriend}.You are still a guest user of Raotts. Only Full members can send and receive World Raott Coins. You have to invite at least one friend and he/she must join Raotts using your invite code for you to become a full member.  `}
      />
    );
  }

  toggleModalGuestUserStatus() {
    this.setState({
      showDialogGuestUserStatus: !this.state.showDialogGuestUserStatus,
    });
  }

  componentDidMount() {
    console.log('Category page', this.props);
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  renderFriends() {
    if (this.state.friends && this.state.friends.length > 0) {
      return this.state.friends.map((singleFriend, i) => (
        <FriendsDetail
          key={i}
          singleFriend={singleFriend}
          onCardClick={singleFriend => this.onCardClick(singleFriend)}
          onTransferClick={(name, fid, membership_status, button) =>
            this.onTransferClick(name, fid, membership_status, button)
          }
        />
      ));
    } else {
      return <View />;
    }
  }

  onLoadButton() {
    this.setState({ count: parseInt(this.state.count) + 1 });
    this.getCoinsToFriends();
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }

  render() {
    return (
      <SafeAreaProvider style={{ flex: 1 }}>

        {/* <Container style={styles.container}> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View style={styles.headerStyle}>
            <TouchableOpacity
              onPress={() => this.goBack()}
              style={styles.headerLeftBtn}>
              {/* <Icon style={{color: 'white'}} name="arrow-back" /> */}
              <MaterialCommunityIcons name="arrow-left" size={24} color="white" />
            </TouchableOpacity>

            <View transparent style={styles.headerTitle}>
              <Text
                style={{
                  color: 'white',
                  textAlign: 'center',
                  alignSelf: 'center',
                }}>
                FRIENDS LIST
              </Text>
            </View>

            {/* <TouchableOpacity
              style={styles.headerRightBtn}
              onPress={() => this.addfriend()}> */}
            {/* <Icon transparent style={styles.rightIcon} name="md-contact" />
              <Icon transparent style={styles.rightAddIcon} name="md-add" /> */}

            {/* <MaterialCommunityIcons name="contacts" size={20} color='#e02d2e' />
              <MaterialCommunityIcons name="plus" size={20} color='#e02d2e'  />
            </TouchableOpacity> */}
          </View>

          <ScrollView>{this.renderFriends()}</ScrollView>

          {/* {this.state.loadButton &&
                    <TouchableOpacity onPress={() => this.onLoadButton()} >
                        <View transparent style={styles.headerTitle}>

                            <Text style={{ color: "black", textAlign: 'center' }}>Load More</Text>

                        </View>
                    </TouchableOpacity>} */}

          {this.state.showDialog && this.renderDialog()}
          {this.state.showDialogGuestUserStatus &&
            this.renderDialogGuestUserStatus()}
          {this.state.nodataText && (
            <Text
              style={{
                textAlign: 'center',
                alignSelf: 'center',
                margin: 10,
                position: 'absolute',
                top: 65,
              }}>
              {' '}
              No data available in table{' '}
            </Text>
          )}

          <Loading isLoading={this.state.isLoading} />
          {/* </Container> */}
        </Box>
      </SafeAreaProvider>
    );
  }

  getCoinsToFriends() {
    const SELF = this;
    // console.log(SELF.state.count);
    if (!SELF.state.isLoading) {
      SELF.setState({
        isLoading: true,
        timeoutDone: false,
      });
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);

        axios({
          // url: 'https://raott.com/api/v2/screenCoinsToFriends/' + SELF.state.count + '/5',
          url: 'https://raott.com/api/v2/screenCoinsToFriends/',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {
            SELF.setState({
              isLoading: false,
            });
            // console.log('userFriends************  ');
            // console.log(response);
            console.log('response - -- -' + JSON.stringify(response));
            let friendList = [];
            Object.keys(response.data.friends).map(key => {
              friendList.push(response.data.friends[key]);
            });

            // console.log(friendList.length,' ',SELF.state.count)
            if (friendList.length == 0) {
              SELF.setState({
                loadButton: false,
              });
            }

            if (SELF.state.count > 1) {
              let friendsArray = SELF.state.friends;
              friendList.map(friend => {
                friendsArray.push(friend);
              });
              SELF.setState({ friends: friendsArray, nodataText: false });
            } else {
              if (friendList.length > 0) {
                SELF.setState({ friends: friendList, nodataText: false });
              } else {
                SELF.setState({ nodataText: true, friends: [] });
              }
            }
          })
          .catch(function (error) {
            // console.log(error);
            if (!SELF.state.timeoutDone) {
              TimeOutError(error, () => SELF.getCoinsToFriends());
            }

            SELF.setState({ isLoading: false, timeoutDone: true });
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

function bindAction(dispatch) {
  return {};
}

const mapStateToProps = state => ({
  user: state.user.data,
});
export default connect(mapStateToProps, bindAction)(FriendsList);
