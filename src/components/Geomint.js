import React, { Component } from 'react';
import { ButtonPress } from './common';
import {
  Text,
  View,
  Dimensions,
  Image,
  ScrollView,
  BackHandler,
} from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  ListItem,
  List,
  Card,
  CardItem,
  Item,
  Input,
} from 'native-base';
import { InternetCheck } from './common/';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';

class Geomint extends Component {
  constructor(props) {
    super(props);
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.props.navigation.navigate('LandingPage');
    return true;
  }
  render() {
    return (
      <SafeAreaProvider style={{ flex: 1 }}>
        {/* <Container style={styles.mainContainer}>
        
        <View>
            <Header style={styles.headerStyle}>
              <Left style={{flex: 1}}>
                <Button
                  transparent
                  onPress={() => this.props.navigation.openDrawer()}>
                  <Icon name="menu" style={{fontSize: 25, color: '#e02d2e'}} />
                </Button>
              </Left>
              <Body style={{flex: 10, alignItems: 'center'}}>
                <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                  <Title style={{color: '#e02d2e', lineHeight: 30}}>
                    What is GeoMint
                  </Title>
                  <Text
                    style={{fontSize: 11, lineHeight: 18, color: '#e02d2e'}}>
                    {'\u00AE'}
                  </Text>
                  <Title style={{color: '#e02d2e', lineHeight: 30}}> ?</Title>
                </View>
              </Body>
            </Header>
          </View>
        */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View
            style={{
              width: deviceWidth,
              backgroundColor: '#e7e4e5',
              paddingHorizontal: 10,
              flexDirection: 'row'
            }}>
            <Button
              transparent
              size="10"
              colorScheme="transparent"
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon
                as={<MaterialIcons name="menu" />}
                style={{ fontSize: 25, color: '#e02d2e' }}
              />
            </Button>
            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', alignSelf: 'stretch' }} >
              <Text style={{ color: '#e02d2e', lineHeight: 30 }}>
                What is GeoMint
              </Text>
              <Text
                style={{ fontSize: 11, lineHeight: 18, color: '#e02d2e' }}>
                {'\u00AE'}
              </Text>
              <Text style={{ color: '#e02d2e', lineHeight: 30 }}> ?</Text>
            </View>
          </View>




          <ScrollView style={{ flex: 1 }}>
            <View style={styles.textViewStyle}>
              <Text style={styles.textHeaderStyle}>What is GeoMint?</Text>
              <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <Text style={styles.textBoldColor}>Geo</Text>
                <Text style={styles.imageText}> - Earth, Land</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'flex-start',
                  marginBottom: 20,
                }}>
                <Text style={styles.textBoldColor}>Mint</Text>
                <Text style={styles.imageText}>
                  {' '}
                  - Produced/Created for the first time
                </Text>
              </View>
              <Text style={styles.textStyle}>
                GeoMint is finding and claiming valuable Digital Assets,
                Treasures and Collectibles hidden in different geographical
                locations, by playing the World’s Biggest Digital Treasure Hunt.
              </Text>
              <Text style={styles.textStyle}>
                These Digital Assets, Treasures and Collectibles are
                geographically generated and can never be duplicated, lost,
                stolen or copied. You become the true digital owner of these
                assets once you find them.
              </Text>
              <Text style={styles.textStyle}>
                We keep a meticulous record of all our members Digital Assets,
                Treasures and Collectibles.
              </Text>
              <Text style={styles.textStyle}>
                Our app is the ONLY place where you can GeoMint valuable Digital
                Assets, Treasures and Collectibles.
              </Text>
              <Text style={styles.textHeaderStyle}>
                How are GeoMinted Digital Assets, Treasures and Collectibles
                valued?
              </Text>
              <Text style={styles.textStyle}>
                The rarer the Digital Asset/Collectibles, the higher the value.
                Certain Digital Assets and Collectibles can be GeoMinted only
                once and will never be produced again. This increases the value
                of the asset as it is very rare and there is a huge demand for
                it.
              </Text>
              <Text style={styles.textHeaderStyle}>
                How many World Raott Coins are there to be GeoMinted?
              </Text>
              <Text style={styles.textStyle}>
                There are exactly 148,940,000 coins to be GeoMinted. This is the
                approximate land surface area of earth in square kilometres.
                GeoMinting World Raott Coins in extremely difficult and rare.
              </Text>
            </View>
          </ScrollView>
          {/* </Container> */}

        </Box>
      </SafeAreaProvider>
    );
  }
}
const styles = {
  mainContainer: {
    flex: 1,
    width: deviceWidth,
    backgroundColor: 'white',
  },
  headerStyle: {
    backgroundColor: '#e7e4e5',
  },
  textViewStyle: {
    flex: 1,
    margin: 16,
  },
  textHeaderStyle: {
    color: '#e02d2e',
    fontSize: 18,
    lineHeight: 24,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 20,
  },
  textStyle: {
    fontSize: 14,
    lineHeight: 18,
    // backgroundColor: 'yellow',
    marginBottom: 10,
  },
  textBoldColor: {
    color: '#e02d2e',
    fontWeight: 'bold',
  },
};

Geomint.navigationOptions = {
  header: null,
  gesturesEnabled: false,
  drawerLockMode: 'locked-closed',
};
export default Geomint;
