import React, {Component} from 'react';
// import { ButtonPress } from './common'
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  Button,
  Dimensions,
  Alert,
  Platform,
  StatusBar,
  BackHandler,
} from 'react-native';

import {InternetCheck} from './common/';
// import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';
// import { requestPermission } from 'react-native-android-permissions';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';
import {connect} from 'react-redux';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: '',
      pwd: '',
      isLoading: false,
      token: '',
      isRemeberMe: false,
    };
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
  }

  backAndroidPress() {
    BackHandler.exitApp();
    return true;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={styles.main}>
          {/* <StatusBar
                    backgroundColor="#e02d2e"
                    barStyle="dark-content"
                /> */}
          <View style={styles.headerViewStyle}>
            <Text style={styles.headerTextStyle}>
              World's Biggest Digital Treasure Hunt
            </Text>
          </View>

          <View style={styles.innerViewStyle}>
            <View style={styles.imageViewStyle}>
              <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                <Text style={[styles.imageText, {lineHeight: 30}]}>
                  GeoMint
                </Text>
                <Text style={{fontSize: 11, lineHeight: 18, color: '#e02d2e'}}>
                  {'\u00AE'}{' '}
                </Text>
                <Text style={[styles.imageText, {lineHeight: 30}]}>
                  Digital Assets and Treasures
                </Text>
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('Category', {
                    screen: 'LoginForm',
                  })
                }>
                <Image
                  style={styles.logoStyle}
                  source={require('../img/raott_logo.png')}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.imageViewStyle}>
              <Text style={[styles.imageText, {textAlign: 'center'}]}>
                Treasure Chest
              </Text>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('Deals', {
                    screen: 'HomeScreen',
                    onSelect: null,
                  })
                }>
                <Image
                  style={styles.logoCupStyle}
                  source={require('../../img/rsz_gold_cup.png')}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.buttonViewStyle}>
              <TouchableOpacity
                style={styles.signUpButton}
                onPress={() => this.props.navigation.navigate('CreateAccount')}>
                <Text style={styles.signUpText}>SIGN UP</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.logInButton}
                onPress={() => this.props.navigation.navigate('LoginForm')}>
                <Text style={styles.logInText}>LOG IN</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = {
  main: {
    backgroundColor: '#e02d2e',
    flex: 1,
    flexDirection: 'column',
  },
  usernameInput: {
    alignSelf: 'center',
    fontSize: 11,
  },
  usernameItem: {
    alignSelf: 'center',
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  passwordItem: {
    alignSelf: 'center',
    marginTop: 20,
    width: deviceWidth - 40,
    height: 40,
    backgroundColor: '#f7f7f7',
    borderRadius: 5,
  },
  errorTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: 'red',
  },
  bottomMain: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  forgotText: {
    width: deviceWidth - 40,
    height: 30,
    marginTop: 10,
    textAlign: 'center',
    backgroundColor: 'white',
    color: '#c2c2c2',
  },
  signUpText: {
    backgroundColor: 'white',
    color: '#c2c2c2',
  },
  rememberText: {
    width: 120,
    height: 20,
    marginLeft: 10,
    color: '#c2c2c2',
    backgroundColor: 'white',
  },
  logoStyle: {
    width: 120,
    height: 120,
    marginTop: 10,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  logoCupStyle: {
    width: 140,
    height: 140,
    marginTop: 10,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10,
  },
  headerViewStyle: {
    height: 64,
    width: deviceWidth,
    backgroundColor: '#e02d2e',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  headerTextStyle: {
    color: 'white',
    padding: 10,
    fontSize: 20,
    textAlign: 'center',
  },
  innerViewStyle: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'space-around',
  },
  imageViewStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  imageText: {
    color: '#e02d2e',
    fontWeight: 'bold',
  },
  buttonViewStyle: {
    width: deviceWidth,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  signUpButton: {
    borderRadius: 30,
    borderColor: '#e02d2e',
    borderWidth: 0.5,
    backgroundColor: '#e02d2e',
  },
  logInButton: {
    backgroundColor: 'white',
    borderColor: '#e02d2e',
    borderRadius: 30,
    borderWidth: 0.9,
  },
  signUpText: {
    color: 'white',
    paddingHorizontal: 30,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0)',
    fontWeight: 'bold',
  },
  logInText: {
    color: '#e02d2e',
    paddingHorizontal: 40,
    paddingVertical: 10,
    backgroundColor: 'rgba(0,0,0,0)',
    fontWeight: 'bold',
  },
};

HomeScreen.navigationOptions = {
  header: null,
  gesturesEnabled: false,
  drawerLockMode: 'locked-closed',
};

const mapStateToProps = state => ({
  user: state.user.data,
});

export default connect(mapStateToProps)(HomeScreen);
