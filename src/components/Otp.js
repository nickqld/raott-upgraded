import React, { Component } from "react";
import { ButtonPress } from './common';
import {
  Text,
  View,
  Dimensions,
  Platform,
  Alert,
  TouchableOpacity,
} from "react-native";
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import axios from 'axios';
import {
  Item,
  Container,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Input,
} from "native-base";

import Loading from './Loading/';
import LoginForm from './LoginForm';
import CreateAccount from './CreateAccount';

import { InternetCheck, TimeOutError } from './common/';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
/*import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType,
} from 'react-native-fcm';*/
import { registerAppListener } from '../Listeners';
// import firebase from '@react-native-firebase/app';
import SafeAreaView from 'react-native-safe-area-view';

export default class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      isLoading: false,
      token: '',
      userid: this.props.navigation.state.params.userid,
    };

    InternetCheck();
  }

  render() {
    const { iconStyle, container, contentStyle, mainView, headerView } = styles;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Container style={container}>
          {/* <Text style={{ color: '#c2c2c2' }}> OTP</Text> */}

          <View style={styles.headerStyle}>
            <TouchableOpacity
              style={styles.goBackButton}
              onPress={() => this.props.navigation.goBack()}>
              {/* <Icon style={{ color: 'white' }} name="arrow-back" /> */}
              <MaterialCommunityIcons name="arrow-left" size={24} color='white' />

            </TouchableOpacity>
            <View transparent style={styles.headerText}>
              <Text style={{ color: 'white' }}>OTP</Text>
            </View>

          </View>
          {/* <Icon active name="ios-log-in" style={iconStyle} />
          <Icon active name="ios-log-in" style={iconStyle} />
          <Icon active name="ios-log-in" style={iconStyle} />
          <Icon style={{ color: 'white' }} name="arrow-back" />
          <Icon style={{ color: 'white' }} name="arrow-back" />
          <Icon style={{ color: 'white' }} name="arrow-back" /> */}
          {/* <View style={{ marginTop: 20 }}> </View> */}

          <Input
            autoFocus={true}
            placeholder="Enter OTP"
            placeholderTextColor="#c2c2c2"
            style={{ alignSelf: 'center', fontSize: 16, marginTop: 40, marginLeft: 60 }}
            onChangeText={otp => this.setState({ otp })}
            value={this.state.otp}
          />
          <View style={{ marginTop: 20, marginLeft: 20 }}>{this.renderButton()}</View>

          {/* <Content style={contentStyle}>
            <View style={mainView}>
              <View
                style={{
                  marginTop: 10,
                  width: deviceWidth - 40,
                  alignSelf: 'center',
                }}>
                <Text style={{ color: '#c2c2c2' }}> OTP</Text>
                <Item
                  style={{
                    marginTop: 10,
                    width: deviceWidth - 40,
                    height: 40,
                    backgroundColor: '#f7f7f7',
                    borderRadius: 5,
                  }}>
                  <Icon active name="ios-log-in" style={iconStyle} />
                  <Input
                    placeholder="Enter OTP"
                    placeholderTextColor="#c2c2c2"
                    style={{ alignSelf: 'center', fontSize: 11 }}
                    onChangeText={otp => this.setState({ otp })}
                    value={this.state.otp}
                  />
                </Item>
              </View>
              <View style={{ marginTop: 0 }}>{this.renderButton()}</View>
            </View>
          </Content> */}

          <Loading isLoading={this.state.isLoading} />
        </Container>
      </SafeAreaView>
    );
  }

  renderButton() {
    return <View>
      <Text style={styles.messageText}>
        Please wait for a few minutes to receive your One Time Pin.
      </Text>
      <ButtonPress onPress={() => this.submitData()}>Submit</ButtonPress>
    </View>;
  }

  componentDidMount() {

    // this.checkPermission();
    // this.createNotificationListeners();

    /* FCM.requestPermissions(); // for iOS
    FCM.getFCMToken().then(token => {
      this.setState({token: token});
    });
   
    this.notificationListener = FCM.on(FCMEvent.Notification, async notif => {
    
    });*/


    //   const channel = new firebase.notifications.Android.Channel(
    //     'test-channel',
    //     'Test Channel',
    //     firebase.notifications.Android.Importance.Max).setDescription('My apps test channel');

    //   // Create the channel
    //   firebase.notifications().android.createChannel(channel);

    //   //registerAppListener();
    //   this.notificationListener = firebase
    //   .notifications()
    //   .onNotification(notification => {
    //     firebase.notifications().displayNotification(notification);
    //   });

    // this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(token => {
    //   console.log('TOKEN (refreshUnsubscribe)', token);
    // });

    // this.messageListener = firebase.messaging().onMessage(message => {
    //   this.displayNotificationFromCustomData(message);
    // });

    //   firebase.notifications().getInitialNotification();
    //   this.checkPermission;

    //   firebase
    //     .messaging()
    //     .getToken()
    //     .then(token => {
    //       console.log('TOKEN (getFCMToken)', token);
    //       this.storingdeviceId(token);
    //     });
  }

  componentWillUnmount() {
    // this.notificationListener;
    // this.notificationOpenedListener;

    // stop listening for events
    // this.notificationListener.remove();
    //this.notificationListener();
    //this.onTokenRefreshListener();
    //this.messageListener();
  }

  //   async checkPermission() {
  //     const enabled = await firebase.messaging().hasPermission();
  //     if (enabled) {
  //       this.getToken();
  //     } else {
  //       this.requestPermission();
  //     }
  //   }

  //   async getToken() {
  //     let fcmToken = await AsyncStorage.getItem('fcmToken');
  //     if (!fcmToken) {
  //       fcmToken = await firebase.messaging().getToken();
  //       if (fcmToken) {
  //         console.log('fcmToken:', fcmToken);
  //         await AsyncStorage.setItem('fcmToken', fcmToken);
  //       }
  //     }
  //   }

  //   async requestPermission() {
  //     try {
  //       await firebase.messaging().requestPermission();
  //       // If user allow Push Notification
  //       this.getToken();
  //     } catch (error) {
  //       // If user do not allow Push Notification
  //       console.log('Rejected');
  //     }
  //   }

  //   async createNotificationListeners() {

  //     // If your app is in Foreground

  //     this.notificationListener = firebase.notifications().onNotification((notification) => {
  //         const localNotification = new firebase.notifications.Notification({
  //           show_in_foreground: true,
  //         })
  //         .setNotificationId(notification.notificationId)
  //         .setTitle(notification.title)
  //         .setBody(notification.body)

  //         firebase.notifications()
  //           .displayNotification(localNotification)
  //           .catch(err => console.error(err));
  //     });


  //     //If your app is in background

  //     this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
  //       const { title, body } = notificationOpen.notification;
  //       console.log('onNotificationOpened:');
  //       Alert.alert(title, body)
  //     });


  //     // If your app is closed

  //     const notificationOpen = await firebase.notifications().getInitialNotification();
  //     if (notificationOpen) {
  //       console.log('getInitialNotification:');
  //     }

  //     // For data only payload in foreground

  //     this.messageListener = firebase.messaging().onMessage
  // ((message) => {
  //       //process data message
  //       console.log("Message", JSON.stringify(message));
  //     });
  //   }

  async submitData() {
    const SELF = this;
    await AsyncStorage.setItem(
      '@deviceid:key',
      JSON.stringify(SELF.state.token || {}),
    );
    SELF.setState({
      isLoading: true,
    });

    if (SELF.state.otp == '') {
      Alert.alert('Invalid Credential', 'Please enter OTP');
      SELF.setState({
        isLoading: false,
      });
    } else {
      axios({
        url: 'https://raott.com/api/Auth/signup_step2/',
        type: 'POST',
        method: 'POST',
        timeout: 30000,
        data: {
          otp: SELF.state.otp,
          userid: SELF.state.userid,
          deviceid: SELF.state.token,
        },
      })
        .then(function (response) {
          // console.log('response signup_step2');
          // console.log(response);
          if (response.data.status == 'success') {
            AsyncStorage.setItem('apiToken', response.data.apitoken);
            Alert.alert(
              'Success',
              'Please Sign in using your account  ',
              [
                {
                  text: 'OK',
                  onPress: () => SELF.props.navigation.navigate('LoginForm'),
                },
              ],
              { cancelable: false },
            );
          } else {
            Alert.alert(response.data.status, response.data.error);
            SELF.setState({ otp: '' });
          }
          SELF.setState({
            isLoading: false,
          });
        })
        .catch(function (error) {
          SELF.setState({ isLoading: false });
          TimeOutError(error, () => SELF.buttonpress());
          // console.log(error);
        });
    }
  }
}

const styles = {
  iconStyle: {
    color: '#e02d2e',
    marginLeft: 10,
  },

  container: {
    width: deviceWidth,
    // backgroundColor: 'white',
    // alignSelf: 'center',
  },
  contentStyle: {
    marginTop: 60,
    marginBottom: 10,
  },
  mainView: {
    width: deviceWidth,
    backgroundColor: 'white',
  },
  headerView: {
    width: deviceWidth - 40,
    alignSelf: 'center',
  },
  headerStyle: {
    backgroundColor: '#e02d2e',
    height: 44,
    marginTop: Platform.OS === 'ios' ? 25 : 0,
    flexDirection: 'row',
  },
  goBackButton: {
    height: 44,
    width: 44,
    alignItems: 'center',
    justifyContent: 'center',
  },

  headerText: {
    height: 44,
    width: deviceWidth, // - 98,
    // alignItems: 'center',
    // alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: deviceWidth - deviceWidth / 1.5,
  },

  messageText: {
    fontSize: 12,
    width: '100%',
    marginBottom: -15,
  }
};


/*

  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();
  }

  componentWillUnmount() {
    this.notificationListener;
    this.notificationOpenedListener;
  }

  //Check whether Push Notifications are enabled or not
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //Get Device Registration Token
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        console.log('fcmToken:', fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  //Request for Push Notification
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // If user allow Push Notification
      this.getToken();
    } catch (error) {
      // If user do not allow Push Notification
      console.log('Rejected');
    }
  }

  async createNotificationListeners() {
    
    // If your app is in Foreground
   
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const localNotification = new firebase.notifications.Notification({
          show_in_foreground: true,
        })
        .setNotificationId(notification.notificationId)
        .setTitle(notification.title)
        .setBody(notification.body)

        firebase.notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));
    });


    //If your app is in background

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      console.log('onNotificationOpened:');
      Alert.alert(title, body)
    });


    // If your app is closed

    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      console.log('getInitialNotification:');
    }

    // For data only payload in foreground

    this.messageListener = firebase.messaging().onMessage
((message) => {
      //process data message
      console.log("Message", JSON.stringify(message));
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Push Notification Demo</Text>
        <Text style={styles.instructions}>By Neova Solutions..</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});


*/