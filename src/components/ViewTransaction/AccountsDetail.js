import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  Input,
} from 'native-base';
import styles from './styles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TransactionDetail from './TransactionDetail.js';
import axios from 'axios';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  NetInfo,
  BackHandler,
  StatusBar,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Picker from '../Picker/Picker.js';
import Display from 'react-native-display';
import Dialog from './Dialog.js';
import Loading from '../Loading/';
import numeral from 'numeral';
import { InternetCheck, TimeOutError } from '../common/';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { NativeBaseProvider, Box, Center } from 'native-base';
const goldCrown = require('../../../img/rsz_gold_crown.png');
const goldCup = require('../../../img/rsz_gold_cup.png');
const goldRing = require('../../../img/rsz_gold_ring.png');

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import Ionicons from 'react-native-vector-icons/Ionicons';
import SafeAreaView from 'react-native-safe-area-view';

class AccountsDetail extends Component {
  state = {
    transactions: [],
    isSortPickerVisible: false,
    isFilterPickerVisible: false,
    enable: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      dealsTransaction: [],
      count: '1',
      isLoading: true,
      sortValue: 2,
      filterValue: -1,
      removeLoadButton: '',
      searchName: '',
      searchTransactions: [],
      isVisibleDialog: true,
      sortListItems: [
        'Sort by (A-Z)',
        'Sort by (Z-A)',
        'Newest first',
        'Oldest first',
      ],
      filterListItems: ['All', 'Deposit', 'Withdrawal'],
      cardClick: '',
      details: [],
      fullName: '',
      category: '',
      search: false,
      showNoDataText: false,
    };
    // headerTitle = "Transfer World Raott Coins to "
    // screen = "FriendList"

    this._backAndroidPress = this.backAndroidPress.bind(this);

    AsyncStorage.getItem('fullName')
      .then(value => {
        // console.log(value)

        this.setState({ fullName: value });
      })
      .done();
    InternetCheck();
    this.getData();
  }

  componentDidMount() {
    console.log('Category page', this.props);
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  click() {
    this.setState({ count: parseInt(this.state.count) + 1, isLoading: true });
    this.getData();
  }

  onCardClick(Details, category) {
    this.setState({
      cardClick: 'clicked',
      details: Details,
      category: category,
      isVisibleDialog: true,
    });
  }

  renderTransaction() {
    return this.state.dealsTransaction.map((dealTransaction, i) => (

      <TransactionDetail
        key={i}
        dealTransaction={dealTransaction}

        onCardClick={(dealTransaction, category) =>
          this.onCardClick(dealTransaction, category)
        }
      />
    ));
  }

  renderSearchTransactions() {
    // console.log(this.state.searchName)
    if (this.state.searchTransactions.length > 0) {
      //  console.log(this.state.searchTransactions.length,'if *********')
      return this.state.searchTransactions.map((dealTransaction, i) => (
        <TransactionDetail
          key={i}
          dealTransaction={dealTransaction}
          onCardClick={(dealTransaction, category) =>
            this.onCardClick(dealTransaction, category)
          }
        />
      ));
    } else if (this.state.searchName != '') {
      // console.log('else *********')
      return (
        <Text style={{ textAlign: 'center' }}> No matching records found</Text>
      );
    }
  }

  renderDialog() {
    // console.log(this.state.details)
    return (
      <Dialog
        details={this.state.details}
        method={() => this.toggleModal()}
        category={this.state.category}
      />
    );
  }

  toggleModal() {
    this.setState({
      isVisibleDialog: !this.state.isVisibleDialog,
    });
  }

  showPicker() {
    this.setState({
      isSortPickerVisible: true,
      removeLoadButton: '',
    });
  }

  hidePicker(sortValue) {
    // console.log(sortValue,'jghdfuy')

    this.setState({
      isSortPickerVisible: false,
      sortValue: sortValue,
      filterValue: -1,
    });
  }

  filterPicker() {
    this.setState({
      isFilterPickerVisible: true,
      removeLoadButton: '',
    });
  }
  filterPickerHide(filterValue) {
    // console.log(filterValue)
    this.setState({
      isFilterPickerVisible: false,
      filterValue: filterValue,
      sortValue: -1,
    });
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }

  render() {
    let renderList = null;
    let raotts = numeral(this.state.raotts).format('0,0.00');
    let currency = numeral(this.state.currency).format('0,0.00');

    if (this.state.sortValue == 0) {
      renderList = this.sortAtoZ(this.state.dealsTransaction);
    } else if (this.state.sortValue == 1) {
      renderList = this.sortZtoA(this.state.dealsTransaction);
    } else if (this.state.sortValue == 2) {
      renderList = this.sortNewest(this.state.dealsTransaction);
    } else if (this.state.sortValue == 3) {
      renderList = this.sortOldest(this.state.dealsTransaction);
    }

    if (this.state.filterValue == 0) {
      renderList = this.renderTransaction();
    } else if (this.state.filterValue == 1) {
      renderList = this.filterDeposit(this.state.dealsTransaction);
    } else if (this.state.filterValue == 2) {
      renderList = this.filterWithdrawal(this.state.dealsTransaction);
    }

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View style={styles.headerStyle}>
            <TouchableOpacity
              style={styles.goBackButton}
              onPress={() => this.goBack()}>
              <MaterialCommunityIcons name="arrow-left" size={24} color="white" />
            </TouchableOpacity>
            <View style={styles.headerText}>
              <Text style={{ color: 'white' }}>Account Details</Text>
            </View>
          </View>
          {this.state.currency && (
            <View style={styles.infoView}>
              <Text style={styles.balanceText}>
                {' '}
                Closing World Raott Balance{' '}
              </Text>
              <Text numberOfLines={1} style={styles.roattCoinInfo}>
                {' '}
                R {raotts} ({currency} {this.state.currency.split(' ')[1]}){' '}
              </Text>
            </View>
          )}
          <View style={styles.menuMainView}>
            <TouchableOpacity onPress={() => this.showPicker()}>
              <View style={[styles.menuBarStyle]}>
                <MaterialIcons
                  name="swap-vert"
                  size={20}
                  style={{ color: 'rgba(160,160,160,1)', marginRight: 0 }}
                />
                <Text style={styles.textStyle}>Sort</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.filterPicker()}>
              <View style={[styles.menuBarStyle]}>
                <Image
                  style={{ width: 15, height: 15, marginRight: 5 }}
                  source={require('../../../img/filterIcon.png')}
                />
                <Text style={styles.textStyle}>Filter</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.onSearchClick()}>
              <View style={[styles.menuBarStyle]}>
                <MaterialIcons
                  name="search"
                  size={20}
                  style={{ color: 'rgba(160,160,160,1)', marginRight: 0 }}
                />
                <Text style={styles.textStyle}>Search</Text>
              </View>
            </TouchableOpacity>
          </View>

          {/* <Content style={{borderWidth: 0}}> */}
          <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
            {this.state.removeLoadButton == 'remove' && (
              <View>
                <View style={styles.mainView}>
                  <View style={styles.searchStyle}>
                    <View style={{
                      flexDirection: 'row',
                      paddingHorizontal: 7,
                    }}>
                      <TouchableOpacity
                        onPress={() => this.onChangeText(this.state.searchName)}
                        style={{ alignSelf: 'center' }}>
                        {/* <Icon
                          style={{
                            fontSize: 25,
                            alignSelf: 'center',
                            marginLeft: 10,
                            backgroundColor: 'transparent',
                          }}
                          active
                          name="search"
                        /> */}
                        <MaterialIcons name="search" size={24} color="grey" alignSelf='start' />
                      </TouchableOpacity>
                      <Input
                        flex='1'
                        style={{
                          color: 'rgba(106,106,106,1)',
                          marginLeft: 16,
                          height: 42,
                          backgroundColor: 'transparent',
                          fontSize: 14,
                        }}
                        placeholder="Search User"
                        onChangeText={searchName =>
                          this.onChangeText(searchName)
                        }
                        variant="Unstyled"
                        value={this.state.searchName}
                        autoCorrect={false}
                      />
                      {/* <Icon
                        active
                        name="people"
                        style={{
                          fontSize: 25,
                          position: 'absolute',
                          right: 10,
                          backgroundColor: 'transparent',
                          alignSelf: 'center',
                        }}
                      /> */}
                      <Ionicons name="md-people" size={24} color="grey" alignSelf="center" />
                      {/* <Text>hello</Text> */}
                    </View>
                  </View>
                </View>

                <View style={{ marginTop: 10 }}>
                  {this.renderSearchTransactions()}

                  <View style={{ height: 20 }} />
                </View>
              </View>
            )}
            {/* <Text>hello</Text> */}
            {this.state.removeLoadButton == '' && (
              <View style={{ marginTop: 10 }}>

                {this.state.dealsTransaction &&
                  this.state.dealsTransaction.length > 0 ?
                  // <Text>hello</Text>

                  (
                    renderList
                  )
                  : (
                    <View />
                  )

                }
                {/* <Text style ={{textAlign:'center',margin:10}}>No Data Available in Table</Text>} */}

                <View style={{ height: 20 }} />
              </View>
            )}
            <View style={{ alignItems: 'center', width: deviceWidth }}>
              {this.state.removeLoadButton == '' &&
                this.state.dealsTransaction &&
                this.state.dealsTransaction.length > 0 && (
                  <TouchableOpacity onPress={() => this.click()}>
                    {/* <View transparent style={styles.headerTitle}> */}

                    <Text
                      style={{
                        color: 'black',
                        textAlign: 'center',
                        marginBottom: 20,
                        justifyContent: 'center',
                      }}>
                      Display More
                    </Text>

                    {/* </View> */}
                  </TouchableOpacity>
                )}
            </View>
            {/* </Content> */}
          </Box>
          {this.state.showNoDataText && (
            <Text
              style={{
                textAlign: 'center',
                alignSelf: 'center',
                margin: 10,
                position: 'absolute',
                marginTop: 85,
              }}>
              No Data Available in Table
            </Text>
          )}

          {this.state.isSortPickerVisible && (
            <Picker
              isVisible={true}
              hidePicker={listItem => this.hidePicker(listItem)}
              listItems={this.state.sortListItems}
            />
          )}

          {this.state.isFilterPickerVisible && (
            <Picker
              isVisible={true}
              hidePicker={listItem => this.filterPickerHide(listItem)}
              listItems={this.state.filterListItems}
            />
          )}

          {/* </Display> */}

          {this.state.cardClick == 'clicked' &&
            this.state.isVisibleDialog &&
            this.renderDialog()}

          <Loading isLoading={this.state.isLoading} />
        </Box>
      </SafeAreaView>
    );
  }

  onChangeText(searchName) {
    this.setState({ searchName: searchName });
    this.fetchData(searchName);
  }

  fetchData(searchName) {
    // console.log(searchName.toLowerCase() , searchName , "searchName")
    var lowerCaseName = searchName.toLowerCase();
    // console.log(lowerCaseName)
    if (searchName != '') {
      let searchArray = [];
      let dealArray = this.state.dealsTransaction;

      dealArray.map(deal => {
        if (deal.Username.includes(searchName.toUpperCase())) {
          searchArray.push(deal);
        }
        if (deal.Username.includes(searchName.toLowerCase())) {
          searchArray.push(deal);
        }
      });
      // console.log(searchArray)
      this.setState({ searchTransactions: searchArray });
    }
  }

  onSearchClick() {
    this.setState({ removeLoadButton: 'remove', search: true });
    this.setState({ showNoDataText: false });
  }

  sortZtoA(deals) {
    let dealArray = deals;

    dealArray = dealArray.sort(function (a, b) {
      var nameA = a.Username.toLowerCase(),
        nameB = b.Username.toLowerCase();
      if (nameA < nameB)
        //sort string ascending
        return 1;
      if (nameA > nameB) return -1;
      return 0; //default return value (no sorting)
    });
    // console.log(dealArray);
    return dealArray.map((dealTransaction, i) => (
      <TransactionDetail key={i} dealTransaction={dealTransaction} />
    ));
  }

  sortAtoZ(deals) {
    let dealArray = deals;

    dealArray = dealArray.sort(function (a, b) {
      var nameA = a.Username.toLowerCase(),
        nameB = b.Username.toLowerCase();
      if (nameA < nameB)
        //sort string ascending
        return -1;
      if (nameA > nameB) return 1;
      return 0; //default return value (no sorting)
    });
    console.log(dealArray);
    return dealArray.map((dealTransaction, i) => (
      <TransactionDetail
        key={i}
        dealTransaction={dealTransaction}
        onCardClick={(dealTransaction, category) =>
          this.onCardClick(dealTransaction, category)
        }
      />
    ));
  }

  sortOldest(deals) {
    let dealArray = deals;

    if (dealArray !== undefined && dealArray.length > 0) {
      dealArray.sort((obj1, obj2) => {
        if (obj1.Date > obj2.Date) return 1;
        if (obj1.Date < obj2.Date) return -1;
        return 0;
      });
    }

    // console.log(dealArray);
    return dealArray.map((dealTransaction, i) => (
      <TransactionDetail
        key={i}
        dealTransaction={dealTransaction}
        onCardClick={(dealTransaction, category) =>
          this.onCardClick(dealTransaction, category)
        }
      />
    ));
  }

  sortNewest(deals) {
    let dealArray = deals;

    if (dealArray !== undefined && dealArray.length > 0) {
      dealArray.sort((obj1, obj2) => {
        if (obj1.Date < obj2.Date) return 1;
        if (obj1.Date > obj2.Date) return -1;
        return 0;
      });
    }

    // console.log(dealArray);
    return dealArray.map((dealTransaction, i) => (
      <TransactionDetail
        key={i}
        dealTransaction={dealTransaction}
        onCardClick={(dealTransaction, category) =>
          this.onCardClick(dealTransaction, category)
        }
      />
    ));
  }

  filterDeposit(deals) {
    let depositDeals = [];

    deals.map(deal => {
      if (deal.Deposit !== '-') {
        depositDeals.push(deal);
      }
    });
    // console.log('filterDeposit')
    // console.log(depositDeals)
    return depositDeals.map((dealTransaction, i) => (
      <TransactionDetail
        key={i}
        dealTransaction={dealTransaction}
        onCardClick={(dealTransaction, category) =>
          this.onCardClick(dealTransaction, category)
        }
      />
    ));
  }

  filterWithdrawal(deals) {
    let withdrawalDeals = [];

    deals.map(deal => {
      if (deal.Deposit == '-') {
        withdrawalDeals.push(deal);
      }
    });
    // console.log('filterWithdrawal')
    // console.log(withdrawalDeals)
    return withdrawalDeals.map((dealTransaction, i) => (
      <TransactionDetail
        key={i}
        dealTransaction={dealTransaction}
        onCardClick={(dealTransaction, category) =>
          this.onCardClick(dealTransaction, category)
        }
      />
    ));
  }
  //********  this is made to change the name of username with "World Raott Fund" so  that each list can be made according to W of World Raott Fund not by the initial of username */
  makingFinalList(deals) {
    let dealArray = [];
    let temp = '';

    deals.map((dealTransaction, i) => {
      if (dealTransaction.Username == this.state.fullName) {
        // console.log(dealTransaction)
        temp = dealTransaction;
        // console.log(temp)
        temp.Username = 'World Raott Fund';
        dealArray.push(temp);
      } else {
        dealArray.push(dealTransaction);
      }
    });
    this.setState({ dealsTransaction: dealArray });
  }

  getData() {
    const SELF = this;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log('helloak ');


        axios.interceptors.request.use(request => {
          console.log('Starting Request', JSON.stringify(request, null, 2))
          return request
        })


        axios({
          url:
            'https://raott.com/api/v2/screenTransactions/' +
            SELF.state.count +
            '/5/' + value,
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          // headers: {
          //   RaottAuth: value,
          // },
        })
          .then(function (response) {
            SELF.setState({
              isLoading: false,
            });
            console.log('responseData');
            console.log(response);
            console.log(response.data.currency)
            console.log(response.data.raotts)
            let dealList = [];

            Object.keys(response.data.history).map(key => {
              dealList.push(response.data.history[key]);
            });

            let finalDealArray = [];
            if (SELF.state.count > 1) {
              let dealArray = SELF.state.dealsTransaction;
              dealArray.map(deal => {
                finalDealArray.push(deal);
              });
              dealList.map(deal => {
                finalDealArray.push(deal);
              });

              SELF.setState({
                currency: response.data.currency,
                raotts: response.data.raotts,
              });
              SELF.makingFinalList(finalDealArray);
            } else {
              if (dealList.length > 0) {
                dealList.map(deal => {
                  finalDealArray.push(deal);
                });
                SELF.setState({
                  currency: response.data.currency,
                  raotts: response.data.raotts,
                });

                SELF.makingFinalList(finalDealArray);
              } else {
                SELF.setState({ showNoDataText: true });
              }
            }
          })
          .catch(function (error) {
            // console.log('jaiho '+error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.getData());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

export default AccountsDetail;
