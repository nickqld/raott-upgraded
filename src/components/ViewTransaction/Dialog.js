import React, {Component} from 'react';
import {
  View,
  ActivityIndicator,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './styles';
import PropTypes from 'prop-types';
// import MaterialIcons from '../../../../../../../../Users/sierra/Library/Caches/typescript/2.6/node_modules/@types/react-native-vector-icons/MaterialIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

class Dialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCoin: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('selectedCoin')
      .then(value => {
        // console.log(value)
        this.setState({selectedCoin: value});
      })
      .done();
  }

  renderImages() {
    // console.log(this.props.details)
    return this.props.details.map((detail, i) => (
      <View
        key={i}
        style={{
          marginBottom: 20,
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{color: 'black', fontSize: 12, textAlign: 'center'}}>
            {detail.id % 100}.
          </Text>
          <Text
            style={{
              color: 'black',
              fontSize: 12,
              textAlign: 'center',
              marginLeft: 3,
            }}>
            {detail.coin}
          </Text>
        </View>
        <Image
          style={{
            width: 60,
            height: 60,
            alignSelf: 'center',
            marginTop: 5,
            marginBottom: 2,
          }}
          source={{uri: 'https:' + detail.image}}
        />

        <Text
          style={{
            margin: 1,
            color: 'black',
            fontSize: 10,
            textAlign: 'center',
          }}>
          No. of coins {detail.quantity}
        </Text>
      </View>
    ));
  }

  render() {
    const {details, method} = this.props;

    return (
      <View style={styles.containerDialog}>
        <View style={{width: deviceWidth - 40, backgroundColor: '#f7f7f7'}}>
          <TouchableOpacity onPress={() => this.props.method()}>
            <MaterialIcons
              name="close"
              size={20}
              style={{
                width: 30,
                height: 30,
                alignSelf: 'flex-end',
                margin: 5,
              }}
            />
          </TouchableOpacity>

          <View
            style={{
              width: deviceWidth - 40,
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text
              style={{
                marginBottom: 20,
                color: 'black',
                fontSize: 15,
                textAlign: 'center',
                marginHorizontal: 12,
              }}>
              Details of transferred {this.props.category} World Raott coins
            </Text>
            <View
              style={{
                backgroundColor: 'black',
                width: deviceWidth - 40,
                borderColor: 'rgba(216,216,216,1)',
                borderWidth: 0.2,
                marginBottom: 10,
              }}
            />

            <ScrollView
              style={{height: deviceHeight * 0.35, width: deviceWidth * 0.8}}>
              {this.renderImages()}
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: 'black',
              width: deviceWidth - 40,
              borderColor: 'rgba(216,216,216,1)',
              borderWidth: 0.2,
              marginBottom: 10,
              marginTop: 10,
            }}
          />
          <TouchableOpacity
            style={{alignSelf: 'flex-end'}}
            onPress={() => this.props.method()}>
            <Text
              style={{
                width: 50,
                borderColor: 'rgba(216,216,216,1)',
                borderWidth: 1,
                margin: 10,
                color: 'black',
                fontSize: 10,
                textAlign: 'center',
                padding: 10,
                backgroundColor: 'rgba(238,238,238,1)',
              }}>
              Close
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

Dialog.propTypes = {
  size: PropTypes.string,
};

Dialog.defaultProps = {
  size: 'large',
};

export default Dialog;
