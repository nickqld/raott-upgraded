import React, {Component} from 'react';
import {Text, View, Button, Dimensions} from 'react-native';

import Display from 'react-native-display';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class testdisplay extends Component {
  constructor(props) {
    super(props);

    this.state = {enable: false};
  }

  toggleDisplay() {
    let toggle = !this.state.enable;
    this.setState({enable: toggle});
  }

  render() {
    return (
      <View style={{borderWidth: 0, height: deviceHeight - 60}}>
        <View style={styles.button}>
          <Button
            onPress={this.toggleDisplay.bind(this)}
            title="Toggle display"
            color="#34495e"
            accessibilityLabel="Toggle display for show/hide circles"
          />
        </View>

        <Display
          enable={this.state.enable}
          enterDuration={400}
          exitDuration={400}
          exit="fadeOutDown"
          enter="fadeInUp"
          style={{
            flexDirection: 'column',
            justifyContent: 'flex-end',
            borderWidth: 0,
            height: deviceHeight - 200,
          }}>
          <View style={[styles.circle, {backgroundColor: 'green'}]} />
        </Display>
      </View>
    );
  }
}

const styles = {
  button: {
    padding: 10,
    margin: 15,
    borderWidth: 0,
  },

  circle: {
    borderRadius: 50,
    height: 100,
    width: 100,
    position: 'absolute',
    alignSelf: 'center',
    bottom: 40,
  },
};
