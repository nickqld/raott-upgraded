import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  NetInfo,
  BackHandler,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import dialogBox from '../dialogBox/';
import styles from './styles';
import RaottCoinsDetail from './RaottCoinsDetail.js';
import axios from 'axios';
import Loading from '../Loading/';
import numeral from 'numeral';
import {InternetCheck, TimeOutError} from '../common/';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
let currency = '',
  screen = '',
  headerTitle = '';
class RoattCoins extends Component {
  constructor(props) {
    super(props);

    this.state = {
      worldRaotts: [],
      isLoading: true,
      symbol: '',
      timeoutDone: false,
    };
    screen = props.navigation.state.params.screen;
    headerTitle = props.navigation.state.params.headerTitle;
    fid = props.navigation.state.params.fid;

    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
    this.screenWorldRaotts();
  }
  onSelectTab = data => {
    this.screenWorldRaotts();
  };

  onCardClick(id) {
    this.props.navigation.navigate('CoinTab', {
      onSelectTab: this.onSelectTab,
      headerTitle: headerTitle,
      screen: screen, // 'FriendList', //screen,
      catid: id,
      fid: fid,
      currency: currency,
    });
  }

  rendercollections() {
    return this.state.worldRaotts.map((worldRaott, i) => (
      <RaottCoinsDetail
        key={i}
        worldRaott={worldRaott}
        onCardClick={id => this.onCardClick(id)}
        symbol={this.state.symbol}
      />
    ));
  }

  componentDidMount() {
    console.log('Category page', this.props);
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }
  goBack() {
    const {navigation} = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }

  subHeading() {
    if (screen === 'FriendList')
      return `From which World Raott Coin collection would you like to transfer to ${
        headerTitle.split('to')[1]
      }`;
    else return 'Click on any symbol to see your World Raott collections';
  }

  render() {
    let balance = 0;
    let currencyAdded = 0;

    if (this.state.worldRaotts && this.state.worldRaotts.length) {
      this.state.worldRaotts.map((worldRaott, i) => {
        (balance = balance + parseInt(worldRaott.balance)),
          (currencyAdded =
            currencyAdded + parseFloat(worldRaott.currency.split(' ')[0]));

        currency = worldRaott.currency.split(' ')[1];
      });
    }

    return (
      <SafeAreaProvider style={{flex: 1}}>
        {/* <Container style={styles.container}> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View style={styles.headerStyle}>
            <TouchableOpacity
              style={styles.goBackButton}
              onPress={() => this.goBack()}>
              {/* <Icon style={{color: '#e02d2e'}} name="arrow-back" /> */}
              <MaterialCommunityIcons name="arrow-left" size={24} color='#e02d2e' />
            </TouchableOpacity>
            <View style={styles.headerTitle}>
              <Text style={styles.headerText}>Your World Raotts</Text>
            </View>
          </View>
          {this.state.worldRaotts.length > 0 && (
            <View>
              <View style={styles.infoMainView}>
                <View style={styles.info}>
                  <Text style={styles.balanceText}>
                    {' '}
                    Your World Raott balance is
                  </Text>
                  <Text numberOfLines={1} style={styles.raottCoin}>
                    R {numeral(balance).format('0,0')} (
                    {numeral(currencyAdded).format('0,0.00')} {currency})
                  </Text>
                </View>
              </View>
              <View style={styles.collectionText}>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#e02d2e',
                    marginHorizontal: 5,
                    textAlign: 'center',
                  }}>
                  {this.subHeading()}
                </Text>
              </View>
            </View>
          )}
          {/* <Content> */}
            <View>{this.rendercollections()}</View>
          {/* </Content> */}
          <Loading isLoading={this.state.isLoading} />
        {/* </Container> */}
        </Box>
      </SafeAreaProvider>
      
    );
  }

  screenWorldRaotts() {
    const SELF = this;

    if (!this.state.isLoading) {
      SELF.setState({isLoading: true, timeoutDone: false});
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);

        // axios.interceptors.request.use(request => {
        //   console.log('Starting Request', JSON.stringify(request, null, 2))
        //   return request
        // })

        axios({  
          url: 'https://raott.com/api/v2/screenWorldRaotts/1/',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            // console.log('response screenWorldRaotts')
            // console.log(response);
            // alert(JSON.stringify(response));
            collectionId = response.data.collectionid;

            let worldRaottsList = [];
            Object.keys(response.data.cats).map(key => {
              worldRaottsList.push(response.data.cats[key]);
            });

            SELF.setState({
              worldRaotts: worldRaottsList,
              symbol: response.data.symbol,
              selected: false,
              isLoading: false,
            });
          })
          .catch(function(error) {
            // console.log(error);
            if (!SELF.state.timeoutDone) {
              TimeOutError(error, () => SELF.screenWorldRaotts());
            }
            SELF.setState({
              isLoading: false,
              selected: false,
              timeoutDone: true,
            });
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

export default RoattCoins;
