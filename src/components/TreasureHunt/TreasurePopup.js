import React, {useState} from 'react';
import {
  View,
  ActivityIndicator,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  Image,
} from 'react-native';
import styles from '../TransferCoinToFriends/styles';
import PropTypes from 'prop-types';
import HTML from 'react-native-render-html';
import {ScrollView} from 'react-native-gesture-handler';
import Modal from 'react-native-modal';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const TreasurePopup = (props, children) => {
  const [loading, setLoading] = useState(props.loading);

  return (
    // <View style={styles.containerDialog}>
    <Modal
      isVisible={loading}
      backdropOpacity={0.5}
      // swipeDirection="left"
      // onSwipe={this.closeModal}
      // onBackdropPress={this.closeModal}
    >
      <View style={styles.innerContainerDialogNew}>
        <View style={styles.messageHeaderView}>
          <Text style={styles.headerTextStyle}> {props.titleText}</Text>
          <TouchableOpacity onPress={() => setLoading(false)}>
            <Image
              style={styles.cancelImageStyle}
              source={require('../../../img/closeicon.png')}
            />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View>
            <Image
              style={styles.imagestyle}
              source={{
                uri:
                  'https://raott.com/raottdev/assets/treasure_images/' +
                  props.details.treasure_image,
              }}
            />
            <Text style={[styles.center, styles.centerRed]}>
              {props.details.treasure_title}
            </Text>
            <Text style={styles.center}>
              {props.details.text1} | {props.details.text2}
            </Text>
            <View style={styles.bgyellow}>
              <HTML html={props.details.treasure_desc} />
            </View>
          </View>
          <Text>sdgsdg</Text>
          <Text>sdgsdg</Text>
          <Text>sdgsdg</Text>
          <Text>sdgsdg</Text>
        </ScrollView>
        <TouchableOpacity onPress={() => setLoading(false)}>
          <Text style={styles.okButtonTextStyle}> OK </Text>
        </TouchableOpacity>
      </View>
    </Modal>
    // </View>
  );
};

TreasurePopup.propTypes = {
  size: PropTypes.string,
};

TreasurePopup.defaultProps = {
  size: 'large',
};

export default TreasurePopup;
