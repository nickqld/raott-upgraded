import React, {Component} from 'react';
import {
  View,
  ActivityIndicator,
  Dimensions,
  TouchableHighlight,
  Text,
  ToastAndroid,
  ToolbarAndroid,
  Platform,
  Alert,
  Image,
} from 'react-native';
import {Button, Container, Content, Icon, Toast} from 'native-base';
import styles from './styles';
import Loading from '../../Loading/';
// import Image from 'react-native-image-zoom';
// import {ViewPagerZoom} from 'react-native-image-zoom';
// import { Actions } from 'react-native-router-flux'
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

class ImageView extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      isLoading: true,
      timePassed: false,
    };

    if (Platform.OS === 'android') {
      this.onLoadPhotoAndroid();
    }
  }

  componentDidUpdate() {
    if (!this.state.isLoading) {
      setTimeout(() => {
        this.setTimePassed();
      }, 5000);
    }
  }

  setTimePassed() {
    this.setState({timePassed: true});
  }

  logOutZoomState = (event, gestureState, zoomableViewEventObject) => {
    console.log('');
    console.log('');
    console.log('-------------');
    console.log('Event: ', event);
    console.log('GestureState: ', gestureState);
    console.log('ZoomableEventObject: ', zoomableViewEventObject);
    console.log('');
    console.log(
      'Zoomed from' +
        zoomableViewEventObject.lastZoomLevel +
        'to' +
        zoomableViewEventObject.zoomLevel,
    );
  };

  render() {
    //  console.warn(this.props.imageUrl)
    return (
      <View style={styles.container}>
        <View style={styles.cancelButtonView}>
          <View style={styles.cancelButtonInnerView}>
            <Button
              style={{alignSelf: 'center'}}
              transparent
              onPress={() => this.props.closeImageView()}>
              {/* <Icon name="md-close" style={{color: 'white'}} /> */}
              <MaterialCommunityIcons name="close" size={24} color="white" />

            </Button>
          </View>
        </View>

        {Platform.OS === 'ios' && (
          <View>
            <ReactNativeZoomableView
              zoomEnabled={true}
              maxZoom={3}
              minZoom={0.85}
              zoomStep={0.25}
              initialZoom={0.85}
              bindToBorders={true}
              onZoomAfter={this.logOutZoomState}
              style={{width: deviceWidth, height: deviceHeight - 80}}
              onLoad={() => this.onLoadPhoto()}>
              <Image
                style={{flex: 1, width: '100%', height: '100%'}}
                source={{uri: this.props.imageUrl}}
                resizeMode="contain"
              />
            </ReactNativeZoomableView>
            {/* {!this.state.timePassed && !this.state.isLoading && ( */}
            <Text style={styles.zoomImageText}>Double Tap to Zoom</Text>
            {/* )} */}
          </View>
        )}

        {Platform.OS === 'android' && (
          // <Image
          //   source={{uri: this.props.imageUrl}}
          //   style={{width: deviceWidth, height: deviceHeight - 80}}
          //   onLoad={() => this.onLoadPhotoAndroid()}
          // />
          <ReactNativeZoomableView
            zoomEnabled={true}
            maxZoom={1.5}
            minZoom={0.5}
            zoomStep={0.25}
            initialZoom={0.9}
            bindToBorders={true}
            onZoomAfter={this.logOutZoomState}
            style={{width: deviceWidth, height: deviceHeight - 80}}
            onLoad={() => this.onLoadPhotoAndroid()}>
            <Image
              style={{flex: 1, width: '100%', height: '100%'}}
              source={{uri: this.props.imageUrl}}
              resizeMode="contain"
            />
          </ReactNativeZoomableView>
        )}

        {/* <Loading isLoading={this.state.isLoading} /> */}
      </View>
    );
  }

  onLoadPhotoAndroid() {
    ToastAndroid.show('Double Tap to zoom', ToastAndroid.LONG);
    this.setState({isLoading: !this.state.isLoading});
  }
  onLoadPhoto() {
    this.setState({isLoading: !this.state.isLoading});
  }
}

export default ImageView;
