const React = require('react-native');
const {StyleSheet, Dimensions} = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const scale = Dimensions.get('window').width / 375;

export default {
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancelButtonView: {
    height: 80,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    width: deviceWidth,
  },
  cancelButtonInnerView: {
    flexDirection: 'row',
    marginTop: 12,
    justifyContent: 'flex-end',
    paddingTop: 20,
  },
  zoomImageText: {
    color: 'red',
    position: 'absolute',
    textAlign: 'center',
    width: deviceWidth,
  },
};
