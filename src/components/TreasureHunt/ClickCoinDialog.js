import React from 'react';
import {
  View,
  ActivityIndicator,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  Image,
} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Dialog = props => {
  // console.warn(deviceWidth)
  let direction = 'row';
  let ht = 80;
  if (deviceWidth <= 320) {
    (direction = 'column'), (ht = 80);
  }
  // console.warn(direction)
  return (
    <View style={styles.containerDialog}>
      <View style={styles.dialogViewStyle}>
        <View style={styles.dialogHeaderTextView}>
          <Text style={styles.dialogHeaderTextStyle}>{props.titleText}</Text>
        </View>

        <Text style={styles.coinDialogMsgText}>{props.msg}</Text>

        {/* <Text style={{ margin: 10, color: 'black', fontSize: 10, textAlign: 'center' }}>{props.finalmsg}</Text> */}

        <View
          style={[
            styles.coinDialogTouchButtonView,
            {flexDirection: direction},
          ]}>
          <TouchableOpacity
            style={styles.coinDialogButtonTouch}
            onPress={() => props.method('Cancel')}>
            <Text style={styles.coinDialogButtonText}>{props.leftButton}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.coinDialogButtonTouch}
            onPress={() => props.method('OK')}>
            <Text style={styles.coinDialogButtonText}>{props.rightButton}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

Dialog.propTypes = {
  size: PropTypes.string,
};

Dialog.defaultProps = {
  size: 'large',
};

export default Dialog;
