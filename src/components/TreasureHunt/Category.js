import React, {Component} from 'react';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  TextInput,
  Linking,
  BackHandler,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  Header,
} from 'native-base';
import styles from './styles';
import {connect} from 'react-redux';
import axios from 'axios';
import CategoryDetail from './CategoryDetail.js';

import Loading from '../Loading/';
import {InternetCheck, TimeOutError} from '../common/';
// import {
//   checkPermission,
//   requestPermission,
// } from 'react-native-android-permissions';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NativeBaseProvider, Box, Center} from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
let screen = '';
let count = 0;
let list = [];
class Category extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      isLoading: true,
      categories: [],
      count: 0,
      searchedCategories: [],
      text: '',
      listStatus: 'Loading',
    };
    // console.log(this.props.navigation.state.params.screen, 'screen')
    screen = this.props.navigation.state.params.screen;
    this._backAndroidPress = this.backAndroidPress.bind(this);

    this.getCategories();
  }

  componentDidMount() {
    // console.log('Category page', this.props)
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }
  backAndroidPress() {
    this.goBack();
    return true;
  }
  goBack() {
    const {navigation} = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }
  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.location,
        {
          title: 'Location Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log("You can use the camera")
      } else {
        // console.log("Camera permission denied")
      }
    } catch (err) {
      // console.warn(err)
    }
  }
  renderCategory() {
    let categoryArray = [];
    let index = 0;
    //  console.log(list)
    if (list.length - this.state.count >= 8) {
      index = this.state.count + 8;
    } else {
      index = list.length - this.state.count;
      index = this.state.count + index;
    }
    // console.log('here', this.state.count, '  ', index)
    for (let i = this.state.count; i < index; i += 2) {
      let prod = list[i];
      let prod2 = list[i + 1];
      if (prod2 != null) {
        categoryArray.push(
          <CategoryDetail
            key={i}
            category={prod}
            category1={prod2}
            onCardClick={(id) => this.onCardClick(id)}
          />,
        );
      } else {
        categoryArray.push(
          <CategoryDetail
            key={i}
            category={prod}
            category1={null}
            onCardClick={(id) => this.onCardClick(id)}
          />,
        );
      }
    }
    return categoryArray;
  }
  onCardClick(id) {
    let userid = '';
    if (screen === 'LandingPage') {
      userid = this.props.user.userid;
    } else {
      userid = '0';
    }
    this.props.navigation.navigate('MapUI', {id, userid});
  }
  render() {
    if (this.state.searchedCategories.length > 0) {
      list = this.state.searchedCategories;
    } else if (this.state.text.length == 0) {
      list = this.state.categories;
    } else {
      list = [];
    }
    return (
      <SafeAreaProvider style={{flex: 1}}>
        {/* <Container style={styles.container}> */}
        <Box
          display="flex"
          flex="1"
          backgroundColor="white"
          flexDirection="column">
          <View style={styles.headerStyle}>
            <TouchableOpacity
              style={styles.goBackButton}
              onPress={() => this.goBack()}>
              {/* <Icon
                  as={<MaterialCommunityIcons name="arrow-left" />}
                  style={[styles.arrowColor, {textAlign: 'center'}]}
                /> */}

              <MaterialCommunityIcons
                name="arrow-left"
                size={24}
                color="white"
              />
            </TouchableOpacity>
            <View transparent style={styles.headerView}>
              <Text style={[styles.arrowColor, {textAlign: 'center'}]}>
                TREASURE HUNT
              </Text>
            </View>
          </View>

          <Text style={styles.subHeading}>
            GeoMint Rare Digital Assets, Treasures and Collectibles from all
            around the world
          </Text>

          <View style={styles.innerViewStyle}>
            <View style={styles.searchViewStyle}>
              <TextInput
                style={styles.textInputView}
                onChangeText={(text) =>
                  this.setState({text}, () => {
                    this.searchCategory();
                  })
                }
                value={this.state.text}
                placeholder="Search"
                underlineColorAndroid="transparent"
                autoCorrect={false}
              />
            </View>

            <TouchableOpacity
              onPress={() => this.searchCategory()}
              style={styles.searchButtonStyle}>
              <Text transparent style={styles.searchTextStyle}>
                Search
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.clearCategory()}
              style={styles.clearButtonStyle}>
              <Text transparent style={styles.searchTextStyle}>
                Clear
              </Text>
            </TouchableOpacity>
          </View>

          <ScrollView style={{marginBottom: 55}}>
            {list.length > 0 ? (
              this.renderCategory()
            ) : (
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  top: 20,
                }}>
                <Text>{this.state.listStatus}</Text>
              </View>
            )}
            <View style={{height: 20}} />
          </ScrollView>

          {list.length > 8 && (
            <View style={styles.listViewStyle}>
              <TouchableOpacity
                onPress={() => this.onPrev()}
                style={styles.prevNextButton}>
                <MaterialIcons
                  name="arrow-back"
                  size={20}
                  style={styles.prevNextIconStyle}
                />
                <Text transparent style={styles.prevNextTextStyle}>
                  PREV
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.onNext()}
                style={styles.prevNextButton}>
                <Text transparent style={styles.prevNextTextStyle}>
                  NEXT
                </Text>
                <MaterialIcons
                  name="arrow-forward"
                  size={20}
                  style={styles.prevNextIconStyle}
                />
              </TouchableOpacity>
            </View>
          )}
          <Loading isLoading={this.state.isLoading} />
        </Box>
        {/* </Container> */}
      </SafeAreaProvider>
    );
  }
  onNext() {
    // console.log(list.length - this.state.count)
    if (list.length - (this.state.count + 8) >= 1) {
      this.setState({next: true, count: this.state.count + 8});
    }
  }

  onPrev() {
    // console.log(this.state.count + 8)
    if (this.state.count + 8 != 8) {
      this.setState({next: true, count: this.state.count - 8});
    }
  }

  searchCategory() {
    if (this.state.searchedCategories.length > 0) {
      this.setState({searchedCategories: []});
    }
    let categoryNameList = [],
      searchList = [],
      searchedCategoriesList = [];
    // console.log(this.state.categories)
    this.state.categories.map((singleCat) => {
      categoryNameList.push(singleCat.category);
    });
    // console.log(categoryNameList)
    categoryNameList.map((ctgyName, i) => {
      if (
        ctgyName.toLowerCase().includes(this.state.text.toLowerCase().trim(' '))
      ) {
        searchList.push(ctgyName);
        searchedCategoriesList.push(this.state.categories[i]);
      }
    });

    this.setState({searchedCategories: searchedCategoriesList, count: 0});
    // console.log(searchedCategoriesList)
  }

  clearCategory() {
    this.setState({searchedCategories: [], text: ''});
  }

  getCategories() {
    const SELF = this;
    let userid = '0';
    // if (screen === 'LandingPage' && this.props.userid != null) {
    if (screen === 'LandingPage' && this.props.user.userid != null) {
      userid = this.props.user.userid;
    } else {
      userid = '0';
    }

    AsyncStorage.getItem('apiToken')
      .then((value) => {
        // console.log(value);
        if (value) {
          value = value;
        } else {
          value = 'Unauthenticated-user-no-api-token';
        }

        axios({
          url:
            'https://raott.com/api/v2/TreasureHuntCategories/' + userid + '/',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {
            let categoryList = [];
            // console.log('Game Categories')
            // console.log(response);

            Object.keys(response.data.categories).map((key) => {
              categoryList.push(response.data.categories[key]);
            });
            SELF.setState({
              isLoading: false,
              categories: categoryList,
              listStatus: 'No data found',
            });
          })
          .catch(function (error) {
            // console.log(error);
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.getCategories());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

Category.navigationOptions = {
  header: null,
  gesturesEnabled: false,
  drawerLockMode: 'locked-closed',
};
function bindAction(dispatch) {
  return {};
}

const mapStateToProps = (state) => ({
  user: state.user.data,
});

export default connect(mapStateToProps, bindAction)(Category);
