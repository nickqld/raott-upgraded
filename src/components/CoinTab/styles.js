import {Dimensions, Platform } from 'react-native'
import { height } from 'styled-system';

const React = require("react-native");

const { StyleSheet } = React;


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: "#FFF"
  },
  containerTab: {
    backgroundColor: "#FFF",




    
    // minWidth: '20%',
    // maxWidth: 500,
    // minHeight: '10%',
    // maxHeight: 150,

    // flex: 1,
    // flexDirection: 'column',
    // flexWrap: 'wrap',
    // alignSelf: 'center', // if you want to fill rows left to right
    // height:100

    // alignSelf: 'center',
    // justifyContent: 'flex-start'
    // justifyContent: 'space-evenly',
    // flexDirection: 'column',
    // flexWrap: 'wrap',

      // borderWidth:3,
      // borderColor:'black',
      // alignItems:'center',
      // flexDirection:'row',
      // height:200,
      // justifyContent:'space-evenly',
      // marginLeft:20
      // alignSelf:'center',
      // flex:1,
      // position:'absolute',
      // ...StyleSheet.absoluteFillObject
      // alignSelf:'stretch',
      // flex: 1,
      // flexDirection:"column",
      // justifyContent: 'space-between',
  },
imagestyle:{
	width: 90, height: 90,alignSelf:'center',
},
textTotalStyle:{fontSize:18,color:'#e02d2e',alignSelf:'center',textAlign:'center'},
textAudStyle:{fontSize:12,color:'#91908c'},

tabBarLine: {
    borderBottomWidth:4, 
    borderBottomColor: '#e02d2e'
  },
  
  blueColor: {
    backgroundColor: 'rgba(247,247,247,1)'
  },
  whiteColor: {
    color: 'black',
    fontSize:14
  },
  
  tabBarText: {
    color: '#e02d2e', 
    fontSize:14
    
  },

 containerDialog: {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: 'rgba(150,150,150, 0.8)',
  alignItems: 'center',
  justifyContent: 'center',
  
},
goBackButton:
{
    height:44,
    width:44,
    alignItems:'center',
    justifyContent:'center'
},

headerText:
{  
    height:44,
    width: deviceWidth - 98,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 5
},
headerStyle:{
  backgroundColor:"#f2f2f2",
  flexDirection:'row',
  height:44,
  marginTop: (Platform.OS === 'ios') ? 20 : 0,
},
viewStyle: {
  // borderWidth:3,
  // borderColor:'black',
  // alignItems:'center',
  // flexDirection:'row',
  // height:200,
  // justifyContent:'space-evenly'
},





};
