import React, { Component } from 'react';

import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  View,
  Body,
  Icon,
} from 'native-base';
import { Dimensions, Image, ScrollView } from 'react-native';
import styles from './styles';
import { TouchableOpacity } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import { NativeBaseProvider, Box, Center } from 'native-base';
export default class Common extends Component {
  // eslint-disable-line

  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      coin: 0,
      noOfCoins: 0,
      screen: this.props.screen,
      catid: this.props.catid,
      collectionid: this.props.collectionid,
      // fid: this.props.fid
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.isClearQty !== this.props.isClearQty) {
      if (nextProps.isClearQty) {
        this.setState({
          noOfCoins: 0,
        });
      }
    }
  }

  increment(_raotts, id, image, coin) {
    if (this.state.noOfCoins < _raotts) {
      this.setState({
        noOfCoins: this.state.noOfCoins + 1,
      });
      this.props.coinTransferClick(id, this.state.noOfCoins + 1, image, coin);
    }
  }

  decrement(id, image, coin) {
    if (this.state.noOfCoins > 0) {
      this.setState({
        noOfCoins: this.state.noOfCoins - 1,
      });
      this.props.coinTransferClick(id, this.state.noOfCoins - 1, image, coin);
    }
  }

  onFullCardClick(rotating_image, coin, _raotts, id, index) {
    if (this.props.screen != 'FriendList' && _raotts != '0') {
      this.props.onCardClick(rotating_image, coin, _raotts, id, index);
    }
  }

  render() {
    let renderCoin = null;
    // eslint-disable-line
    const { singleCoin, coinTransferClick, onCardClick, index } = this.props;
    const {
      image,
      coin,
      numberOfCoins,
      _raotts,
      id,
      rotating_image,
    } = singleCoin;

    return (
      <View style={{ marginLeft: 40, marginTop: 20, marginRight: 35 }}>
        {/* <Text>
              {screen}
      </Text> */}
      {/* <TouchableOpacity
            onPress={() =>
              this.onFullCardClick(rotating_image, coin, _raotts, id, index)
            }></TouchableOpacity> */}
        {this.state.screen == 'FriendsList' ? (

          <Box
            width='100%'
            height='104'
            overflow="hidden"
            borderColor="gray.300"
            borderWidth="1"
            borderRadius="3"
            alignSelf="center"
            marginTop='10'

            shadow='2'

            _web={{
              shadow: 2,
              borderWidth: 0,
            }}
            _light={{
              backgroundColor: "gray.50",
            }}
          >
            {/* <Card>
            <CardItem style={{height: 140}}> */}
            {_raotts == '0' ? (
              <View
                style={{
                  justifyContent: 'center',
                  width: deviceWidth * 0.46 - 30,
                }}>
                <Image
                  style={styles.imagestyle}
                  source={require('../../../img/blank_coin.png')}
                />
              </View>
            ) : (
              <View
                style={{
                  justifyContent: 'center',
                  width: deviceWidth * 0.46 - 30,
                }}>
                <Image
                  style={styles.imagestyle}
                  source={{ uri: 'https:' + image }}
                />
              </View>
            )}

            <View
              style={{
                width: deviceWidth * 0.46 - 40,
                height: 140,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#ffffdf',
              }}>
              <Text style={styles.textTotalStyle}> {coin} </Text>
              <Text style={styles.textAudStyle}>No. of coins {_raotts}</Text>

              <View
                style={{
                  height: 55,
                  marginTop: 2,
                  backgroundColor: 'white',
                  width: 80,
                  borderRadius: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    position: 'absolute',
                    marginTop: 8,
                    marginLeft: 10,
                    padding: 4,
                  }}>
                  {this.state.noOfCoins}
                </Text>
                <View
                  style={{
                    flexDirection: 'column',
                    position: 'absolute',
                    right: 10,
                  }}>
                  <TouchableOpacity
                    style={{ marginLeft: 20 }}
                    onPress={() => this.increment(_raotts, id, image, coin)}>
                    <Icon name="ios-arrow-up" style={{ fontSize: 23 }} />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ marginLeft: 20 }}
                    onPress={() => this.decrement(id, image, coin)}>
                    <Icon name="ios-arrow-down" style={{ fontSize: 23 }} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            {/* </CardItem>
          </Card> */}
          </Box>
        ) : (
          <TouchableOpacity
            onPress={() =>
              this.onFullCardClick(rotating_image, coin, _raotts, id, index)
            }>
            <Box
              width='100%'
              height='104'
              overflow="hidden"
              borderColor="gray.300"
              borderWidth="1"
              borderRadius="3"
              alignSelf="center"
              marginTop='10'

              shadow='2'

              _web={{
                shadow: 2,
                borderWidth: 0,
              }}
              _light={{
                backgroundColor: "gray.50",
              }}
            >
              {/* <Card>
              <CardItem style={{height: 140}}> */}
              {_raotts == '0' ? (
                <View
                  style={{
                    justifyContent: 'center',
                    width: deviceWidth * 0.46 - 30,
                  }}>
                  <Image
                    style={styles.imagestyle}
                    source={require('../../../img/blank_coin.png')}
                  />
                </View>
              ) : (
                <View
                  style={{
                    justifyContent: 'center',
                    width: deviceWidth * 0.46 - 30,
                  }}>
                  <Image
                    style={styles.imagestyle}
                    source={{ uri: 'https:' + image }}
                  />
                </View>
              )}

              <View
                style={{
                  width: deviceWidth * 0.46 - 40,
                  height: 120,
                  position: 'absolute',
                  right: 0,
                  height: 140,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#ffffdf',
                }}>
                <Text style={styles.textTotalStyle}> {coin} </Text>
                <Text style={styles.textAudStyle}>
                  No. of coins {_raotts}
                </Text>
              </View>
              {/* </CardItem>
            </Card> */}
            </Box>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
