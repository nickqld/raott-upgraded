import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Dimensions, StyleSheet, Animated, Easing} from 'react-native';
import {Svg, Path} from 'react-native-svg';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  overlayView: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 1.0,
    backgroundColor: 'rgba(100, 100, 100, 0.6)',
    width: deviceWidth,
    height: deviceHeight,
    zIndex: 2,
  },
  activityIndicator: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: 80,
    width: 70,
    marginTop: deviceHeight * 0.5 - 40,
    color: '#b71c1c',
  },
  activityIndicator1: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: 80,
    color: '#b71c1c',
    marginTop: deviceHeight * 0.5 - 40,
  },
});

export default class Loading extends Component {
  static propTypes = {
    isLoading: PropTypes.bool,
  };

  // Specifies the default values for props:
  static defaultProps = {
    isLoading: false,
  };

  constructor(props) {
    super(props);
    this.spinValue = new Animated.Value(0);
  }

  componentDidMount() {
    this.spin();
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 2000,
      easing: Easing.linear,
    }).start(() => this.spin());
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    if (this.props.isLoading == true) {
      return (
        <View style={styles.overlayView}>
          <Animated.View
            style={{
              width: 50,
              height: 50,
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              marginTop: deviceHeight * 0.5 - 40,
              transform: [{rotate: spin}],
            }}>
            <Svg
              width="70"
              height="70"
              fontSize="70"
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                marginLeft: 20,
                marginTop: 20,
              }}>
              <Path
                fill="red"
                d="M23.98,0.04c-13.055,0-23.673,10.434-23.973,23.417C0.284,12.128,8.898,3.038,19.484,3.038c10.76,0,19.484,9.395,19.484,20.982c0,2.483,2.013,4.497,4.496,4.497c2.482,0,4.496-2.014,4.496-4.497C47.96,10.776,37.224,0.04,23.98,0.04z M23.98,48c13.055,0,23.673-10.434,23.972-23.417c-0.276,11.328-8.89,20.42-19.476,20.42    c-10.76,0-19.484-9.396-19.484-20.983c0-2.482-2.014-4.496-4.497-4.496C2.014,19.524,0,21.537,0,24.02C0,37.264,10.736,48,23.98,48z"
              />
            </Svg>
          </Animated.View>
        </View>
      );
    } else {
      return <View />;
    }
  }
}
