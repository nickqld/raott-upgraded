import React, {Component} from 'react';
import {
  View,
  ActivityIndicator,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './styles';
// import PropTypes from 'prop-types';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
// let total = 0

class CoinCombinationDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCoin: '',
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('selectedCoin')
      .then(value => {
        // console.log(value)
        this.setState({selectedCoin: value});
      })
      .done();
  }
  renderImages() {
    detailData = [];
    Object.keys(this.props.details).map(key => {
      detailData.push(this.props.details[key]);
    });
    return detailData.map((detail, i) => {
      return (
        <View key={i}>
          <View style={styles.coinDetailView}>
            <Text style={styles.coinDetailText}>{detail.id % 100}.</Text>
            <Text style={[styles.coinDetailText, {marginLeft: 3}]}>
              {detail.coin}
            </Text>
          </View>
          <Image
            style={styles.coinImageStyle}
            source={{uri: 'https:' + detail.image}}
          />
          {/* </Image> */}
          <Text style={[styles.coinDetailText, {marginBottom: 35}]}>
            No. of coins {detail.quantity}
          </Text>
        </View>
      );
    });
  }

  render() {
    // console.log(this.state.selectedCoin)
    const {details, method} = this.props;
    return (
      <View style={styles.containerDialog}>
        <View style={styles.innerContainerDialog}>
          <TouchableOpacity onPress={() => this.props.method()}>
            {/* <Image style={{ width: 30, height: 30, alignSelf: 'flex-end', margin: 5 }}
                         source={require('../../../img/exit.png')}>
                        </Image> */}
            <MaterialIcons name="close" size={20} style={styles.closeIcon} />
          </TouchableOpacity>

          <View style={[styles.innerContainerDialog, {alignItems: 'center'}]}>
            <Text style={styles.dialogTextHeader}>
              Details of the World Raott coin combination to by this treasure.
            </Text>
            <View style={styles.headerLineStyle} />
            <ScrollView style={styles.scrollViewStyle}>
              {this.renderImages()}
            </ScrollView>
          </View>

          <View style={styles.headerLineStyle} />

          <View style={{alignItems: 'flex-end'}}>
            <TouchableOpacity onPress={() => this.props.method()}>
              <Text style={styles.closeButtonStyle}>Close</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default CoinCombinationDialog;
