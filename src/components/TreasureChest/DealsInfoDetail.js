import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
  Card,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
} from 'react-native';
//import {DrawerNavigator} from 'react-navigation';
import axios from 'axios';
import styles from './styles';
import Moment from 'moment';
import numeral from 'numeral';
import _ from 'lodash';
import FastImage from 'react-native-fast-image';

const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';

class DealsInfoDetail extends Component {
  constructor(props) {
    super(props);
    this.clickDebounce = _.debounce(this.click.bind(this), 1000, {
      leading: true,
      trailing: false,
    });
  }
  click = (id, index) => {
    this.props.onCardClick(id, index);
  };
  render() {
    const { deal, deal1 } = this.props;
    const { onCardClick } = this.props;
    let quantity = 0;
    let userHas = 0;
    let div = 0;
    let quantity1 = 0;
    let userHas1 = 0;
    let div1 = 0;
    let divison = 0;
    let divison1 = 0;
    if (deal._coinCombination) {
      deal._coinCombination.map((coin, i) => {
        if (coin.quantity < coin.userHas) {
          quantity = quantity + parseInt(coin.quantity);
          userHas = userHas + parseInt(coin.quantity);
        } else {
          quantity = quantity + parseInt(coin.quantity);
          userHas = userHas + parseInt(coin.userHas);
        }

      });
    }

    divison = (userHas / quantity) * 100;
    div = (userHas / quantity) * (deviceWidth / 2.4);
    if (div > 0) {
      div = div;
    } else {
      div = 0;
    }

    deal1 &&
      deal1._coinCombination &&
      deal1._coinCombination.map((coin, i) => {
        // quantity1=quantity1+parseInt(coin.quantity)
        // userHas1=userHas1+parseInt(coin.userHas)

        if (coin.quantity < coin.userHas) {
          quantity1 = quantity1 + parseInt(coin.quantity);
          userHas1 = userHas1 + parseInt(coin.quantity);
        } else {
          quantity1 = quantity1 + parseInt(coin.quantity);
          userHas1 = userHas1 + parseInt(coin.userHas);
        }
      });
    divison1 = (userHas1 / quantity1) * 100;
    div1 = (userHas1 / quantity1) * (deviceWidth / 2.4);
    if (div1 > 0) {
      div1 = div1;
    } else {
      div1 = 0;
    }

    return (
      <SafeAreaProvider style={{ flex: 1 }}>
        <View style={styles.mainView}>

          <Box
            width='48%'

            overflow="hidden"
            borderColor="gray.300"
            borderWidth="1"
            borderRadius="3"
            p="0"
            shadow='2'

            _web={{
              shadow: 2,
              borderWidth: 0,
            }}
            _light={{
              backgroundColor: "gray.50",
            }}
          >
            {/* <Card style={{width: '48%'}}> */}
            <TouchableOpacity
              onPress={() =>
                this.click(
                  this.props.treasure ? deal.treasure_id : deal.id,
                  this.props.currentIndex,
                )
              }>
              {/* <CardItem style={styles.cardStyle}> */}
              <View display='flex' flexDirection='column' alignItems='center' >
                <FastImage
                  style={styles.imagestyle}
                  source={{
                    uri: this.props.treasure
                      ? deal.media[0].image.split('"')[1]
                      : deal.image,
                    priority: FastImage.priority.high,
                    cache: FastImage.cacheControl.immutable,
                  }}
                  resizeMode={FastImage.resizeMode.contain}
                />
                <View style={styles.middleViewStyle}>
                  <Text
                    numberOfLines={1}
                    style={{ fontSize: 18, marginBottom: 1 }}>
                    {this.props.treasure ? deal.treasure_title : deal.title}
                  </Text>
                  <Text numberOfLines={1} style={styles.roattText}>
                    R{' '}
                    {numeral(
                      this.props.treasure
                        ? deal.treasure_amount
                        : deal._price.raotts,
                    ).format('0,0')}{' '}

                  </Text>
                  {/* {!this.props.treasure && (
                    <TouchableOpacity
                      onPress={combination =>
                        this.props.onCardCoinCombinationClick(
                          deal._coinCombination,
                        )
                      }>
                      <Text style={styles.combinationText}>
                        View Coin Combination
                      </Text>
                    </TouchableOpacity>
                  )} */}
                </View>
                {/* {!this.props.treasure && (
                  <View style={styles.progressBar}>
                    <View
                      style={{
                        flexDirection: 'row',
                        borderRadius: 10,
                        width: div,

                        height: 20,
                        borderColor: 'rgba(216,0,0,1)',
                        backgroundColor: 'green',
                      }}
                    />
                    <Text
                      numberOfLines={1}
                      style={{
                        justifyContent: 'center',
                        marginLeft: 10,
                        marginTop: 0,
                        fontSize: 10,
                        position: 'absolute',
                        backgroundColor: 'transparent',
                      }}>
                      {numeral(divison).format('0,0.00')} %
                    </Text>
                  </View>
                )} */}

                <View style={styles.footerMain}>
                  <Text style={styles.dateStyle}>
                    {this.props.treasure
                      ? deal.text1
                      : `Valid till ${Moment(deal.expiry).format(
                        'D MMM YYYY ',
                      )}`}
                  </Text>
                  <View style={styles.seperator} />
                  <Text style={styles.dealstextstyle}>
                    {this.props.treasure
                      ? deal.text2
                      : `Number of treasures left ${deal.number_of_deals}`}
                  </Text>
                </View>
                {/* </CardItem> */}
              </View>
            </TouchableOpacity>
            {/* </Card> */}

          </Box>


          {deal1 ? (
            <Box width='48%'
              marginBottom="0"
              overflow="hidden"
              borderColor="gray.300"
              borderWidth="1"
              borderRadius="3"
              p="0"
              shadow='2'

              _web={{
                shadow: 2,
                borderWidth: 0,
              }}
              _light={{
                backgroundColor: "gray.50",
              }}
            >
              {/* <Card style={{width: '48%'}}> */}
              <TouchableOpacity
                onPress={() =>
                  this.click(
                    this.props.treasure ? deal1.treasure_id : deal1.id,
                    this.props.currentIndexOne,
                  )
                }>
                {/* <CardItem style={styles.cardStyle}> */}
                <Box display='flex' flexDirection='column' alignItems='center'>
                  <FastImage
                    style={styles.imagestyle}
                    source={{
                      uri: this.props.treasure
                        ? deal1.media[0].image.split('"')[1]
                        : deal1.image,
                      priority: FastImage.priority.high,
                      cache: FastImage.cacheControl.immutable,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                  <View style={styles.middleViewStyle}>
                    <Text
                      numberOfLines={1}
                      style={{ fontSize: 18, marginBottom: 1 }}>
                      {this.props.treasure ? deal1.treasure_title : deal1.title}
                    </Text>

                    <Text numberOfLines={1} style={styles.roattText}>
                      R{' '}
                      {numeral(
                        this.props.treasure
                          ? deal1.treasure_amount
                          : deal1._price.raotts,
                      ).format('0,0')}{' '}

                    </Text>

                    {/* {!this.props.treasure && (
                      <TouchableOpacity
                        onPress={combination =>
                          this.props.onCardCoinCombinationClick(
                            deal1._coinCombination,
                          )
                        }>
                        <Text style={styles.combinationText}>
                          View Coin Combination
                        </Text>
                      </TouchableOpacity>
                    )} */}
                  </View>
                  {/* borderWidth: 1,
    borderColor: 'rgba(226,218,218,1)',
    marginTop: 10,
    borderRadius: 10,
    width: deviceWidth/2.4,
    backgroundColor: 'rgba(240,233,233,1)' */}
                  {/* {!this.props.treasure && (
                    <View style={styles.progressBar}>
                      <View
                        style={{
                          flexDirection: 'row',
                          borderRadius: 10,
                          width: div1,
                          height: 20,
                          borderColor: 'rgba(216,0,0,1)',
                          backgroundColor: 'green',
                        }}
                      />
                      <Text
                        numberOfLines={1}
                        style={{
                          justifyContent: 'center',
                          marginTop: 0,
                          marginLeft: 10,
                          fontSize: 10,
                          position: 'absolute',
                          backgroundColor: 'transparent',
                        }}>
                        {numeral(divison1).format('0,0.00')} %
                      </Text>
                    </View>
                  )} */}

                  <View style={styles.footerMain}>
                    <Text style={styles.dateStyle}>
                      {this.props.treasure
                        ? deal1.text1
                        : `Valid till ${Moment(deal.expiry).format(
                          'D MMM YYYY ',
                        )}`}
                    </Text>
                    <View style={styles.seperator} />
                    <Text style={styles.dealstextstyle}>
                      {this.props.treasure
                        ? deal1.text2
                        : `Number of treasures left ${deal1.number_of_deals}`}
                    </Text>
                  </View>
                  {/* </CardItem> */}
                </Box>
              </TouchableOpacity>
              {/* </Card> */}
            </Box>
          ) : (
            <View style={{ flex: 1 }} />
          )}
        </View>
      </SafeAreaProvider>
    );
  }
}

export default DealsInfoDetail;
