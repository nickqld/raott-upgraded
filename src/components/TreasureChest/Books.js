import React, { Component } from 'react';
import { Container, Content, Card, CardItem, Text, Body } from 'native-base';
import {
  Dimensions,
  Image,
  ScrollView,
  View,
  TouchableOpacity,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';
import axios from 'axios';
import Loading from '../Loading/';
import DealsInfoDetail from './DealsInfoDetail.js';
import { SafeAreaProvider } from 'react-native-safe-area-context';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
import { NativeBaseProvider, Box, Center } from 'native-base';
import { FlatList, TouchableWithoutFeedback } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
export default class Books extends Component {
  constructor(props) {
    super(props);
    SELF = this;
  }

  onCardClick(id, index) {
    console.log('onCardClick id', id);
    console.log('onCardClick index', index);
    console.log('Treasure', this.props.list[index]);
    console.log('onCardClick ddd');
    if (this.props.screen === 'LandingPage') {
      this.props.navigation.navigate('DealsDetails', {
        id: id,
        onRefresh: this.props.onRefresh,
        treasure: this.props.treasure || false,
        treasureData: this.props.list[index],
      });
    } else {
      this.props.navigation.navigate('LoginForm');
    }
  }

  renderDeals() {
    console.log('Books.js renderDeals---------------------', this.props.list.length + "");
    let dealArray = [];
    for (let i = 0; i < this.props.list.length; i += 2) {

      let prod = this.props.list[i];
      let prod2 = this.props.list[i + 1];
      if (prod2 != null) {
        dealArray.push(
          <DealsInfoDetail
            treasure={this.props.treasure || false}
            key={i}
            deal={prod}
            deal1={prod2}
            currentIndex={prod.itemIndex || i}
            currentIndexOne={(prod2 && prod2.itemIndex) || i + 1}
            onCardClick={(id, index) => this.onCardClick(id, index)}
            onCardCoinCombinationClick={combination =>
              this.props.onCardCoinCombinationClick(combination)
            }
          />,
        );
      } else {
        dealArray.push(
          <DealsInfoDetail
            key={i}
            deal={prod}
            currentIndex={prod.itemIndex || i}
            currentIndexOne={(prod2 && prod2.itemIndex) || i + 1}
            treasure={this.props.treasure || false}
            deal1={null}
            onCardClick={(id, index) => this.onCardClick(id, index)}
            onCardCoinCombinationClick={combination =>
              this.props.onCardCoinCombinationClick(combination)
            }
          />,
        );
      }
    }
    return dealArray;
  }

  render() {
    // console.warn(this.props.screen)
    return (

      <SafeAreaView style={{ flex: 1, }}>
        <Box>
          {/* <Container> */}
          {this.props.list.length === 0 ? (
            <View />
          ) : (

            <FlatList
              style={{
                backgroundColor: '#f9fafc',
                borderWidth: 1,
                borderColor: '#dedede',


              }}
              contentContainerStyle={styles.contentContainer}

              data={this.renderDeals()}

              renderItem={({ item }) =>
                <View >
                  {item}
                </View>


              }
            />


          )}
          {this.props.noDataText && (
            <View
              style={{
                width: deviceWidth,
                height: deviceHeight,
                backgroundColor: 'grey',
              }}> 
              <Text
                style={{
                  textAlign: 'center',
                  justifyContent: 'center',
                  top: 20,
                }}>
                {' '}
                {this.props.treasure ? 'No treasure found' : 'No deals found'}
              </Text>
            </View>
          )}
          {/* </Container> */}
        </Box>
      </SafeAreaView>
    );
  }
}
