import React, {Component} from 'react';
import {
  View,
  ActivityIndicator,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './styles';
// import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
let total = 0;

class ConfirmBuyDeal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCoin: '',
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('selectedCoin')
      .then(value => {
        // console.log(value)
        this.setState({selectedCoin: value});
      })
      .done();
  }

  renderImages() {
    detailData = [];
    Object.keys(this.props.details).map(key => {
      detailData.push(this.props.details[key]);
    });
    return detailData.map((detail, i) => {
      return (
        <View key={i}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'black', fontSize: 12, textAlign: 'center'}}>
              {detail.id % 100}.
            </Text>
            <Text
              style={{
                color: 'black',
                fontSize: 12,
                textAlign: 'center',
                marginLeft: 3,
              }}>
              {detail.coin}
            </Text>
          </View>
          <Image
            style={{width: 80, height: 80, alignSelf: 'center'}}
            source={{uri: 'https:' + detail.image}}></Image>
          <Text
            style={{
              marginBottom: 35,
              color: 'black',
              fontSize: 12,
              textAlign: 'center',
            }}>
            No. of coins {detail.quantity}
          </Text>
        </View>
      );
    });
  }

  render() {
    // console.log(this.state.selectedCoin)
    const {details, method, buttonClicked} = this.props;

    return (
      <View style={styles.containerDialog}>
        <View style={styles.innerContainerDialog}>
          <Text style={styles.confirmBuyHeader}>Confirmation</Text>

          <View style={styles.headerLineStyle} />

          <View
            style={[
              styles.innerContainerDialog,
              {alignItems: 'center', marginTop: 10},
            ]}>
            <Text style={styles.textSubHeader}>
              Are you sure you want to buy this treasure?The following{' '}
              {this.props._price_raotts} World Raott coins (
              {this.props._price_currency}) will be deducted from your account.
            </Text>
            <ScrollView
              style={{height: deviceHeight * 0.45, width: deviceWidth * 0.8}}>
              {this.renderImages()}
            </ScrollView>
          </View>

          <View style={styles.headerLineStyle} />
          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <TouchableOpacity onPress={() => this.props.method('YES')}>
              <Text
                style={{
                  width: 100,
                  borderColor: 'rgba(216,216,216,1)',
                  borderWidth: 1,
                  margin: 10,
                  color: 'black',
                  fontSize: 10,
                  textAlign: 'center',
                  padding: 10,
                  backgroundColor: 'rgba(238,238,238,1)',
                }}>
                Yes, Buy Now
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.method('CANCEL')}>
              <Text
                style={{
                  width: 80,
                  borderColor: 'rgba(216,216,216,1)',
                  borderWidth: 1,
                  margin: 10,
                  color: 'black',
                  fontSize: 10,
                  textAlign: 'center',
                  padding: 10,
                  backgroundColor: 'rgba(238,238,238,1)',
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default ConfirmBuyDeal;
