import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  TabBar,
  TabViews,
  // TabView,
  Tab,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  NetInfo,
  BackHandler,
  StatusBar,
  Animated,
  Pressable,
  StyleSheet
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import SafeAreaView from 'react-native-safe-area-view';
import styles from './styles';
import Bpartooks from './Books.js';
import Loading from '../Loading/';
import axios from 'axios';
import CoinCombinationDialog from './CoinCombinationDialog.js';
import { InternetCheck, TimeOutError } from '../common/';
import { NativeBaseProvider, Box, Center } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SafeAreaProvider } from 'react-native-safe-area-context';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
//import { TabView, SceneMap } from 'react-native-tab-view';

import DynamicTabView from "react-native-dynamic-tab-view";
import DynamicTabViewDeals from '../DynamicTabViewDeals';
// const getRoutes = titles => {
//   let routes = []

//   titles.forEach(title => {
//     routes.push({
//       key: title,
//       title: title
//     })
//   })

//   return routes
// }
// const getSceneMap = (routes, scenes) => {
//   let sceneMap = {}

//   routes.forEach((route, index) => {
//     sceneMap[route.key] = scenes[index]
//   })

//   return sceneMap
// }
// let categoryId = [],
//   categoryName = [];

class Deals extends React.Component {
  constructor(props) {
    super(props);
    //this.data=[];
    this.state = {
      isLoading: true,
      deals: [],
      dealCategory: [],
      noDataText: false,
      coin_combination: '',
      renderCoinCombination: false,
      index: 0,
      // data:[],
      // dataarray: [
      //   { title: "thiisfirstTab1", key: "item1", color: "blue" },
      //   { title: "thiisfirstTab2", key: "item2", color: "pink" },
      //   { title: "Tabdsdd3", key: "item3", color: "blue" },
      //   { title: "ddddTab4", key: "item4", color: "yellow" },
      //    { title: "5tqb", key: "item5", color: "blue" },
      //   { title: "Tab6", key: "item6", color: "yellow" },
      //    { title: "Tab7", key: "item7", color: "blue" },
      //   { title: "Tab8", key: "item8", color: "yellow" },
      //   { title: "thiisfirstTab1", key: "item9", color: "blue" },
      //   { title: "thiisfirstTab2", key: "item12", color: "pink" },
      //   { title: "Tabdsdd3", key: "item13", color: "blue" },
      //   { title: "ddddTab4", key: "item14", color: "yellow" },
      //    { title: "5tqb", key: "item15", color: "blue" },
      //   { title: "Tab6", key: "item16", color: "yellow" },
      //    { title: "Tab7", key: "item17", color: "blue" },
      //    { title: "Tabdsdd3", key: "item23", color: "blue" },
      //    { title: "ddddTab4", key: "item24", color: "yellow" },
      //     { title: "5tqb", key: "item25", color: "blue" },
      //    { title: "Tab6", key: "item26", color: "yellow" },
      //     { title: "Tab7", key: "item27", color: "blue" },
      //      ],


      // routes: [
      //   { key: 'first', title: 'First' },
      //   { key: 'second', title: 'Second' },
      // ],
      // routes: [],
      //routes: getRoutes([ 'First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth', 'Seventh' ]),
      defaultIndex: 0,

    };
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
    //**************** api call to fetch deals ************************/

    this.getdealCategories();
    this.getDealsData();

  }

  //   _setdata(){
  //     console.log('-----_setdata-------------');
  //     let dataarray = [
  //       { title: "thiisfirstTab1", key: "item1", color: "blue" },
  //       { title: "thiisfirstTab2", key: "item2", color: "pink" },
  //       { title: "Tabdsdd3", key: "item3", color: "blue" },
  //       { title: "ddddTab4", key: "item4", color: "yellow" },
  //        { title: "5tqb", key: "item5", color: "blue" },
  //       { title: "Tab6", key: "item6", color: "yellow" },
  //        { title: "Tab7", key: "item7", color: "blue" },
  //       { title: "Tab8", key: "item8", color: "yellow" },
  //       { title: "thiisfirstTab1", key: "item9", color: "blue" },
  //       { title: "thiisfirstTab2", key: "item12", color: "pink" },
  //       { title: "Tabdsdd3", key: "item13", color: "blue" },
  //       { title: "ddddTab4", key: "item14", color: "yellow" },
  //        { title: "5tqb", key: "item15", color: "blue" },
  //       { title: "Tab6", key: "item16", color: "yellow" },
  //        { title: "Tab7", key: "item17", color: "blue" },
  //        { title: "Tabdsdd3", key: "item23", color: "blue" },
  //        { title: "ddddTab4", key: "item24", color: "yellow" },
  //         { title: "5tqb", key: "item25", color: "blue" },
  //        { title: "Tab6", key: "item26", color: "yellow" },
  //         { title: "Tab7", key: "item27", color: "blue" },
  //          ];


  //     // dataarray.map((item, i) => {
  //     //   this.data.push({ title: item.title, key: item.key, color:item.color })
  //     //     console.log('---------------------',item.title);
  //     //   })

  //       // this.state.dataarray.map((item, i) => {
  //       //   console.log('------data pushed for tab name--sass-------------',item.key);
  //       //   this.data.push({ title: item.title , key:item.key })

  //       //   })

  // this.state.dealCategory.map((item, i) => {
  //         console.log('------data pushed for tab name--sass-------------',item.key);
  //         this.data.push({ title: item.category_name , key:item.id })

  //         })

  //         this.setState({    });
  //     //set route here

  // //this.data=tabArray;
  //   }

  // _renderItem = (item, index) => {
  //   console.log("renderItem", index);
  //   let dataarray = [
  //     { title: "thiisfirstTab1", key: "item1", color: "blue" },
  //     { title: "thiisfirstTab2", key: "item2", color: "pink" },
  //     { title: "Tabdsdd3", key: "item3", color: "blue" },
  //     { title: "ddddTab4", key: "item4", color: "yellow" },
  //      { title: "5tqb", key: "item5", color: "blue" },
  //     { title: "Tab6", key: "item6", color: "yellow" },
  //      { title: "Tab7", key: "item7", color: "blue" },
  //     { title: "Tab8", key: "item8", color: "yellow" },
  //     { title: "thiisfirstTab1", key: "item9", color: "blue" },
  //     { title: "thiisfirstTab2", key: "item12", color: "pink" },
  //     { title: "Tabdsdd3", key: "item13", color: "blue" },
  //     { title: "ddddTab4", key: "item14", color: "yellow" },
  //      { title: "5tqb", key: "item15", color: "blue" },
  //     { title: "Tab6", key: "item16", color: "yellow" },
  //      { title: "Tab7", key: "item17", color: "blue" },
  //      { title: "Tabdsdd3", key: "item23", color: "blue" },
  //      { title: "ddddTab4", key: "item24", color: "yellow" },
  //       { title: "5tqb", key: "item25", color: "blue" },
  //      { title: "Tab6", key: "item26", color: "yellow" },
  //       { title: "Tab7", key: "item27", color: "blue" },
  //   ];
  //   let layout={}
  //   dataarray.forEach((arrayItem, index) => {
  //     console.log('---------------------' + arrayItem.key,item["key"]);
  //     if (item["key"] == arrayItem.key) {
  //     layout=  <View
  //       key={item["key"]}
  //       style={{ backgroundColor:item["color"], flex: 1 }}
  //     />
  //     }
  //   })
  //   return (
  //     layout
  //     );
  // };

  // state = {
  //   isLoading: true,
  //   deals: [],
  //   dealCategory: [],
  //   noDataText: false,
  //   coin_combination: '',
  //   renderCoinCombination: false,
  //   index: 0,
  //   // routes: [
  //   //   { key: 'first', title: 'First' },
  //   //   { key: 'second', title: 'Second' },
  //   // ],
  //   routes: [],
  //   //routes: getRoutes([ 'First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth', 'Seventh' ]),
  //   defaultIndex: 0,

  //   data: [],

  // };

  //  getScenes = () => {
  //   let scenes = [];

  //   this.state.dealCategory &&
  //     this.state.dealCategory != 'undefined' &&
  //     this.state.dealCategory.map((cat, i) => {

  //       let dealList = [];
  //       this.state.deals.map((singleDeal, j) => {
  //         if (cat.id == singleDeal.dealCategory) {
  //           dealList.push({ ...singleDeal, itemIndex: j });
  //         }
  //         if (dealList.length > 0) {
  //           noDataText = false;
  //         } else {
  //           noDataText = true;
  //         }
  //       });
  //       scenes.push(
  //         () =>
  //           <View style={styles1.container}

  //             key={i}  >
  //             <Books
  //               navigation={this.props.navigation}
  //               list={dealList}
  //               noDataText={noDataText}
  //               startLoader={() => this.startLoader()}
  //               stopLoader={() => this.stopLoader()}
  //               onCardCoinCombinationClick={combination =>
  //                 this.onCardCoinCombinationClick(combination)
  //               }
  //               screen={this.props.navigation.state.params.screen}
  //               onRefresh={this.onRefresh}
  //               navigateToHistory={() =>
  //                 this.props.navigation.navigate('DealHistory', {
  //                   onSelect: null,
  //                 })
  //               }
  //             />
  //           </View>
  //       )
  //     })




  //   return scenes
  // }

  // renderTabBar = props => {
  //   const inputRange = props.navigationState.routes.map((x, i) => i)

  //   return (
  //     <View style={styles1.tabBar}>
  //       <ScrollView
  //         horizontal
  //         showsHorizontalScrollIndicator={false}
  //         contentContainerStyle={styles1.tabBarScrollView}

  //       >
  //         {props.navigationState.routes.map((route, i) => {
  //           const opacity = props.position.interpolate({
  //             inputRange,
  //             outputRange: inputRange.map((inputIndex) =>
  //               inputIndex === i ? 1 : 0.5
  //             ),
  //           });
  //           const color = this.state.index === i ? '#1f2937' : '#a1a1aa';
  //           const borderColor = this.state.index === i ? 'cyan.500' : 'coolGray.200';

  //           return (
  //             <Box
  //               borderBottomWidth="3"
  //               borderColor={borderColor}
  //               flex={1}
  //               alignItems="center"
  //               p="3"
  //               cursor="pointer">
  //               <Pressable
  //                 onPress={() => {
  //                   console.log(i);
  //                   this.setState({
  //                     index: i,

  //                   });
  //                 }}>
  //                 <Animated.Text style={{ color }}>{route.title}</Animated.Text>
  //               </Pressable>
  //             </Box>
  //           );
  //         })}
  //       </ScrollView>
  //     </View>
  //   )
  // }

  // scenes = this.getScenes()
  // sceneMap = getSceneMap(this.state.routes, this.scenes)






  // renderItem = (item, index) => {
  //   console.log("------------------------===========renderItem", index);
  //   let noDataText;
  //   let layout={}
  //  this.state.dealCategory &&
  //     this.state.dealCategory != 'undefined' &&
  //     this.state.dealCategory.map((cat, i) => {
  //       console.log("in renderItem dealcateory", cat.name);
  //       let dealList = [];
  //       this.state.deals.map((singleDeal, j) => {
  //         if (cat.id == singleDeal.dealCategory) {
  //           dealList.push({ ...singleDeal, itemIndex: j });
  //         }
  //         if (dealList.length > 0) {
  //           noDataText = false;
  //         } else {
  //           noDataText = true;
  //         }
  //       });



  //       if (item["key"] == cat.id) {
  //         console.log("in renderItem creating layout", item["key"] +" "+ cat.id);
  //         layout=   <Books
  //         navigation={this.props.navigation}
  //         list={dealList}
  //         noDataText={noDataText}
  //         startLoader={() => this.startLoader()}
  //         stopLoader={() => this.stopLoader()}
  //         onCardCoinCombinationClick={combination =>
  //           this.onCardCoinCombinationClick(combination)
  //         }
  //         screen={this.props.navigation.state.params.screen}
  //         onRefresh={this.onRefresh}
  //         navigateToHistory={() =>
  //           this.props.navigation.navigate('DealHistory', {
  //             onSelect: null,
  //           })
  //         }
  //       />

  //         }


  //     })

  //   return (

  //     layout
  //     );

  // };

  // onChangeTab = index => {};
  // _handleIndexChange = (index) => this.setState({ index });

  // _renderTabBar = (props) => {
  //   const inputRange = props.navigationState.routes.map((x, i) => i);

  //   return (
  //     <View style={styles1.tabBar}>
  //       {props.navigationState.routes.map((route, i) => {
  //         const opacity = props.position.interpolate({
  //           inputRange,
  //           outputRange: inputRange.map((inputIndex) =>
  //             inputIndex === i ? 1 : 0.5
  //           ),
  //         });

  //         return (
  //           <TouchableOpacity
  //             style={styles1.tabItem}
  //             onPress={() => this.setState({ index: i })}>
  //             <Animated.Text style={{ opacity }}>{route.title}</Animated.Text>
  //           </TouchableOpacity>
  //         );
  //       })}
  //     </View>
  //   );
  // };

  // _renderScene = SceneMap({
  //   first: FirstRoute,
  //   second: SecondRoute,
  // });
  componentDidMount() {
    // console.log('Category page', this.props)
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }

  onRefresh = data => {
    this.getdealCategories();
    this.getDealsData();
  };

  // renderCategories() {
  //   let noDataText;

  //   return (




  //     this.state.dealCategory &&
  //     this.state.dealCategory != 'undefined' &&
  //     this.state.dealCategory.map((cat, i) => {


  //       let dealList = [];
  //       this.state.deals.map((singleDeal, j) => {
  //         if (cat.id == singleDeal.dealCategory) {
  //           dealList.push({ ...singleDeal, itemIndex: j });
  //         }
  //         if (dealList.length > 0) {
  //           noDataText = false;
  //         } else {
  //           noDataText = true;
  //         }
  //       });
  //       return (
  //         <Tab
  //           key={i}
  //           heading={cat.category_name.toUpperCase()}
  //           tabStyle={styles.blueColor}
  //           textStyle={styles.whiteColor}
  //           activeTabStyle={styles.blueColor}
  //           activeTextStyle={styles.tabBarText}>
  //           <Books
  //             navigation={this.props.navigation}
  //             list={dealList}
  //             noDataText={noDataText}
  //             startLoader={() => this.startLoader()}
  //             stopLoader={() => this.stopLoader()}
  //             onCardCoinCombinationClick={combination =>
  //               this.onCardCoinCombinationClick(combination)
  //             }
  //             screen={this.props.navigation.state.params.screen}
  //             onRefresh={this.onRefresh}
  //             navigateToHistory={() =>
  //               this.props.navigation.navigate('DealHistory', {
  //                 onSelect: null,
  //               })
  //             }
  //           />
  //         </Tab>
  //       );
  //     })
  //   );
  // }



  // getSceneMap = () => {




  //   let scenes = []


  //   this.state.dealCategory &&
  //     this.state.dealCategory != 'undefined' &&
  //     this.state.dealCategory.map((cat, i) => {

  //       let dealList = [];
  //       this.state.deals.map((singleDeal, j) => {
  //         if (cat.id == singleDeal.dealCategory) {
  //           dealList.push({ ...singleDeal, itemIndex: j });
  //         }
  //         if (dealList.length > 0) {
  //           noDataText = false;
  //         } else {
  //           noDataText = true;
  //         }
  //       });
  //       scenes.push(
  //         () =>
  //           <Books
  //             navigation={this.props.navigation}
  //             list={dealList}
  //             noDataText={noDataText}
  //             startLoader={() => this.startLoader()}
  //             stopLoader={() => this.stopLoader()}
  //             onCardCoinCombinationClick={combination =>
  //               this.onCardCoinCombinationClick(combination)
  //             }
  //             screen={this.props.navigation.state.params.screen}
  //             onRefresh={this.onRefresh}
  //             navigateToHistory={() =>
  //               this.props.navigation.navigate('DealHistory', {
  //                 onSelect: null,
  //               })
  //             }
  //           />
  //       )
  //     })

  //   let sceneMap = {}

  //   // this.state.routes.forEach((route, index) => {
  //   //   sceneMap[route.key] = scenes[index]
  //   // })


  //   this.state.routes.forEach((route, index) => {
  //     sceneMap[index] = { [route.key]: scenes[index] }
  //   })

  //   return sceneMap;
  //   //return SceneMap(this.sceneMap);
  // }

  // setRoutes() {
  //   console.log('trackicccccccccccccccccccngStatus----------------');
  //   let tabArray = [];
  //   this.state.dealCategory &&
  //     this.state.dealCategory != 'undefined' &&
  //     this.state.dealCategory.map((cat, i) => {
  //       tabArray.push({ key: i, title: cat.name })
  //     })
  //   //set route here
  //   this.setState({
  //     routes: tabArray
  //   });

  //   this.state.routes.map((route, i) => {
  //     console.log('trackingStatus----------------' + route.id);
  //   })


  // }


  render() {
    // let noDataText;
    // let ReadyToBuyList = [];
    // if (this.state.deals && this.state.deals.length) {
    //   this.state.deals.map(deal => {
    //     let quantity = 0,
    //       userHas = 0;
    //     deal._coinCombination.map((coin, i) => {
    //       if (coin.quantity < coin.userHas) {
    //         quantity = quantity + parseInt(coin.quantity);
    //         userHas = userHas + parseInt(coin.quantity);
    //       } else {
    //         quantity = quantity + parseInt(coin.quantity);
    //         userHas = userHas + parseInt(coin.userHas);
    //       }
    //     });
    //     // ************ segregating deal type according to ids of Deals *********************
    //     if (userHas >= quantity) {
    //       ReadyToBuyList.push(deal);
    //     }

    //        });
    // }

    // if (ReadyToBuyList.length > 0) {
    //   noDataText = false;
    // } else {
    //   noDataText = true;
    // }

    return (

      <SafeAreaProvider style={{ flex: 1 }}>
        {/* <Container style={styles.container}> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View style={styles.headerStyle}>
            <TouchableOpacity
              onPress={() => this.goBack()}
              style={styles.goBackButton}>

              <MaterialCommunityIcons name="arrow-left" size={24} color="white" />

              {/* <Icon
                as={<MaterialCommunityIcons name="arrow-left" size={15} />}
                style={{ color: 'white' }}
              /> */}
            </TouchableOpacity>
            <View transparent style={styles.headerText}>
              <Text style={{ color: 'white', textAlign: 'center' }}>
                TREASURES
              </Text>
            </View>
          </View>

          {/* <Tabs
            tabBarBackgroundColor="white"
            tabBarUnderlineStyle={styles.tabBarLine}
            renderTabBar={() => <ScrollableTab />}>
            <Tab
              heading="ALL TREASURES"
              tabStyle={styles.blueColor}
              textStyle={styles.whiteColor}
              activeTabStyle={styles.blueColor}
              activeTextStyle={styles.tabBarText}>
              <Books
                navigation={this.props.navigation}
                list={this.state.deals}
                noDataText={this.state.noDataText}
                startLoader={() => this.startLoader()}
                stopLoader={() => this.stopLoader()}
                onCardCoinCombinationClick={combination =>
                  this.onCardCoinCombinationClick(combination)
                }
                screen={this.props.navigation.state.params.screen}
                onRefresh={this.onRefresh}
                navigateToHistory={() =>
                  this.props.navigation.navigate('DealHistory', {
                    onSelect: null,
                  })
                }
              />
            </Tab>

            <Tab
              heading="READY TO BUY TREASURES"
              tabStyle={styles.blueColor}
              textStyle={styles.whiteColor}
              activeTabStyle={styles.blueColor}
              activeTextStyle={styles.tabBarText}>
              <Books
                navigation={this.props.navigation}
                list={ReadyToBuyList}
                noDataText={noDataText}
                startLoader={() => this.startLoader()}
                onRefresh={this.onRefresh}
                stopLoader={() => this.stopLoader()}
                screen={this.props.navigation.state.params.screen}
                onCardCoinCombinationClick={combination =>
                  this.onCardCoinCombinationClick(combination)
                }
                navigateToHistory={() =>
                  this.props.navigation.navigate('DealHistory', {
                    onSelect: null,
                  })
                }
              />
            </Tab>

            {this.renderCategories()}

          </Tabs> */}

          {/* <Tabs  
          
           >
            <Tabs.Bar
         
         renderTabBar={() => <ScrollableTab />} 
            >
              <Tabs.Tab  
               
               
          
          
            > ALL TREASURES</Tabs.Tab>
              <Tabs.Tab>Twdo</Tabs.Tab>
              <Tabs.Tab>Three</Tabs.Tab>
              <Tabs.Tab>Three</Tabs.Tab>
              <Tabs.Tab>Three</Tabs.Tab>
              <Tabs.Tab>Three</Tabs.Tab>
            </Tabs.Bar>
            <Tabs.Views>
              <Tabs.View>One</Tabs.View>
              <Tabs.View>Two</Tabs.View>
              <Tabs.View>Three</Tabs.View>
              <Tabs.View>Three</Tabs.View>
              <Tabs.View>Three</Tabs.View>
              <Tabs.View>Three</Tabs.View>
            </Tabs.Views>
          </Tabs> */}

          {/* <TabView
            navigationState={this.state}
            renderScene={SceneMap(this.getSceneMap())}
            renderTabBar={this._renderTabBar}
            onIndexChange={this._handleIndexChange}
          /> */}
          {/* <TabView
            navigationState={this.state}
            renderScene={SceneMap(this.sceneMap)}
            renderTabBar={this.renderTabBar}
            onIndexChange={index => this.setState({ index })}

          /> */}

          {/* {this.state.deals != 'undefined' &&  this.state.deals.length > 0 &&   this.state.dealCategory != 'undefined' &&
          this.state.dealCategory.length > 0 &&

           <DynamicTabView
            data={this.state.data}
            renderTab={this._renderItem}
            defaultIndex={this.state.defaultIndex}
            containerStyle={styles1.container}
            headerBackgroundColor={'white'}
            headerTextStyle={styles1.headerText}
           
            onChangeTab={this.onChangeTab}
            headerUnderlayColor={'red'}
          /> 
           } */}

          {this.state.dealCategory != 'undefined' &&
            this.state.dealCategory.length > 0 && this.state.deals.length > 0 &&
            <DynamicTabViewDeals navigation={this.props.navigation} categories={this.state.dealCategory} deals_data={this.state.deals} />
          }


          {/* {this.state.renderCoinCombination &&  this.renderCoinCombinationDialog()} */}
          <Loading isLoading={this.state.isLoading} />
          {/* </Container> */}
        </Box>
      </SafeAreaProvider>

    );
  }
  // onCardCoinCombinationClick(combination) {
  //   this.setState({
  //     coin_combination: combination,
  //     renderCoinCombination: true,
  //   });
  // }
  // renderCoinCombinationDialog() {
  //   return (
  //     <CoinCombinationDialog
  //       details={this.state.coin_combination}
  //       method={() => this.toggleCoinCombinationDialog()}
  //     />
  //   );
  // }
  // toggleCoinCombinationDialog() {
  //   this.setState({ renderCoinCombination: false });
  // }
  // startLoader() {
  //   this.setState({
  //     isLoading: true,
  //   });
  // }
  // stopLoader() {
  //   this.setState({
  //     isLoading: false,
  //   });
  // }

  getdealCategories() {
    console.log('-----getdealCategories-------------');
    const SELF = this;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        //  console.log(value);
        if (value) {
          value = value;
        } else {
          value = 'raott-app-deals-public-user';
        }
        axios({
          url: 'https://raott.com/api/res/dealCategories/',
          type: 'GET',
          method: 'GET',
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {

            // console.log('responseCategoryData');
            console.log(response.data.result);
            let dealCategoryList = [];
            Object.keys(response.data.result).map(key => {
              dealCategoryList.push(response.data.result[key]);
            });

            SELF.setState({ dealCategory: dealCategoryList });
            console.log('-----getdealCategories- sate set------------');

            SELF.setState({
              isLoading: false,
            });


            // dealCategoryList.map((cat, i) => {
            //   SELF.data.push({ title: cat.category_name, key:cat.id + "" })

            //   })
            //  SELF.setState({

            //            });
            // SELF.getDealsData();
            //  let tabArray = [];
            // console.log("============", "getdealCategorySetState.");

            //  SELF.state.dealCategory.map((cat, i) => {
            //      tabArray.push({ key:cat.id, title: cat.category_name })
            //    })
            //   console.log("============", "dadddddddta array for dynamic tabs set");

            //set route here
            //  SELF.setState({
            //    data: tabArray
            //  });
            //  console.log("============", "data array for dynamic tabs set");

          })
          .catch(function (error) {
            // console.log(error);
          }


          );
      })
      .done(); // ending statement of asyncstorage
  }

  getDealsData() {
    const SELF = this;
    if (!SELF.state.isLoading) {
      SELF.setState({ isLoading: true });
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        console.log('auth token: ' + value);
        if (value) {
          value = value;
        } else {
          value = 'raott-app-deals-public-user';
        }
        axios({
          url: 'https://raott.com/api/v2/screenDeals/',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {
            let dealList = [];
            Object.keys(response.data.deals).map(key => {
              dealList.push(response.data.deals[key]);
            });
            if (dealList.length > 0) {
              SELF.setState({
                isLoading: false,
                deals: dealList,
              });
            } else {
              SELF.setState({
                isLoading: false,
                noDataText: true,
              });
            }
            // SELF.setState({

            // });
            console.log("============", "getDealsDataSetState");
            // console.log("======getDealsDataSetState======", response.data.deals);

          })
          .catch(function (error) {
            // alert(error);
            SELF.setState({ isLoading: false });
            // TimeOutError(error,()=>SELF.getDealsData());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}
// const styles1 = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   tabBar: {
//     flexDirection: 'row',
//     paddingTop: 10,
//   },
//   tabItem: {
//     flex: 1,
//     alignItems: 'center',
//     padding: 16,
//   },

//   headerText: {
//     color: 'red'
//   },
//   headerTextInactive: {
//     color: 'black'
//   },
//   tabBarText: {

//     color: 'red',

//   },
//   tabBarScrollView: {

//     alignItems: 'center',
//     padding: 0,

//   },
// });


export default Deals;






