import React, { Component } from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  Platform,
  NetInfo,
  BackHandler,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import styles from './styles';
import Books from './Books.js';
import Loading from '../Loading/';
import axios from 'axios';
import CoinCombinationDialog from './CoinCombinationDialog.js';
import { InternetCheck, TimeOutError } from '../common/';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SafeAreaView from 'react-native-safe-area-view';
import { NativeBaseProvider, Box, Center } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DynamicTabViewTreasures from '../DynamicTabViewTreasures';


let categoryId = [],
  categoryName = [];
class Treasure extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      deals: [],
      dealCategory: [],
      noDataText: false,
      coin_combination: '',
      renderCoinCombination: false,
      index: 0,
      defaultIndex: 0,

    };
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
    //**************** api call to fetch deals ************************/
    this.getdealCategories();
    this.getDealsData();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }

  onRefresh = data => {
    this.getdealCategories();
    this.getDealsData();
  };

  // renderCategories() {
  //   let noDataText;
  //   // if(this.state.dealCategory && this.state.dealCategory.length)
  //   // {
  //   //  this.state.dealCategory((cat) => {
  //   //  categoryId.push(deal.id);
  //   //  categoryName.push(deal.category_name);
  //   //  })
  //   // }
  //   // console.log(this.state.dealCategory)
  //   return (
  //     this.state.dealCategory &&
  //     this.state.dealCategory != 'undefined' &&
  //     this.state.dealCategory.map((cat, i) => {
  //       let dealList = [];
  //       this.state.deals.map((singleDeal, j) => {
  //         if (cat.id == singleDeal.treasure_category) {
  //           dealList.push({ ...singleDeal, itemIndex: j });
  //         }
  //       });
  //       if (dealList.length > 0) {
  //         noDataText = false;
  //       } else {
  //         noDataText = true;
  //       }
  //       return (
  //         <Tab
  //           key={i}
  //           heading={cat.treasure_category_name.toUpperCase()}
  //           tabStyle={styles.blueColor}
  //           textStyle={styles.whiteColor}
  //           activeTabStyle={styles.blueColor}
  //           activeTextStyle={styles.tabBarText}>
  //           <Books
  //             navigation={this.props.navigation}
  //             list={dealList}
  //             treasure={true}
  //             noDataText={noDataText}
  //             startLoader={() => this.startLoader()}
  //             stopLoader={() => this.stopLoader()}
  //             onCardCoinCombinationClick={combination => { }}
  //             screen={this.props.navigation.state.params.screen}
  //             onRefresh={this.onRefresh}
  //             navigateToHistory={() => {
  //               this.props.navigation.navigate('DealHistory', {
  //                 onSelect: null,
  //               });
  //             }}
  //           />
  //         </Tab>
  //       );
  //     })
  //   );
  // }

  render() {
    return (
      <SafeAreaProvider style={{ flex: 1 }}>
        {/* <Container style={styles.container}> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View style={styles.headerStyle}>
            <TouchableOpacity
              onPress={() => this.goBack()}
              style={styles.goBackButton}>
              {/* <Icon
                  as={<MaterialCommunityIcons name="arrow-left" />}
                  style={[styles.arrowColor, {textAlign: 'center'}]}
                /> */}

              <MaterialCommunityIcons name="arrow-left" size={24} color='white' />

            </TouchableOpacity>
            <View transparent style={styles.headerText}>
              <Text style={{ color: 'white', textAlign: 'center' }}>
                Treasures
              </Text>
            </View>
          </View>
          {/* <View>
          <Header style={styles.header} hasTabs>             */}
          {/* <Heading /> */}

          {/* <Tabs
            tabBarBackgroundColor="white"
            tabBarUnderlineStyle={styles.tabBarLine}
            renderTabBar={() => <ScrollableTab />}>
            <Tab
              heading="All Treasures"
              tabStyle={styles.blueColor}
              textStyle={styles.whiteColor}
              activeTabStyle={styles.blueColor}
              activeTextStyle={styles.tabBarText}>
              <Books
                navigation={this.props.navigation}
                list={this.state.deals}
                treasure={true}
                noDataText={this.state.noDataText}
                startLoader={() => this.startLoader()}
                stopLoader={() => this.stopLoader()}
                onCardCoinCombinationClick={combination =>
                  this.onCardCoinCombinationClick(combination)
                }
                screen={this.props.navigation.state.params.screen}
                onRefresh={this.onRefresh}
                navigateToHistory={() =>
                  this.props.navigation.navigate('DealHistory', {
                    onSelect: null,
                  })
                }
              />
            </Tab>
            {this.renderCategories()}
          </Tabs> */}
          {/* </Header>
          </View> */}
          {/* {this.state.renderCoinCombination && this.renderCoinCombinationDialog()} */}

          {/* <Text style={{ color: 'red', textAlign: 'center' }}>
            Treasures {this.state.dealCategory.length}
          </Text> */}


          {/* //   let noDataText; */}
          {/* //       if (dealList.length > 0) { */}
          {/* //         noDataText = false; */}
          {/* //       } else { */}
          {/* //         noDataText = true;
  //       } */}

          {this.state.deals.length < 1 && (
            <Text
              style={{
                textAlign: 'center',
                alignSelf: 'center',
                margin: 10,
                position: 'absolute',
                top: 65,
              }}>
              {' '}
              No record found.
            </Text>
          )}



          {this.state.dealCategory != 'undefined' &&
            this.state.dealCategory.length > 0 && this.state.deals.length > 0 &&
            <DynamicTabViewTreasures navigation={this.props.navigation} categories={this.state.dealCategory} deals_data={this.state.deals} />
            //   <Text style={{ color: 'red', textAlign: 'center' }}>
            //   Treasuressssss {this.state.dealCategory.length}
            // </Text>

          }
          <Loading isLoading={this.state.isLoading} />
          {/* </Container> */}
        </Box>
      </SafeAreaProvider>
    );
  }
  // onCardCoinCombinationClick(combination) {
  //   this.setState({
  //     coin_combination: combination,
  //     renderCoinCombination: true,
  //   });
  // }
  // renderCoinCombinationDialog() {
  //   return (
  //     <CoinCombinationDialog
  //       details={this.state.coin_combination}
  //       method={() => this.toggleCoinCombinationDialog()}
  //     />
  //   );
  // }
  // toggleCoinCombinationDialog() {
  //   this.setState({ renderCoinCombination: false });
  // }
  // startLoader() {
  //   this.setState({
  //     isLoading: true,
  //   });
  // }
  // stopLoader() {
  //   this.setState({
  //     isLoading: false,
  //   });
  // }

  getdealCategories() {
    const SELF = this;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        //  console.log(value);
        if (value) {
          value = value;
        } else {
          value = 'raott-app-deals-public-user';
        }
        axios({
          url: 'https://raott.com/api/v2/TreasureCategories/',
          type: 'GET',
          method: 'GET',
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {


            // let dealCategoryList = [];
            // Object.keys(response.data.result).map(key => {
            //   dealCategoryList.push(response.data.result[key]);
            // });

            // SELF.setState({ dealCategory: dealCategoryList });
            // console.log('-----getdealCategories- sate set------------');

            // console.log('responseCategoryData');
            //  console.log(response.data.result);
            // alert(JSON.stringify(response.data.treasure_categories));
            let dealCategoryList = [];
            // if (response.data.status === 'success') {
            // response.data.treasure_categories.map(data => {
            Object.keys(response.data.treasure_categories).map(key => {
              // dealCategoryList.push(data);
              dealCategoryList.push(response.data.treasure_categories[key]);

            });

            // alert(JSON.stringify(dealCategoryList));
            SELF.setState({ dealCategory: dealCategoryList });
            // }

            SELF.setState({
              isLoading: false,
            });

          })
          .catch(function (error) {
            // alert(JSON.stringify(error));
            // console.log(error);
          });
      })
      .done(); // ending statement of asyncstorage
  }

  getDealsData() {
    const SELF = this;
    if (!SELF.state.isLoading) {
      SELF.setState({ isLoading: true });
    }
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);
        if (value) {
          value = value;
        } else {
          value = 'raott-app-deals-public-user';
        }
        axios({
          url: 'https://raott.com/api/v2/Treasures/',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(response => {

            console.log('responseTara');
            console.log('update: ' + JSON.stringify(response));


            // let dealList = [];
            // Object.keys(response.data.deals).map(key => {
            //   dealList.push(response.data.deals[key]);
            // });
            // if (dealList.length > 0) {
            //   SELF.setState({
            //     isLoading: false,
            //     deals: dealList,
            //   });
            // } else {
            //   SELF.setState({
            //     isLoading: false,
            //     noDataText: true,
            //   });
            // }


            let dealList = [];
            // if (response.data.status === 'success') {
            // response.data.treasures_list.map(data => {
            Object.keys(response.data.treasures_list).map(key => {
              // dealList.push(data);
              dealList.push(response.data.treasures_list[key]);
            });
            // }

            if (dealList.length > 0) {
              this.setState({
                isLoading: false,
                deals: dealList,
                noDataText: false,
              });
            } else {
              this.setState({
                isLoading: false,
                noDataText: true,
              });
            }
          })
          .catch(error => {
            // console.log(error);
            SELF.setState({ isLoading: false });
            // TimeOutError(error,()=>SELF.getDealsData());
          });
      })
      .done(); // ending statement of asyncstorage
  }
}

export default Treasure;
