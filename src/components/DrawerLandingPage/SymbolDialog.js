import React from 'react';
import { View, ActivityIndicator, Dimensions, Text, Button, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


const SymbolDialog = (props, children) => {






  return (
    <View style={styles.containerDialog}>


      <View style={{ width: deviceWidth - 40, backgroundColor: '#f7f7f7' }}>
        <View style={{
          width: deviceWidth - 40, backgroundColor: '#e02d2e', flexDirection: 'row',

          justifyContent: 'space-between', height: 50, alignItems: 'center'
        }}>
          <Text style={{ margin: 10, color: 'white', textAlign: 'center' }} > {props.titleText}</Text>

          <TouchableOpacity onPress={() => props.method('Cancel')}>
            <Image style={{ marginRight: 10, width: 20, height: 20, marginTop: 0, justifyContent: 'space-between' }} source={require('../../../img/closeicon.png')}>


            </Image>

          </TouchableOpacity>
        </View>

        <Text style={{ margin: 10, color: 'red', fontSize: 13 }}>{props.msg}</Text>
        <View style={{ flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
          <TouchableOpacity onPress={() => props.method('Cancel')}>
            <Text style={{ margin: 10, color: 'black', fontSize: 10, textAlign: 'right' }}> Cancel </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => props.method('OK')}>
            <Text style={{ margin: 10, color: 'black', fontSize: 10, textAlign: 'right' }}> OK </Text>
          </TouchableOpacity>
        </View>
      </View>

    </View>
  );
};

SymbolDialog.propTypes = {
  size: PropTypes.string,
};

SymbolDialog.defaultProps = {
  size: 'large',
};


export default SymbolDialog;


