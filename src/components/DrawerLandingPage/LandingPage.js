import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  BackHandler,
  Animated,
  Easing,
  Vibration,
  Pressable,
  PermissionsAndroid,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import DialogBox from '../dialogBox/';
import DialogRandomTreasure from '../dialogRandomTreasure/';
import Dialog from '../dialog/';
// import FriendlyReminder from '../dialogBox/FriendlyReminder.js'
import RoattCoins from '../RaottCollections/RoattCoins';
import AccountsDetail from '../ViewTransaction/AccountsDetail';
import styles from './styles';
import DrawerHeader from '../common/drawerHeader/drawerHeader';
import FriendRequest from '../common/drawerHeader/FriendRequest';
import Deals from '../TreasureChest/Deals.js';
import TransferCoinToAny from '../TransferCoinToAny/';
import FriendsList from '../TransferCoinToFriends/FriendsList.js';
import Category from '../TreasureHunt/Category.js';
// import MapUI from '../TreasureHunt/MapUI.js'
import DealHistory from '../DealHistory/DealHistory.js';
import axios from 'axios';
import Loading from '../Loading/';
import Moment from 'moment';
import SymbolDialog from './SymbolDialog';
import { setUser } from '../../actions/user';
import numeral from 'numeral';
import { InternetCheck, TimeOutError } from '../common/';
import _ from 'lodash';
/*import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType,
} from 'react-native-fcm';*/
import { registerAppListener } from '../../Listeners';
//import firebase from 'react-native-firebase';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';
import Modal from 'react-native-modal';
import AnimatedEllipsis from './AnimatedEllipsis';
import { getTrackingStatus } from 'react-native-tracking-transparency';
import RenderHtml from 'react-native-render-html';

let rotateValueHolder = new Animated.Value(0);

// const RotateData = rotateValueHolder.interpolate({
//   inputRange: [0, 1],
//   outputRange: ['0deg', '360deg'],
// });

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
import { RNCamera } from 'react-native-camera';

class LandingPage extends Component {
  // eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      fullName: ' ',
      homeBalance: ' ',
      raottsBalance: '',
      profileImage: '',
      isLoading: true,
      firstTimeUser: '',
      selectedCoin: null,
      isVisible: false,
      isVisibleInvite: true,
      isVisibleCongratulations: false,
      membership_status: '',
      showCongratulationsDialog: false,
      symbolConfirmationDialog: false,
      homeCurrency: '',
      timeoutDone: false,
      loadingCompass: true,
      treasure_id: 0,
      treasure_details: [],
      treasurePopup: false,
      isSearchGeoMint: false,
      isShowCameraModel: false,
      waiting: ""
    };

    console.log('initial');
    InternetCheck();
    this.getData();
    this.getRandomTreasure();

    this.categoryClick = _.debounce(this.categoryClick.bind(this), 1000, {
      leading: true,
      trailing: false,
    });
    this.yourTreasureCollectionClick = _.debounce(
      this.yourTreasureCollectionClick.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this.treasureChestClick = _.debounce(
      this.treasureChestClick.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this.transferFriendsClick = _.debounce(
      this.transferFriendsClick.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this.transferAnyoneClick = _.debounce(
      this.transferAnyoneClick.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this.raottCollectionClick = _.debounce(
      this.raottCollectionClick.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this.viewTransactionClick = _.debounce(
      this.viewTransactionClick.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this.dealHistoryClick = _.debounce(this.dealHistoryClick.bind(this), 1000, {
      leading: true,
      trailing: false,
    });
    this.homeCurrencyClick = _.debounce(
      this.homeCurrencyClick.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this._backAndroidPress = this.backAndroidPress.bind(this);
  }

  clickOnCoin = () => {
    this.collectCoin();

    // console.log(this.state.membership_status, 'membership_status')
    // if (this.state.coinOnceClicked === false) {
    //   if (
    //     this.state.membership_status === '0' ||
    //     this.state.membership_status === 'null'
    //   ) {
    //     this.setState({
    //       showClickCoinDialog: true,
    //       coinOnceClicked: true,
    //       checking: false,
    //     });
    //   } else if (this.state.membership_status === '1') {
    //     this.collectCoin();
    //   }
    // }
  };
  backAndroidPress() {
    BackHandler.exitApp();
    return true;
  }

  startImageRotateFunction() {
    rotateValueHolder.setValue(0);
    Animated.timing(rotateValueHolder, {
      toValue: 1,
      duration: 10000,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start(() => this.startImageRotateFunction());
  }

  async componentDidMount() {
    let trackingStatus = await getTrackingStatus();

    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable' || trackingStatus === '' || !trackingStatus) {
      this.startImageRotateFunction();

      // this.myInterval = setInterval(() => {
      //   this.setState({ loadingCompass: true });

      //   setTimeout(() => {
      //     this.setState({ loadingCompass: false });
      //   }, 15000);
      // }, 60000);

      // setTimeout(() => {
      //   this.setState({ loadingCompass: false });
      // }, 15000);
    }

    // FCM.on(FCMEvent.Notification, notif => {
    //   if (notif.fcm && notif.fcm.body) {
    //     /* Create local notification for showing in a foreground */
    //     FCM.presentLocalNotification({
    //       body: notif.fcm.body,
    //       priority: 'high',
    //       title: notif.fcm.title,
    //       sound: 'default',
    //       badge: 0,
    //       show_in_foreground: true /* notification when app is in foreground (local & remote)*/,
    //       vibrate: 300 /* Android only default: 300, no vibration if you pass null*/,
    //       large_icon: 'ic_launcher', // Android only
    //       icon: 'ic_launcher', // as FCM payload, you can relace this with custom icon you put in mipmap
    //       color: '#e02d2e',
    //       status: notif.status,
    //     });
    //   }
    // });
    // FCM.setBadgeNumber(0); // iOS and supporting android.
    // FCM.getBadgeNumber().then(number => console.log(number));

    //started
    // Build a channel
    //   const channel = new firebase.notifications.Android.Channel(
    //     'test-channel',
    //     'Test Channel',
    //     firebase.notifications.Android.Importance.Max).setDescription('My apps test channel');

    //   // Create the channel
    //   firebase.notifications().android.createChannel(channel);

    //   //registerAppListener();
    //   this.notificationListener = firebase
    //   .notifications()
    //   .onNotification(notification => {
    //     firebase.notifications().displayNotification(notification);
    //   });

    // this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(token => {
    //   console.log('TOKEN (refreshUnsubscribe)', token);
    // });

    // this.messageListener = firebase.messaging().onMessage(message => {
    //   this.displayNotificationFromCustomData(message);
    // });

    //   firebase.notifications().getInitialNotification();
    //   this.checkPermission;

    //   firebase
    //     .messaging()
    //     .getToken()
    //     .then(token => {
    //       console.log('TOKEN (getFCMToken)', token);
    //       // this.storingdeviceId(token);
    //     });
    //end

    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
    //this.notificationListener();
    //this.onTokenRefreshListener();
    //this.messageListener();
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  // componentDidMount() {
  //   setInterval( () => {
  //     this.forceUpdate();
  //   }, 1000);

  // }

  openDrawer() {
    this.props.navigation.openDrawer();
  }

  toggleModal(e) {
    let id = 0;
    try {
      AsyncStorage.setItem('selectedCoin', e);
    } catch (error) {
      // Error saving data
    }
    this.setState({ isVisible: !this.state.isVisible });
    e = e.toString().toLowerCase();

    if (e == 'cup') {
      id = 2;
    } else if (e == 'crown') {
      id = 1;
    } else if (e == 'ring') {
      id = 3;
    } else {
      id = null;
    }
    this.selectUserCoin(id, e);
  }

  categoryClick() {
    this.props.navigation.navigate('Category', {
      screen: 'LandingPage',
      onSelect: this.onSelect,
    });
  }

  homeCurrencyClick() {
    this.props.navigation.navigate('HomeCurrency', { screen: 'HomeCurrency' });
  }

  treasureChestClick() {
    this.props.navigation.navigate('Deals', {
      screen: 'LandingPage',
      onSelect: this.onSelect,
    });
  }

  yourTreasureCollectionClick() {
    this.props.navigation.navigate('Treasure', {
      screen: 'LandingPage',
      onSelect: this.onSelect,
    });
  }

  transferFriendsClick() {
    this.props.navigation.navigate('FriendsList', { onSelect: this.onSelect });
  }

  transferAnyoneClick() {
    this.props.navigation.navigate('TransferCoinToAny', {
      onSelect: this.onSelect,
      navigationFrom: 'LandingPage',
    });
  }

  raottCollectionClick() {
    this.props.navigation.navigate('RoattCoins', {
      onSelect: this.onSelect,
      headerTitle: 'World Raott Coins',
      screen: 'RaottCoins',
    });
  }

  viewTransactionClick() {
    this.props.navigation.navigate('AccountsDetail', {
      onSelect: this.onSelect,
    });
  }

  dealHistoryClick() {
    this.props.navigation.navigate('DealHistory', { onSelect: this.onSelect });
  }

  toggleModalInvite() {
    this.setState({
      isVisibleInvite: !this.state.isVisibleInvite,
    });
  }

  toggleModalRandomTreasure() {
    this.setState({
      // isVisibleInvite: !this.state.isVisibleInvite,
    });
  }

  getTimeDifference(latestDate, prevDate) {
    if (!latestDate || !prevDate) {
      return '';
    }
    if (latestDate.getTime() < prevDate.getTime()) {
      return '';
    }
    const duration = Moment.duration(Moment(latestDate).diff(Moment(prevDate)));
    const days = Math.floor(duration.asDays());
    let hours = Math.floor(duration.asHours());
    let minutes = Math.floor(duration.asMinutes());
    let seconds = Math.floor(duration.asSeconds());
    hours -= days * 24;
    minutes -= (days * 24 + hours) * 60;
    seconds -= ((days * 24 + hours) * 60 + minutes) * 60;
    let str = '';
    if (days > 0) {
      str += days + (days > 1 ? ' days ' : ' day ');
    }
    if (hours > 0 || days > 0) {
      str += hours + (hours > 1 ? ' hours ' : ' hour ');
    }
    if (minutes > 0 || hours > 0 || days > 0) {
      str += minutes + (minutes > 1 ? ' minutes ' : ' minute ');
    }
    if (seconds > 0 || minutes > 0 || hours > 0 || days > 0) {
      str += seconds + (seconds > 1 ? ' seconds ' : ' second ');
    }
    return str;
  }

  renderCongratulationsDialog() {
    return (
      <Dialog
        method={() => this.toggleModalCongratulations()}
        titleText="Congratulations!"
        msg={
          'Congrats! You are now a full member of Raotts. You can start buying treasure using World Raott Coins and transfer World Coins to anyone'
        }
      />
    );
  }

  toggleModalCongratulations() {
    this.setState({
      isVisibleCongratulations: !this.state.isVisibleCongratulations,
    });
  }

  renderSymbolConfirmationDialog() {
    return (
      <SymbolDialog
        method={(...a) => this.toggleSymbolConfirmation(...a)}
        titleText=""
        msg={`Are you sure you want to select ${this.state.selectedCoin
          } Symbol ?`}
      />
    );
  }

  toggleSymbolConfirmation(choose) {
    if (choose == 'OK') {
      this.setState({
        symbolConfirmationDialog: !this.state.symbolConfirmationDialog,
      });
    } else {
      this.setState({
        symbolConfirmationDialog: !this.state.symbolConfirmationDialog,
        selectedCoin: null,
        isVisible: true,
      });
    }
  }
  onSelect = data => {
    this.getData();
  };

  onClickFriendRequest() {
    this.props.navigation.navigate('FriendRequest');
  }

  localToUtc(date, format) {
    if (date && format) {
      const dateMoment = Moment(date, format);
      if (!dateMoment) {
        return '';
      }
      const dateUtc = dateMoment.utc().format(format);
      return dateUtc;
    }
    return date;
  }

  utcToLocal(date, format) {
    if (date && format) {
      const dateMoment = Moment.utc(date, format);
      if (!dateMoment) {
        return '';
      }
      const dateLocal = dateMoment.local().format(format);
      return dateLocal;
    }
    return date;
  }

  searchTreasure() {

  }
  render() {
    // console.log(this.props, 'this.props');

    // console.log(this.localToUtc(this.state.reminder_date,'YYYY-MM-DD HH:mm:ss'),"((((((((((((((((&&&&&&&&&&&&");
    let remiderUtc = this.localToUtc(
      this.state.reminder_date,
      'YYYY-MM-DD HH:mm:ss',
    );
    let reminder = Moment(remiderUtc, 'YYYY-MM-DD HH:mm:ss');
    let diffInStr = this.getTimeDifference(reminder.toDate(), new Date());
    // console.log(reminder.toDate(),'TIME++++++',new Date(),'******',diffInStr)
    //     console.log(this.state.homeCurrency, ' homeCurrency')
    //     console.log(this.state.symbolConfirmationDialog, 'symbolConfirmationDialog')
    //     console.log(this.state.isVisibleCongratulations, 'isVisibleCongratulations')
    return (
      <SafeAreaProvider width={deviceWidth}>
        {/* <SafeAreaView style={{flex: 1}}> */}
        {/* <Container style={styles.container}> */}
        <Box display='flex' flexDirection='column' backgroundColor='#e7e4e5'>
          <DrawerHeader
            openDrawer={() => this.openDrawer()}
            navigation={this.props.navigation}
            username={this.state.fullName}
            pImage={this.state.profileImage}
            onClickFriendRequest={() => this.onClickFriendRequest()}
          />

          <View>
            {this.state.loadingCompass && (
              <View style={styles.searchContainer}>
                <Animated.Image
                  style={styles.compass}
                  source={require('../../../img/compass.gif')}
                />
                {/* <Image
                  style={[styles.compass, {transform: [{rotate: RotateData}]}]}
                  source={require('../../../img/compass.png')}
                /> */}
                <Text>Searching for hidden treasures nearby</Text>

                <AnimatedEllipsis style={{ color: '#000000', fontSize: 18 }} />
              </View>
            )}
            <View style={styles.viewStyle}>
              <TouchableOpacity onPress={this.homeCurrencyClick}>
                <View
                  style={{
                    width: deviceWidth / 2,
                    alignItems: 'center',
                    marginTop: 10,
                    height: 40,
                  }}>
                  <Text style={{ color: '#e02d2e', fontSize: 14 }}>
                    {' '}
                    WORLD RAOTT{' '}
                  </Text>
                  <Text numberOfLines={1} style={{ fontSize: 12 }}>
                    {' '}
                    R {numeral(this.state.raottsBalance).format('0,0')}{' '}
                  </Text>
                </View>
              </TouchableOpacity>

              <View
                style={{
                  width: 1,
                  height: 30,
                  marginTop: 10,
                  backgroundColor: '#e02d2e',
                }}
              />

              <TouchableOpacity onPress={this.homeCurrencyClick}>
                <View
                  style={{
                    width: deviceWidth / 2,
                    alignItems: 'center',
                    marginTop: 10,
                    height: 40,
                    marginLeft: 10,
                  }}>
                  <Text style={{ color: '#e02d2e', fontSize: 14 }}>
                    {' '}
                    HOME CURRENCY
                  </Text>
                  <Text numberOfLines={1} style={{ fontSize: 12 }}>
                    {' '}
                    {numeral(this.state.homeBalance).format('0,0.00')}{' '}
                    {this.state.homeCurrency}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <ScrollView style={{ height: deviceHeight * 0.55, width: deviceWidth }}>

            <View style={{ alignItems: 'center', marginBottom: 15 }}>
              <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity onPress={this.categoryClick}>
                  <View style={[styles.tabStyle, { justifyContent: 'center' }]}>
                    <Image
                      // style={{ width: 80, height: 80, marginTop: -10 }}
                      // style={{height: deviceHeight * 0.45, width: deviceWidth}}

                      style={[
                        styles.imageStyle,
                        {
                          alignSelf: 'center',
                          marginTop: 0,
                          width: 60,
                          height: 60,
                        },
                      ]}
                      source={require('../../../img/treasureHunt.png')}
                    />
                    <Text style={styles.textStyle}>Treasure Hunt</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ marginLeft: 20 }}
                  onPress={this.treasureChestClick}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/treasurechest.png')}
                    />
                    <Text style={styles.textStyle}>Treasure Chest</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={{ flexDirection: 'row', marginTop: 15 }}>
                {/* <TouchableOpacity onPress={this.transferFriendsClick}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/potcoin.png')}
                    />
                    <Text style={styles.textStyle}>
                      Transfer World Raott to friends
                    </Text>
                  </View>
                </TouchableOpacity> */}
                <TouchableOpacity
                  onPress={this.yourTreasureCollectionClick}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/potcoin.png')}
                    />
                    <Text style={styles.textStyle}>
                      Your Treasure Collections
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginLeft: 20 }}
                  onPress={() => this.setState({ isSearchGeoMint: !this.state.isSearchGeoMint })}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/compass2.png')}
                    />
                    <Text style={styles.textStyle}>Search and GeoMint</Text>
                  </View>
                </TouchableOpacity>
                <Modal
                  animationType="slide"
                  transparent={true}
                  animationInTiming={1000}
                  animationOutTiming={1000}
                  backdropTransitionInTiming={800}
                  backdropTransitionOutTiming={800}
                  visible={this.state.isSearchGeoMint}>
                  <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                      <Text style={styles.modalText}>Warning – Please be aware of your surroundings. Look for treasures only if it is safe to do so. Minors must only hunt with adult supervision. Explore and find new treasures. Stay safe.</Text>
                      <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => this.openCamera()}
                      >
                        <Text style={styles.modalTextStyle}>I agree</Text>
                      </Pressable>
                    </View>
                  </View>
                </Modal>
                <Modal
                  animationType="slide"
                  transparent={true}
                  animationInTiming={1000}
                  animationOutTiming={1000}
                  backdropTransitionInTiming={800}
                  backdropTransitionOutTiming={800}
                  visible={this.state.isShowCameraModel}>
                  <RNCamera
                    ref={ref => {
                      this.camera = ref;
                    }}
                    captureAudio={false}
                    style={{ flex: 1, marginTop: -20, height: deviceHeight + 20, width: deviceWidth + 1, marginLeft: -19 }}
                    type={RNCamera.Constants.Type.back}
                    androidCameraPermissionOptions={{
                      title: 'Permission to use camera',
                      message: 'We need your permission to use your camera',
                      buttonPositive: 'Ok',
                      buttonNegative: 'Cancel',
                    }} >

                    <View>
                      {this.state.treasurePopup ? (
                        <View style={[styles.centeredView, { position: 'absolute', maxWidth: 200, marginLeft: deviceWidth / 2 - 100, backgroundColor: 'transparent' }]}>
                          {
                            this.state.waiting ? (
                              <View style={[styles.modalView3, { maxHeight: 300 }]}>
                                <Text style={[styles.center2, styles.centerRed, { fontSize: 16, padding: 13 }]}>
                                  You are very close to a hidden treasure!
                                </Text>
                              </View>
                            ) :
                              (<View style={[styles.modalView3, { maxHeight: 350 }]}>
                                <Text style={[styles.headerTextStyle2, { fontSize: 14 }]}>
                                  Treasure GeoMinted in this location!
                                </Text>
                                <View
                                  style={{
                                    alignItems: 'center',
                                  }}>
                                  <Image
                                    style={styles.imagestyle2}
                                    source={{
                                      uri:
                                        'https://raott.com/raottdev/assets/treasure_images/' +
                                        this.state.treasure_details.treasere_media,
                                      // 'https://raott.com/raottdev/assets/treasure_images/202105182332140_100.png'
                                    }}
                                  />
                                </View>
                                <Text style={[styles.center2, styles.centerRed, { fontSize: 17 }]}>
                                  {this.state.treasure_details.treasure_title || 'Treasure Name'}
                                </Text>
                                <Text
                                  style={{
                                    fontSize: 12,
                                    color: '#000000',
                                    textAlign: 'center',
                                  }}>
                                  Treasure Value: R{' '}
                                  {numeral(this.state.treasure_details.treasure_amount).format(
                                    '0,0',
                                  )}{' '}
                                  ({this.state.treasure_details.home_currency})
                                </Text>
                                <TouchableOpacity
                                  style={[styles.button2, styles.buttonClose2]}
                                  onPress={() => this.collectTreasureButtonPress()}>
                                  <Text style={styles.textStyle2}>Collect</Text>
                                </TouchableOpacity>
                              </View>)
                          }

                        </View>
                      ) : (
                        <View></View>
                      )}
                      <View>
                        {this.state.loadingCompass && (
                          <View style={[styles.searchContainer, { marginLeft: -30, marginTop: 0, backgroundColor: '#fff', paddingTop: 46 }]}>
                            <Animated.Image
                              style={styles.compass}
                              source={require('../../../img/compass.gif')}
                            />
                            <Text style={{ color: '#000000', fontSize: 14, marginLeft: - 5, fontWeight: 'bold' }}>Searching for hidden treasures nearby</Text>

                            <AnimatedEllipsis style={{ color: '#000000', fontSize: 18 }} />
                          </View>
                        )}
                        <Pressable style={[{ position: 'absolute', top: 65, right: 0 }]} onPress={() => this.setState({ isShowCameraModel: !this.state.isShowCameraModel })}>
                          <Image
                            style={styles.imageStyle}
                            source={require('../../../img/closeButton.png')}
                          />
                        </Pressable>
                      </View>

                    </View>



                  </RNCamera>
                </Modal>

                {/* <TouchableOpacity
                  onPress={this.transferAnyoneClick}
                  style={{ marginLeft: 20 }}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/potcoin.png')}
                    />
                    <Text style={styles.textStyle}>
                      Transfer World Raott to anyone{' '}
                    </Text>
                  </View>
                </TouchableOpacity> */}
              </View>

              <View style={{ flexDirection: 'row', marginTop: 15 }}>
                {/* <TouchableOpacity onPress={this.raottCollectionClick}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/raottcollection.png')}
                    />
                    <Text style={styles.textStyle}>
                      View all your Raott Collections
                    </Text>
                  </View>
                </TouchableOpacity> */}

                {/* <TouchableOpacity
                  onPress={this.viewTransactionClick}
                  style={{ marginLeft: 20 }}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/viewtransaction.png')}
                    />
                    <Text style={styles.textStyle}>View Transactions</Text>
                  </View>
                </TouchableOpacity> */}
              </View>

              <View style={{ flexDirection: 'row', marginTop: 15 }}>
                {/* <TouchableOpacity onPress={this.dealHistoryClick}>
                  <View style={styles.tabStyle}>
                    <Image
                      style={styles.imageStyle}
                      source={require('../../../img/dealhistory.png')}
                    />
                    <Text style={styles.textStyle}>Deal History</Text>
                  </View>
                </TouchableOpacity> */}


              </View>
            </View>
          </ScrollView>

          {this.state.selectedCoin !== null &&
            this.state.isVisibleInvite &&
            this.state.membership_status == '0' &&
            this.state.reminder_date !== '' &&
            diffInStr !== '' && (
              <Dialog
                method={() => this.toggleModalInvite()}
                titleText="A friendly reminder"
                msg={`You are still a guest user of Raotts. Only Full members can send and receive World Raott Coins and buy deals using World Raott coins. You have to invite at least one friend and he/she must join Raotts using your invite code for you to become a full member. You have remaining time ${diffInStr}`}

              />
            )}

          {this.state.selectedCoin === null ||
            (this.state.selectedCoin.toString().length === 0 &&
              this.state.isVisible && (
                <DialogBox
                  method={(...a) => this.toggleModal(...a)}
                  titleText="Select one symbol"
                  msg="The World Raott Coins were minted in 3 powerful symbols . The Crown, the Cup and the Ring."
                  msg1="Please decide carefully and choose one symbol to start earning and collecting World Raott Coins."
                  msg2="Remember, you can only win deals and cash if you get the right World Raott Coin combination of your chosen symbol. Also note that once you have selected a symbol you cannot change it."
                  msg3="Please click below and choose one symbol to start your earnings and collection."
                />
              ))}
          {this.state.isVisibleCongratulations &&
            this.renderCongratulationsDialog()}
          {this.state.symbolConfirmationDialog &&
            this.renderSymbolConfirmationDialog()}

          {this.state.treasurePopup && this.showAlert()}
          {/* {this.state.treasurePopup && this.renderTreasurePopup()} */}
          {<Loading isLoading={this.state.isLoading} />}
          {/* </Container> */}
        </Box>
        {/* </SafeAreaView> */}

      </SafeAreaProvider>




    );
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.location,
        {
          title: 'Location Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log("You can use the camera")
      } else {
        // console.log("Camera permission denied")
      }
    } catch (err) {
      // console.warn(err)
    }
  }

  openCamera() {
    this.setState({ waiting: true })
    this.setState({ isSearchGeoMint: !this.state.isSearchGeoMint })
    this.setState({ isShowCameraModel: !this.state.isShowCameraModel })
    setTimeout(() => {
      this.setState({ waiting: false })
    }, 5000)

  }
  getData() {
    console.log('getData');
    const SELF = this;
    if (!this.state.isLoading) {
      SELF.setState({ isLoading: true, timeoutDone: false });
    }
    AsyncStorage.getItem('apiToken')

      .then(value => {

        axios.interceptors.request.use(request => {
          console.log('Starting Request', JSON.stringify(request, null, 2))
          return request
        })
        axios({

          // 'https://raott.com/api/v2/screenTransactions/' +
          //   SELF.state.count +
          //   '/5/' + value
          url: 'https://raott.com/api/v2/screenHome/' + value,
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {

            console.log('response - -- -' + JSON.stringify(response));

            if (response.data.status == 'success') {
              AsyncStorage.getItem('membership_status').then(value => {
                if (
                  value &&
                  value != response.data.userinfo.membership_status
                ) {
                  SELF.setState({ isVisibleCongratulations: true });
                }
              }).done;
              if (response.data.userinfo.membership_status == '0') {
                //   if(response.data.reminder_date !== null){
                try {
                  AsyncStorage.setItem('inviteCode', response.data.invite_code);
                  AsyncStorage.setItem(
                    'membership_status',
                    response.data.userinfo.membership_status,
                  );
                  AsyncStorage.setItem(
                    'reminder_date',
                    response.data.reminder_date,
                  );
                  AsyncStorage.setItem(
                    'fullName',
                    response.data.userinfo.fullname,
                  );
                } catch (error) {
                  // Error saving data
                }
              } else {
                try {
                  AsyncStorage.setItem(
                    'membership_status',
                    response.data.userinfo.membership_status,
                  );
                  AsyncStorage.setItem(
                    'fullName',
                    response.data.userinfo.fullname,
                  );
                } catch (error) {
                  // Error saving data 28232287
                }
              }
              if (
                response.data.selectedCoin &&
                response.data.selectedCoin !== ''
              ) {
                try {
                  let type = '';
                  if (response.data.selectedCoin == 2) {
                    type = 'Cup';
                  } else if (response.data.selectedCoin == 1) {
                    type = 'Crown';
                  } else if (response.data.selectedCoin == 3) {
                    type = 'Ring';
                  }
                  AsyncStorage.setItem('selectedCoin', type);
                } catch (error) {
                  // Error saving data
                }
              }
              let fullName = '';
              if (response.data.userinfo.fullname == '') {
                fullName = response.data.userinfo.username;
              } else {
                fullName = response.data.userinfo.fullname;
              }
              let visibility = '';
              if (response.data.selectedCoin) {
                visibilty = false;
              } else {
                visibilty = true;
              }
              SELF.props.setUser({
                membership_status: response.data.userinfo.membership_status,
                reminder_date: response.data.reminder_date,
                userid: response.data.userid,
              });
              SELF.setState({
                fullName: fullName,
                homeBalance: response.data.homeBalance,
                raottsBalance: response.data.raottsBalance,
                profileImage: response.data.userinfo.profile_image,
                selectedCoin: response.data.selectedCoin,
                membership_status: response.data.userinfo.membership_status,
                reminder_date: response.data.reminder_date,
                homeCurrency: response.data.homeCurrency,
                isVisible: visibilty,
                isLoading: false,
              });
            }
          })
          .catch(function (error) {
            // console.log(error , ' ', SELF.state.timeoutDone);
            if (!SELF.state.timeoutDone) {
              TimeOutError(error, () => SELF.getData());
            }
            SELF.setState({ isLoading: false, timeoutDone: true });
          });
      })
      .done(); // ending statement of asyncstorage
  }
  /*
      "lat": 37.785834,
      "lon": -122.406417, 
  
  */
  getRandomTreasure() {
    const SELF = this;
    if (this.count == 6) {
      return
    } else {
      AsyncStorage.getItem('apiToken')
        .then(value => {
          axios.interceptors.request.use(request => {
            console.log('Starting Request', JSON.stringify(request, null, 2))
            return request
          })

          axios({
            url: 'https://raott.com/api/V2/treasureon/' + value,
            type: 'GET',
            method: 'GET',
            headers: {
              RaottAuth: value,
            },
          })
            .then(function (response) {
              console.log('akbhairesponse - -- -' + JSON.stringify(response));
              SELF.setState({ isLoading: false });
              // console.log(mationD'response userCoin');
              // console.log(response);
              // if (response.data.status == '1') {
              // SELF.setState({  21Zkd$u21!yaB5 teamt@synapseindia.com
              //   symbolConfirialog: true,
              //   selectedCoin: e,
              // });
              // }

              if (response.data.status === 'success') {
                let popup = false;
                if (response.data.treasures_list[0].treasure_id > 0) {
                  popup = true;
                }
                console.log('bhutbdia', response.data.treasures_list);
                // alert("message" + JSON.stringify(response.data.treasures_list))
                console.log(response.data.treasures_list[0].treasure_id);
                console.log(response.data.treasures_list[0]);
                let data = response.data.treasures_list;
                SELF.setState({
                  treasure_id: data[0].treasure_id,
                  treasure_details: data[0],
                  treasurePopup: popup,
                });
                // this.showAlert(response.data.treasures_list);
              }
            })
            .catch(function (error) {
              // console.log(error);
              SELF.setState({ isLoading: false });
              // TimeOutError(error, () => SELF.selectUserCoin(idFetch, coinFetch));
            });
        })
        .done(); // ending statement of asyncstorage
    }
  }

  selectUserCoin(id, e) {
    const SELF = this;
    let idFetch = id;
    let coinFetch = e;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        // console.log(value);

        axios({
          url: 'https://raott.com/api/v2/userCoin/',
          type: 'POST',
          method: 'POST',
          timeout: 30000,
          data: {
            symbol: id,
          },
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {
            // console.log('response userCoin');
            // console.log(response);
            if (response.data.status == '1') {
              SELF.setState({
                symbolConfirmationDialog: true,
                selectedCoin: e,
              });
            }
          })
          .catch(function (error) {
            // console.log(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.selectUserCoin(idFetch, coinFetch));
          });
      })
      .done(); // ending statement of asyncstorage
  }
  count = 0;
  collectCoin() {
    const SELF = this;

    // SELF.setState({ collectAnyway: !this.state.collectAnyway });
    // console.log(SELF.props.navigation.state.params.userid, 'userid')
    // console.log(SELF.state.coinid, 'coinid')
    // console.log(SELF.state.huntcoinid, 'huntcoinid')
    AsyncStorage.getItem('apiToken')
    axios.interceptors.request.use(request => {
      console.log('Starting Request', JSON.stringify(request, null, 2))
      return request
    })

      .then(value => {
        axios({
          url: 'https://raott.com/api/v2/collectcoin/',
          type: 'POST',
          method: 'POST',
          timeout: 30000,
          data: {
            userid: SELF.props.navigation.state.params.userid,
            coinid: SELF.state.coinid,
            huntcoinid: SELF.state.huntcoinid,
          },
          headers: {
            RaottAuth: value,
          },
        })
          .then(function (response) {
            // let clueList = [];
            // console.log('collect coins')
            // console.log(response, 'response123');
            if (response.data.status == 'success') {
              SELF.setState({
                clickOnCoin: false,
                coinCollect: true,
                collectAnyway: false,
                coinOnceClicked: true,
                checking: false,
              });
            } else if (response.data.status.includes('already')) {
              SELF.setState({
                clickOnCoin: false,
                alreadyCollected: true,
                collectAnyway: false,
                coinOnceClicked: true,
              });
            }
          })
          .catch(function (error) {
            // console.log(error);
            SELF.setState({ isLoading: false });
            TimeOutError(error, () => SELF.collectCoin());
          });
      })
      .done(); // ending statement of asyncstorage
  }


  showAlert() {
    Vibration.vibrate(1, false);
    // Vibration.vibrate()
    //   this.setState({
    //     treasurePopup: true,
    //     treasure_details: details,
    //   });  
    // this.setState({ 
    //   treasurePopup: !this.state.treasurePopup,
    // });
  }

  renderTreasurePopup() {
    // Vibration.vibrate(1 * 1000);

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.treasurePopup}>
        <View style={styles.centeredView2}>
          <View style={styles.modalView3}>
            <Text style={styles.headerTextStyle2}>
              Treasure GeoMinted in this location!
            </Text>
            <View
              style={{
                alignItems: 'center',
              }}>
              <Image
                style={styles.imagestyle2}
                source={{
                  uri:
                    'https://raott.com/raottdev/assets/treasure_images/' +
                    this.state.treasure_details.treasere_media,
                  // 'https://raott.com/raottdev/assets/treasure_images/202105182332140_100.png'
                }}
              />
            </View>
            <Text style={[styles.center2, styles.centerRed]}>
              {this.state.treasure_details.treasure_title || 'Treasure Name'}
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: '#000000',
                textAlign: 'center',
              }}>
              Treasure Value: R{' '}
              {numeral(this.state.treasure_details.treasure_amount).format(
                '0,0',
              )}{' '}
              ({this.state.treasure_details.home_currency})
            </Text>
            {/* <Text numberOfLines={1} style={{ fontSize: 12 }}>
                    {' '}
                    {numeral(this.state.homeBalance).format('0,0.00')}{' '}
                    {this.state.homeCurrency}
                  </Text> */}
            <TouchableOpacity
              style={[styles.button2, styles.buttonClose2]}
              onPress={() => this.collectTreasureButtonPress()}>
              {/* onPress={() => this.setState({ treasurePopup: false })}> */}
              {/* <ButtonPress onPress={() => this.loginButtonPress()}> */}
              <Text style={styles.textStyle2}>Collect</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }


  collectTreasureButtonPress() {
    this.setState({
      treasurePopup: !this.state.treasurePopup,
    });
    // this.setState({
    //   isShowCameraModel: false
    // })
    this.count = this.count + 1;
    // alert(this.count)
    const SELF = this;
    this.setState({ treasurePopup: false })
    setTimeout(() => {
      this.getRandomTreasure();
      // this.setState({ treasurePopup: true })
    }, 3000);
    axios.interceptors.request.use(request => {
      console.log('Starting Request', JSON.stringify(request, null, 2))
      return request
    })
    axios({
      url: 'https://raott.com/api/V2/CollectTreasure',
      type: 'POST',
      method: 'POST',
      timeout: 30000,
      data: {
        userid: this.props.user.userid,//SELF.props.navigation.state.params.userid,
        treasureid: SELF.state.treasure_id,
      },
    })
      .then(function (response) {
        console.log(response.data);
        SELF.setState({ isLoading: false });

        if (response.data.status == 'success') {

          try {
            // AsyncStorage.setItem('apiToken', response.data.apitoken);
          } catch (error) {
            // Error saving data
          }
          // SELF.props.navigation.navigate('LandingPage');
          // SELF.props.navigation.push('LandingPage');
        } else {
          // if (response.data.error == 'Account not confirmed') {
          //   SELF.requestOtp(response.data.userid);
          //   SELF.setState({ isLoading: false });
          // } else if (response.data.error == 'Authentication failure!') {
          //   Alert.alert('Invalid Credential', response.data.error);
          //   SELF.setState({ uname: '', isLoading: false, pwd: '' });
          // } else {
          //   Alert.alert('Error', 'Username or Password is Invalid');
          //   SELF.setState({ uname: '', isLoading: false, pwd: '' });
          // }
        }
      })
      .catch(function (error) {
        SELF.setState({ isLoading: false });
        // TimeOutError(error, () => SELF.loginButtonPress());
      });

  }

  // renderTtreasurePopup3() {
  // return (
  // <Modal
  // animationType="slide"
  // isVisible={this.state.treasurePopup}
  // backdropOpacity={0.5}
  // transparent={true}
  // visible={this.state.treasurePopup}

  // swipeDirection="left"
  // onSwipe={this.closeModal}
  // onBackdropPress={this.closeModal}
  // >

  // {/* <Modal
  // animationType="slide"
  // transparent={true}
  // visible={this.state.treasurePopup}
  // > */}

  // <View style={styles.innerContainerDialogNew}>
  // <View style={styles.messageHeaderView}>
  // <Text style={styles.headerTextStyle}>Details of Treasure</Text>
  // <TouchableOpacity
  // onPress={() => this.setState({ treasurePopup: false })}>
  // <Image
  //       style={{
  //         marginRight: 10,
  //         width: 20,
  //         height: 20,
  //         marginTop: 0,
  //         justifyContent: 'space-between',
  //       }}
  //       source={require('../../../img/closeicon.png')}></Image>
  //   </TouchableOpacity>
  // </View>
  // <ScrollView>
  //   <View>
  //     <View
  //       style={{
  //         alignItems: 'center',
  //       }}>
  //       <Image
  //         style={{
  //           marginRight: 10,
  //           width: 20,
  //           height: 20,
  //           marginTop: 0,
  //           justifyContent: 'space-between',
  //         }}
  //         source={{
  //           uri:
  // 'https://raott.com/raottdev/assets/treasure_images/' +
  // this.state.treasure_details.treasure_image,
  // 'https://raott.com/raottdev/assets/treasure_images/202105182332140_100.png'
  // }}
  // ></Image>
  // {/* <Image
  // style={styles.imagestyle2}
  // source={{
  //   uri:
  // 'https://raott.com/raottdev/assets/treasure_images/' +
  // this.state.treasure_details.treasure_image,
  //         'https://raott.com/raottdev/assets/treasure_images/202105182332140_100.png'
  //     }}
  //   /> */}
  // </View>
  // <Text style={[styles.center, styles.centerRed]}>
  //   {this.state.treasure_details.treasure_title}
  // </Text>
  // <Text style={styles.center}>
  //   {this.state.treasure_details.text1} |{' '}
  //   {this.state.treasure_details.text2}
  // </Text>
  // <View style={styles.bgyellow}>
  // {/* <HTML html={this.state.treasure_details.treasure_desc} /> */}
  //               <RenderHtml
  //                 source={{ html: this.state.treasure_details.treasure_desc }}
  //               />
  //             </View>
  //             <Text
  //               style={{
  //                 fontSize: 16,
  //                 color: '#e02d2e',
  //                 textAlign: 'center',
  //                 marginTop: 10,
  //               }}>
  //               Treasure Value: R {this.state.treasure_details.treasure_amount}{' '}
  //               ({this.state.treasure_details.treasure_home_cur})
  //             </Text>
  //           </View>
  //         </ScrollView>
  //         <TouchableOpacity
  //           onPress={() => this.setState({ treasurePopup: false })}>
  //           <Text style={styles.okButtonTextStyle}> OK </Text>
  //         </TouchableOpacity>
  //       </View>
  //     </Modal>
  //   );
  // }

  // renderTtreasurePopup2() {
  //   return (

  //     <Modal
  //       animationType="slide"
  //       transparent={true}
  //       visible={this.state.treasurePopup}
  //     >

  //       <View style={styles.container}>
  //         <View style={{ width: deviceWidth - 40, backgroundColor: "#f7f7f7" }}>
  //           <View
  //             style={{
  //               width: deviceWidth - 40,
  //               backgroundColor: "#e02d2e",
  //               flexDirection: "row",

  //               justifyContent: "space-between",
  //               height: 50,
  //               alignItems: "center"
  //             }}
  //           >
  //             <Text
  //               style={{
  //                 margin: 10,
  //                 color: "white",
  //                 textAlign: "center",
  //                 fontSize: 15
  //               }}
  //             >
  //               {" "}
  //               {this.state.treasure_details.treasure_title || ''}
  //             </Text>
  //             <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
  // {/*<Image style={{marginRight:10,width:20,height:20,marginTop:0 ,justifyContent:'space-between'}} source={require('../../../img/closeicon.png')}>


  //  </Image>*/}
  //         </TouchableOpacity>
  //       </View>
  //       <View style={{ margin: 10 }}>
  //         <RenderHtml
  //           source={{ html: this.state.treasure_details.treasure_desc }}
  //         />
  //       </View>
  //       <TouchableOpacity
  //         style={[styles.button, styles.buttonClose]}
  //         onPress={() => this.setState({ treasurePopup: false })}>
  //         {/* onPress={() => this.clickOnCoin()}> */}
  //         <Text style={styles.textStyle22}>Collect</Text>

  //       </TouchableOpacity>


  // {/* <Text style={{ margin: 10, color: "black", fontSize: 10 }}>
  //   {props.msg}
  // </Text>
  // <Text style={{ margin: 10, color: "black", fontSize: 10 }}>
  //   {props.msg1}
  // </Text>
  // <Text
  //   style={{
  //     marginLeft: 10,
  //     marginRight: 10,
  //     marginBottom: 10,
  //     color: "black",
  //     fontSize: 10
  //   }}
  // >
  //   {props.msg2}
  // </Text>
  // <Text
  //   style={{
  //     marginLeft: 10,
  //     marginRight: 10,
  //     marginBottom: 10,
  //     color: "black",
  //     fontSize: 10
  //   }}
  // >
  //   {props.msg3}
  // </Text> */}
  // <View
  //   style={{
  //     width: deviceWidth - 40,
  //     flexDirection: "row",
  //     justifyContent: "space-around",
  //     alignItems: "center",
  //     alignSelf: "center",
  //     margin: 10
  //   }}
  // >
  // {/* <TouchableOpacity onPress={() => props.method("Crown")}>
  //     <Image
  //       style={{ width: 80, height: 80 }}
  //       source={require("../../../img/rsz_gold_crown.png")}
  //     />
  //   </TouchableOpacity>
  //   <TouchableOpacity onPress={() => props.method("Cup")}>
  //     <Image
  //       style={{ width: 80, height: 80 }}
  //       source={require("../../../img/rsz_gold_cup.png")}
  //     />
  //   </TouchableOpacity>
  //   <TouchableOpacity onPress={() => props.method("Ring")}>
  //     <Image
  //       style={{ width: 80, height: 80 }}
  //       source={require("../../../img/rsz_gold_ring.png")}
  //     />
  //   </TouchableOpacity> */}
  //     </View>
  //   </View>
  // </View>
  /* <Dialog
          method={() => this.toggleModalRandomTreasure()}
          titleText="A friendly reminder"
          msg={`You are still a guest user of Raotts. Only Full members can send and receive World Raott Coins and buy deals using World Raott coins. You have to invite at least one friend and he/she must join Raotts using your invite code for you to become a full member. You have remaining time ${diffInStr}`}
          msg={
            'You are still a guest user of Raotts. Only Full Members can send and receive World Raott Coins and play the Treasure Hunt. Please allow 24 hours for processing to be approved as a Full Member.'
          }
        /> */

  /* <DialogRandomTreasure
    method={() => this.toggleModalRandomTreasure()}
    titleText={this.state.treasure_details.treasure_title || 'Treasure Name'}//"Select one symbol"
  // msg="The World Raott Coins were minted in 3 powerful symbols . The Crown, the Cup and the Ring."
  // msg1="Please decide carefully and choose one symbol to start earning and collecting World Raott Coins."
  // msg2="Remember, you can only win deals and cash if you get the right World Raott Coin combination of your chosen symbol. Also note that once you have selected a symbol you cannot change it."
  // msg3="Please click below and choose one symbol to start your earnings and collection."
  />
  <RenderHtml
    source={{ html: this.state.treasure_details.treasure_desc }}
  />
*/
  /* <View style={styles.centeredView}>
    <View style={styles.modalView2}>
      <Text style={styles.headerTextStyle}>
        Treasure GeoMinted in this location!
      </Text>
      <View
        style={{
          alignItems: 'center',
        }}> */
  /* <Image
          // style={styles.imagestyle}
          // source={{
          //   uri:
          //     'https://raott.com/raottdev/assets/treasure_images/' //+
          //   //  this.state.treasure_details.treasure_image,
          // }}
          style={[
            styles.imageStyle,
            {
              alignSelf: 'center',
              marginTop: 0,
              width: 60,
              height: 60,
            },
          ]}
          source={require('../../../img/blank_coin.png')}

        /> */
  /* </View>
      <Text style={[styles.center, styles.centerRed]}>
        {this.state.treasure_details.treasure_title || 'Treasure Name'}
      </Text>
      <Text
        style={{
          fontSize: 16,
          color: '#000000',
          textAlign: 'center',
        }}>
        Treasure Value: R{' '}
        {numeral(this.state.treasure_details.treasure_amount).format(
          '0,0',
        )}{' '}
        ({this.state.treasure_details.home_currency})
      </Text>
      <TouchableOpacity
        style={[styles.button, styles.buttonClose]}
        onPress={() => this.setState({ treasurePopup: false })}>
        <Text style={styles.textStyle}>Collect</Text>
      </TouchableOpacity>
    </View>
  </View> */
  /*
{"home_currency": "", "text1": "Plenty", "text2": "0omnfg", "treasure_amount": "1", "treasure_category": "22", "treasure_desc": "<p style=\"text-align: center;\">Congratulations.</p>
<p style=\"text-align: left;\">You have Geominted the above Digital Collectible.</p>
<p style=\"text-align: left;\">This is valued at 1 World Raott Coin ($1 USD)</p>
<p style=\"text-align: left;\">Each Collectible has a unique id and can never be duplicated, lost, stolen or copied.</p>
<p style=\"text-align: left;\">You have become the true digital owner of this collectible. The unique id of the collectible will be attached to your username.</p>
<p style=\"text-align: left;\">PS. This Digital collectible/ treasure is not redeemable for cash or World Raott Coins or any other prizes unless otherwise stated.</p>", "treasure_id": "99", "treasure_title": "Card 8"}
*/


  //     </Modal>
  //   );
  // }

}



function bindAction(dispatch) {
  return {
    setUser: data => dispatch(setUser(data)),
  };
}

const mapStateToProps = state => ({
  user: state.user.data,
});

LandingPage.navigationOptions = {
  drawerLockMode: 'locked-closed',
};

export default connect(
  mapStateToProps,
  bindAction,
)(LandingPage);

/*
Can I have an on/off button next to each treasure in the admin ->Manage Treasures-> View treasure. Where I turn on the button, these treasures will be randomly allocated to users once a day when they open the app. The user will get only one treasure out of all the treasures turned on The user will get the treasure the same way he gets a treasure when he is in a certain location. Ie pop up on the screen and then when he clicks collect it will be added to his treasure collection. Follow exact steps as when the user gets a treasure when he is in a certain location. I can send you the video of how this is done. Note- If the user already has this treasure, it should not add another one in his “Treasure collection” . it is already coded this way.

Hi Nick,
To keep you updated on the project progress. I have worked on mobile API (so that users get one random treasure when he opens the app for the first time in a 24 hours period, from the list of "ON" treasure list). For the same I have worked onto the below mentioned points:

1. Updated Controller Script as we have to get only random treasure from the on list of treasure.

2. Created Model Script, also set headers as per user authentication in API.

3. I have created staging environment of your api and created a new database for staging environment. Kindly find the staging site URL and database details with directory structure. We have also updated database name by staging database and also updated its config URL.

Database (For Staging API): raott_staging_api Directory Staging API: public_html/raott.com/api-staging
4. I have tested API with API tool (Postman), app developer can also start his work. Further if he need any assistance then he can connect with me. Kindly find the screenshot for your reference.

Further if you have any other work in your project related to admin panel and new APIs related or any modification, please let us know so that we can plan our work accordingly.

*/