const React = require('react-native');
import { Dimensions } from 'react-native';
const { StyleSheet } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: '#e7e4e5',
  },
  mb10: {
    marginBottom: 10,
  },
  tabStyle: {
    alignItems: 'center',
    width: deviceWidth / 2 - 30,
    height: 120,
    borderRadius: 5,
    backgroundColor: 'white',
  },
  imageStyle: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginTop: 20,
  },
  centerRed: {
    color: '#e02d2e',
    fontSize: 20,
    fontWeight: 'bold',
  },

  textStyle: {
    textAlign: 'center',
    top: 10,
    width: 140,
    fontSize: 11,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  textStyle22: {
    marginRight: 10,
    textAlign: 'right',
    alignSelf: 'flex-end',
    top: 10,
    width: 140,
    fontSize: 10,
    color: 'black',
    backgroundColor: 'rgba(0,0,0,0)',
  },

  //              <Text style={{ margin: 10, color: 'black', fontSize: 10, textAlign: 'right' }}>Close</Text>

  containerDialog: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(150,150,150, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewStyle: {
    flexDirection: 'row',
    width: deviceWidth,
    height: 60,
    marginTop: 50,
    alignSelf: 'center',
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  compass: {
    width: 40,
    height: 40,
    marginRight: 5,
  },
  centeredView2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
    // width: deviceWidth,
    // height: deviceHeight,
    // marginRight: 50,
    // marginLeft: 50,
    // marginTop: 5,
    // width: deviceWidth-100,
    // innerWidth: 100,
    position: 'absolute',
    top: -100,
    bottom: -100,
    left: -100,
    right: -100,
    // backgroundColor: 'rgba(150,150,150, 0.8)',
    // alignItems: 'center',
    // justifyContent: 'center',


  },
  modalView3: {
    backgroundColor: 'white',
    borderRadius: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  headerTextStyle2: {
    backgroundColor: '#e02d2e',
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 16,
    padding: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingLeft: 30,
    paddingRight: 30,
  },
  imagestyle2: {
    width: 150,
    height: 150,
    marginTop: 10,
    marginBottom: 10,
    resizeMode: 'contain',
  },
  cancelImageStyle2: {
    marginRight: 10,
    width: 20,
    height: 20,
    marginTop: 0,
    justifyContent: 'space-between',
  },

  center2: {
    textAlign: 'center',
    marginBottom: 10,
  },
  button2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginTop: 10,
    marginBottom: 10,
  },
  buttonClose2: {
    backgroundColor: '#e02d2e',
  },
  textStyle2: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingLeft: 30,
    paddingRight: 30,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: -20,
    marginLeft: -20,
    backgroundColor: '#0000009e',
    height: deviceHeight + 30,
    width: deviceWidth + 10
  },
  modalView: {
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: deviceWidth - 50
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#e02d2e",
    width: 180
  },
  modalTextStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
};
