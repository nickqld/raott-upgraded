import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Text,
  ListItem,
  List,
  Card,
  CardItem,
  ScrollableTab,
  Tabs,
  Tab,
} from 'native-base';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
  BackHandler,
} from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import {DrawerNavigator} from 'react-navigation';
import DealDetails from './DealDetails';
import styles from './styles';
import axios from 'axios';
import Loading from '../Loading/';
import {InternetCheck, TimeOutError} from '../common/';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { NativeBaseProvider, Box, Center } from 'native-base';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import SafeAreaView from 'react-native-safe-area-view';

export default class DealHistory extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      dealsHistory: [],
      isLoading: true,
      count: '1',
      loadButton: false,
      nodataText: false,
    };
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
    this.getData(this.state.count);
  }

  componentDidMount() {
    console.log('Category page', this.props);
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.goBack();
    return true;
  }

  renderDealsHistory() {
    if (this.state.dealsHistory && this.state.dealsHistory.length > 0) {
      return this.state.dealsHistory.map((dealHistory, i) => (
        <DealDetails key={i} dealHistory={dealHistory} />
      ));
    } else {
      return <View />;
    }
  }

  click() {
    SELF.setState({count: SELF.state.count});

    this.getData(SELF.state.count);
  }

  goBack() {
    const {navigation} = this.props;
    navigation.goBack();
    if (navigation.state.params.onSelect) {
      navigation.state.params.onSelect();
    }
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        {/* <Container style={styles.container}> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >
          <View style={styles.headerStyle}>
            <TouchableOpacity
              style={styles.goBackButton}
              onPress={() => this.goBack()}>
              <MaterialCommunityIcons name="arrow-left" size={24} color="white" />
            </TouchableOpacity>

            <View transparent style={styles.headerText}>
              <Text style={{color: 'white'}}>Deal History</Text>
            </View>
          </View>

          <ScrollView>
            {this.renderDealsHistory()}
            {this.state.loadButton && (
              <TouchableOpacity onPress={() => this.click()}>
                <View transparent style={styles.headerTitle}>
                  <Text style={{color: 'black', marginTop: 10}}>Load More</Text>
                </View>
              </TouchableOpacity>
            )}
          </ScrollView>
          {this.state.nodataText && (
            <Text
              style={{
                textAlign: 'center',
                alignSelf: 'center',
                margin: 10,
                position: 'absolute',
                top: 65,
              }}>
              {' '}
              No record found.
            </Text>
          )}

          <Loading isLoading={this.state.isLoading} />
        {/* </Container> */}
        </Box>
      </SafeAreaView>
    );
  }

  isContainId(arr, id) {
    if (!id) {
      return false;
    }
    for (const obj of arr) {
      if (obj.dealId && obj.dealId === id) {
        return true;
      }
    }

    return false;
  }

  isSameDeals(arr1, arr2) {
    for (const obj1 of arr1) {
      if (!this.isContainId(arr2, obj1.dealId)) {
        return false;
      }
    }

    return true;
  }

  getData(count) {
    let countFetch = count;
    AsyncStorage.getItem('apiToken')
      .then(value => {
        axios({
          url: 'https://raott.com/api/v2/screenDealsHistory/' + count + '/20',
          type: 'GET',
          method: 'GET',
          timeout: 30000,
          headers: {
            RaottAuth: value,
          },
        })
          .then(function(response) {
            SELF.setState({
              isLoading: false,
            });
            // console.log('response screenDealsHistory');
            // console.log(response);
            let dealList = [];
            Object.keys(response.data.history).map(key => {
              dealList.push(response.data.history[key]);
            });

            if (dealList.length === 0) {
              SELF.setState({
                loadButton: false,
              });
            } else if (SELF.isSameDeals(dealList, SELF.state.dealsHistory)) {
              SELF.setState({
                loadButton: false,
              });
            }

            if (count > 1) {
              let dealArray = SELF.state.dealsHistory;

              dealList.map(deal => {
                dealArray.push(deal);
              });
              SELF.setState({dealsHistory: dealArray, isLoading: false});
            } else {
              if (dealList.length > 0 && dealList.length === 20) {
                SELF.setState({
                  dealsHistory: dealList,
                  isLoading: false,
                  loadButton: true,
                });
              } else if (dealList.length > 0) {
                SELF.setState({dealsHistory: dealList, isLoading: false});
              } else {
                SELF.setState({nodataText: true});
              }
            }
          })
          .catch(function(error) {
            SELF.setState({isLoading: false});
            TimeOutError(error, () => SELF.getData(countFetch));
          });
      })
      .done(); // ending statement of asyncstorage
  }
}
