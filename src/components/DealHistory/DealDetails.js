import React, { Component } from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    Left,
    Right,
    Body,
    Text,
    ListItem,
    List, CardItem, ScrollableTab, Tabs, Tab, Card
} from "native-base";
import { Dimensions, View, TouchableOpacity, Image, ScrollView, Alert, Platform } from 'react-native'
//import { DrawerNavigator } from 'react-navigation';
import axios from 'axios';
import styles from "./styles";
import Moment from 'moment';




const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const setImage = '';

export default class DealDetails extends Component {






    render() {
        const { dealHistory } = this.props;
        const { purchasedDateTime, dealId, coinsUsed, title, image } = dealHistory;
        return (
       
                <Box
                width='100%'
                overflow="hidden"
                borderColor="gray.300"
                borderWidth="1"
                borderRadius="3"
             
            
           
                 shadow= '2'
                 alignSelf= 'center'
                _web={{
                  shadow: 2,
                  borderWidth: 0,
                }}
                _light={{
                  backgroundColor: "gray.50",
                }}
                >
        
                 
                    <View style={{ flexDirection: 'row', height: 80 }}>
                        <Image style={styles.profileImage} source={{uri:image}} />
                        <View style={{ justifyContent: 'center', marginLeft: 20, flexDirection: 'column' }}>
                            <Text numberOfLines={1} style={{marginHorizontal:5,   width: deviceWidth - 180 }}>{title}</Text>
                            <View style={{ flexDirection: 'row', width: deviceWidth - 180, marginTop: 10, borderWidth: 0 }}>
                                <Text style={{ color: 'grey', fontSize: 14 }}>R {coinsUsed}</Text>

                                <Text style={{ marginLeft: 80, fontSize: 14, color: 'grey' }}>{Moment(purchasedDateTime).format('D MMM YYYY ')}</Text>
                            </View>
                        </View>
                    </View>
                


            
 </Box>


        )
    }

}

