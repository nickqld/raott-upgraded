import { Dimensions, Platform } from 'react-native'

const React = require("react-native");

const { StyleSheet } = React;


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: "white",

  },
  headerStyle: {
    backgroundColor: "#e02d2e",
    height: 44,
    marginTop: (Platform.OS === 'ios') ? 20 : 0,
    flexDirection:'row'
  },
   footerStyle: {
    backgroundColor: "rgba(0,0,0,0)",
    height: (Platform.OS === 'android') ? 64 : 44,
    marginTop: (Platform.OS === 'ios') ? 20 : 0
  },
  headerLeftBtn: {
    left: 10,
    position: 'absolute',
  },
  headerTitle: {
   
    alignItems: 'center',
    borderWidth:0,
    justifyContent:'center',
    bottom:(Platform.OS === "ios")?5:0,
  },
  headerRightBtn: {
    alignSelf: 'flex-end',
    backgroundColor: 'white',
    width: 70,
    height: 30,
    right: 20,
    bottom:(Platform.OS === "ios")?5:15,
    borderRadius: 20,
    flexDirection: 'row',
    position: 'absolute'
  },
  rightIcon: {
    backgroundColor: 'rgba(0,0,0,0)',
    left: 10,
    top: 2,
    fontSize: 25,
    color: '#e02d2e'
  },
  rightAddIcon: {
    backgroundColor: 'rgba(0,0,0,0)',
    fontSize: 20,
    top: 5,
    color: '#e02d2e',
    left: 25
  },
  cardStyle: {
    width: deviceWidth - 80,
    alignSelf: 'center'
  },
  profileImage: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    left: 10,borderRadius:30,
   resizeMode:'cover'
  },
  trasnferBtn: {
    width: 70,
    height: 20,
    marginTop:5,
    backgroundColor: '#e02d2e',
    borderRadius: 10,justifyContent:'center'
  },
  transferText: {
    color: 'white',
    
    fontSize: 12,
    alignSelf: 'center',
    backgroundColor: 'rgba(0,0,0,0)'
  },
  cardRightPortion: {

    backgroundColor: 'rgba(254,254,222,1)',
    width: deviceWidth * 0.296,
    height: 80,
    position:'absolute',
    right:0,
    
    justifyContent: 'center',
    flexDirection: 'row',
    
  },
  coinStyle: {
    width: 60,
    height: 60,
    alignSelf: 'center',
   
  },
  nextForward: {
    color: 'rgba(188,183,183,1)',
    alignSelf: 'center',
    left:(Platform.OS === 'ios')?10:0
    
    
  },
  goBackButton:
    {
        height:44,
        width:44,
        alignItems:'center',
        justifyContent:'center'
    },
    
    headerText:
    {  
        height:44,
        width: deviceWidth - 98,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 5
    },

};
