import React, { Component } from 'react';
import { ButtonPress } from './common';
import {
  Text,
  View,
  Dimensions,
  Image,
  ScrollView,
  BackHandler,
} from 'react-native';
import { WebView } from 'react-native-webview';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Left,
  Right,
  Body,
  ListItem,
  List,
  Card,
  CardItem,
  Item,
  Input,
} from 'native-base';
import { InternetCheck } from './common/';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
import SafeAreaView from 'react-native-safe-area-view';

class FAQ extends Component {
  constructor(props) {
    super(props);
    this._backAndroidPress = this.backAndroidPress.bind(this);

    InternetCheck();
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backAndroidPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this._backAndroidPress,
    );
  }

  backAndroidPress() {
    this.props.navigation.navigate('LandingPage');
    return true;
  }
  render() {
    return (
      <SafeAreaProvider style={{ flex: 1 }}>
        {/* <Container style={styles.mainContainer}> */}
        {/* <View>
            <Header style={styles.headerStyle}>
              <Left style={{flex: 1}}>
                <Button
                  transparent
                  onPress={() => this.props.navigation.openDrawer()}>
                  <Icon name="menu" style={{fontSize: 25, color: '#e02d2e'}} />
                </Button>
              </Left>
              <Body style={{flex: 10, alignItems: 'center'}}>
                <Title style={{color: '#e02d2e'}}>FAQ</Title>
              </Body>
              <Right style={{alignItems: 'center'}}></Right>
            </Header>
          </View> */}
        <Box display='flex' flex='1' backgroundColor='white' flexDirection='column' >

          <View
            style={{
              width: deviceWidth,
              backgroundColor: '#e7e4e5',
              paddingHorizontal: 10,
              flexDirection: 'row'
            }}>
            <Button
              transparent
              size="10"
              colorScheme="transparent"
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon
                as={<MaterialIcons name="menu" />}
                style={{ fontSize: 25, color: '#e02d2e' }}
              />
            </Button>
            <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', alignSelf: 'stretch' }} >
            <Text style={{color: '#e02d2e'}}>FAQ</Text>
            </View>
          </View>







          <WebView
            source={{ uri: 'https://raott.com/raottdev/faq' }}
            style={{ marginTop: 10 }}
          />

        </Box>
        {/* </Container> */}
      </SafeAreaProvider>
    );
  }
}
const styles = {
  mainContainer: {
    flex: 1,
    width: deviceWidth,
    backgroundColor: 'white',
  },
  headerStyle: {
    backgroundColor: '#e7e4e5',
  },
};
FAQ.navigationOptions = {
  header: null,
  gesturesEnabled: false,
  drawerLockMode: 'locked-closed',
};
export default FAQ;
