import React, {Component} from 'react';
import { View, ActivityIndicator, Dimensions,Text,Button,TouchableOpacity,Image } from 'react-native';
import styles from './styles';
import Display from 'react-native-display';
import PropTypes from 'prop-types';


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


class Picker extends Component {

// const {listItem} = pickerData
  
  state = {  enable: false};
  componentDidMount() {
    setTimeout(
      () => { this.showPicker() },
      100
    );
  }

  getListItems(data, i) {
    return (<TouchableOpacity style={{alignItems:'center', }} onPress={() => this.props.hidePicker(i)}>
        <Text style={{marginTop:10,fontSize:18,color:'grey',  marginBottom:10}}>{data}</Text>
        <View style={{width:deviceWidth-60,alignSelf:'center',height:1,backgroundColor:'rgba(244,243,243,1)'}}></View> 
</TouchableOpacity>);
  }

  showPicker(){
    this.setState({isEnable: true})
  }


  render() {
    return (
    <TouchableOpacity activeOpacity={1.0} style={[styles.container]} onPress={this.props.hidePicker}>

   <Display enable={this.state.isEnable} 
            enterDuration={400} 
            exitDuration={400}
            exit="fadeOutDown"
            enter="fadeInUp">        
  
      <TouchableOpacity style={{width:deviceWidth, backgroundColor: 'white',}}  activeOpacity={1.0}
     
      >
   <View style={{width:deviceWidth,backgroundColor:'rgba(245,245,245,1)',flexDirection:'row', 
       
        justifyContent: 'center',height:50,alignItems:'center'}}>
   <Text style={{ fontSize:18,color:'grey',}} > SORT BY</Text>
   
   </View>
   
    {
     this.props.listItems.map((data,i) => {
       return (this.getListItems(data,i))
     })
     
   } 



             
      </TouchableOpacity>
      </Display>

    </TouchableOpacity>
  );
  }

  
};

Picker.propTypes = {
  size: PropTypes.string,
};

Picker.defaultProps = {
  size: 'large',
};

export default Picker;


