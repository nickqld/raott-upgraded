import React, { Component } from 'react';
import { Image, Dimensions, Alert, ScrollView, FlatList, TouchableWithoutFeedback } from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  Button,
  View,
  StyleProvider,
  getTheme,
  variables,
} from 'native-base';
import axios from 'axios';
import styles from './style';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IonIcon from 'react-native-vector-icons/Ionicons';

import _ from 'lodash';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider, Box, Center } from 'native-base';
const drawerCover = require('../../../img/raott_logo.png');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const dataInitial = [
  {
    name: 'Go to Raott Account',
    route: 'LandingPage',
    icon: 'md-cash',
    color: '#e02d2e',

  },
  {
    name: 'Edit Profile',
    route: 'EditProfile',
    icon: 'md-person',
    bg: '#e02d2e',
  },
  {
    name: 'Home Currency',
    route: 'HomeCurrency',
    icon: 'logo-usd',
    bg: '#e02d2e',
  },
  {
    name: 'What is GeoMint?',
    route: 'Geomint',
    icon: 'md-help-circle',
    bg: '#e02d2e',
  },
  {
    name: 'FAQ',
    route: 'FAQ',
    icon: 'md-information-circle',
    bg: '#e02d2e',
  },
  {
    name: 'Contact Us',
    route: 'ContactUs',
    icon: 'call',
    bg: '#e02d2e',
  },
  {
    name: 'Sign Out',
    route: 'HomeScreen',
    icon: 'md-log-out',
    bg: '#e02d2e',
  },
];

navigationOptions: ({ navigation }) => ({
  drawerLockMode: 'locked-closed',
});

let deviceId = '';
let status = false;
let invite_icon = require('../../../img/WarningMsg.png');
class SideBar extends Component {
  constructor(props) {
    super(props);
    SELF = this;
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
    };
    this.clickDebounce = _.debounce(
      this.sideBarButtonPressed.bind(this),
      1000,
      { leading: true, trailing: false },
    );
    this.getdeviceId();
  }

  sideBarButtonPressed(data) {
    console.log('data')
    console.log(data)
    if (data.route === 'HomeScreen') {
      this.signOut();
    }
    // this.props.navigation.navigate("HomeScreen")
    this.props.navigation.navigate(data.route);
  }

  isFriendRequestAdded() {
    for (const data of dataInitial) {
      if (data.name === 'Invite a Friend') {
        return true;
      }
    }
    return false;
  }
  render() {
    // console.log('this.props.navigation.state.routeName');
    // console.log(this.props.navigation.state.routeName);
    // console.log(this.props.user);
    // console.log('this.props.user');
    // const isShowFriendRequest = true;
    let datas = [].concat(dataInitial);
    // console.log(status)
    if (this.props.user && this.props.user.membership_status == 0) {
      datas.splice(2, 0, {
        name: 'Invite A Friend',
        route: 'Invite',
        icon: 'card-giftcard',
        bg: '#e02d2e',
        isMaterialIcon: true,
      });
    }

    return (

      <Box display='flex' flex='1'
        backgroundColor='#fff'
        top='-1'
        height={deviceHeight} flexDirection='column' >


        {/* <Container> */}
        {/* <Content
          bounces={false}
          style={{
            flex: 1,
            backgroundColor: '#fff',
            top: -1,
            height: deviceHeight,
          }}> */}
        <Image source={drawerCover} style={styles.drawerCover}></Image>

        {/* <List
            style={{
              backgroundColor: '#f9fafc',
              borderWidth: 1,
              borderColor: '#dedede',
              marginTop: 20,
              height: deviceHeight - 120,
            }}
            dataArray={datas}
            renderRow={data => (
              <List.Item
                button
                noBorder
                onPress={() => this.clickDebounce(data)}>
                  
                <Left>
                  {data.isMaterialIcon ? (
                    <MaterialIcons
                      active
                      name={data.icon}
                      style={{color: 'red', fontSize: 26, width: 30}}
                    />
                  ) : (
                    <Icon
                      active
                      name={data.icon}
                      style={{color: 'red', fontSize: 26, width: 30}}
                    />
                  )}
                  {data.name === 'What is GeoMint?' ? (
                    <View
                      style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                      <Text style={{lineHeight: 30, marginLeft: 20}}>
                        What is GeoMint
                      </Text>
                      <Text
                        style={{
                          fontSize: 11,
                          lineHeight: 18,
                        }}>
                        {'\u00AE'}
                      </Text>
                      <Text style={[styles.imageText, {lineHeight: 30}]}>
                        ?
                      </Text>
                    </View>
                  ) : (
                    <Text style={styles.text}>{data.name}</Text>
                  )}
                  {data.name === 'Invite A Friend' && (
                    <Image
                      style={{width: 20, height: 20}}
                      source={require('../../../img/WarningMsg.png')}></Image>
                  )}
                </Left>
                {data.types && (
                  <Right style={{flex: 1}}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg,
                      }}>
                      <Text
                        style={styles.badgeText}>{`${data.types} Types`}</Text>
                    </Badge>
                   </Right>
                )}
              </List.Item>
            )}
          /> */}

        <FlatList
          style={{
            backgroundColor: '#f9fafc',
            borderWidth: 1,
            borderColor: '#dedede',
            marginTop: 20,
            height: deviceHeight - 120,
          }}
          data={datas}

          renderItem={({ item }) =>
            <TouchableWithoutFeedback onPress={() => this.clickDebounce(item)}>
              <Box margin='2' display='flex' flexDirection='row' >

                {item.isMaterialIcon ? (
                  <MaterialIcons
                    active
                    name={item.icon}
                    style={{ color: 'red', marginHorizontal: 10, fontSize: 26, width: 30 }}
                  />
                ) : (
                  <IonIcon
                    active
                    name={item.icon}
                    style={{ color: 'red', marginHorizontal: 10, fontSize: 26, width: 30 }}
                  />
                )}


                <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', alignSelf: 'stretch' }} >


                  {item.name === 'What is GeoMint?' ? (
                    <View
                      style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                      <Text style={{ lineHeight: 30, marginLeft: 20 }}>
                        What is GeoMint
                      </Text>
                      <Text
                        style={{
                          fontSize: 11,
                          lineHeight: 18,
                        }}>
                        {'\u00AE'}
                      </Text>
                      <Text style={[styles.imageText, { lineHeight: 30 }]}>
                        ?
                      </Text>
                    </View>
                  )
                    :
                    (item.name === 'Invite A Friend' ? (


                      <Box display='flex' flexDirection='row' >
                        <Text style={styles.text}>{item.name}</Text>
                        <Image
                          style={{ width: 20, height: 20 }}
                          source={require('../../../img/WarningMsg.png')}></Image>

                      </Box>
                    )

                      : (
                        <Text style={styles.text}>{item.name}</Text>
                      ))}


                </View>


                <Box display='flex' flexDirection='row' >

                  {item.types && (

                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: item.bg,
                      }}>
                      <Text
                        style={styles.badgeText}>{`${item.types} Types`}</Text>
                    </Badge>

                  )}

                </Box>

                {/* <Text style={styles.item}>{item.name}</Text> */}
              </Box>
            </TouchableWithoutFeedback>


          }
        />



        {/* </Content> */}
        {/* </Container> */}
      </Box>

    );
  }

  async getdeviceId() {
    deviceId = await AsyncStorage.getItem('@deviceid:key').then(data =>
      data ? JSON.parse(data) : null,
    );
    deviceId?.replace(/[^a-zA-Z ]/g, "")

  }

  signOut = async () => {
    // console.warn('signOut')
    await AsyncStorage.getItem('apiToken')
      .then(apiToken => {
        // console.log(apiToken)
        // console.log( deviceId)
        axios({
          url: 'https://raott.com/api/v2/logout',
          type: 'POST',
          method: 'POST',
          data: {
            deviceid: deviceId,
          },
          headers: {
            RaottAuth: apiToken,
          },
        })
          .then(function (response) {
            // console.warn('HERE')
            // this.props.navigation.navigate("HomeScreen")
            // console.log('response');
            // console.log(response);
          })
          .catch(function (error) {
            // console.log(error);
            Alert.alert('Error', error.reason);
          });
        // }).done();
      })
      .done();

    try {
      // console.warn('SignOut')
      await AsyncStorage.setItem('apiToken', '');
      await AsyncStorage.setItem('selectedCoin', '');
      await AsyncStorage.setItem('inviteCode', '');
      await AsyncStorage.setItem('membership_status', '');
      await AsyncStorage.setItem('reminder_date', '');
    } catch (error) {
      // Error saving data
    }
  };
}

function bindAction(dispatch) {
  return {};
}

const mapStateToProps = state => ({
  user: state.user.data,
});

export default connect(mapStateToProps, bindAction)(SideBar);
