import 'react-native-gesture-handler';
import React, {Component} from 'react';
import CreateAccount from './CreateAccount.js';

import Drawer from './Drawer.js';
import LoginForm from 'LoginForm.js';
//import {createStackNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import EditProfile from './EditProfile.js';
import Picker from './components/Picker/Picker.js';
import ViewAnimation from './ViewAnimation.js';
import AccountsDetail from './components/ViewTransaction/AccountsDetail.js';
import Deals from './components/ViewDeals/Deals.js';
import TransferCoins from './components/TransferCoinToAny/TransferCoins.js';
import FriendsList from './components/TransferCoinToFriends/FriendsList.js';
import DealsDetails from './components/TreasureChest/DealsDetails.js';
import RaottCoinsDetail from './components/RaottCollection/RaottCoinsDetail.js';
import dealsHistory from './components/DealHistory/deals.js';
import Invite from './Invite.js';
import Otp from './Otp.js';
import ForgotPassword from './ForgotPassword.js';
import Category from './TreasureHunt/Category.js';
import MapUI from './TreasureHunt/MapUI.js';
import HomeScreen from './HomeScreen.js';

export default DrawNav = createStackNavigator({
  CreateAccount: {screen: CreateAccount},
  Drawer: {screen: Drawer},
  LoginForm: {screen: LoginForm},
  Invite: {screen: Invite},
  Otp: {screen: Otp},
  ForgotPassword: {screen: ForgotPassword},
  Category: {screen: Category},
  MapUI: {screen: MapUI},
  HomeScreen: {screen: HomeScreen},
});
